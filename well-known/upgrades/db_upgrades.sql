--
-- Table structure for table `exercise_methods`
--

CREATE TABLE `exercise_methods` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise_methods`
--

INSERT INTO `exercise_methods` (`id`, `name`) VALUES
(1, 'Private'),
(10, 'TL Guitar'),
(11, 'TL Bass Guitar'),
(12, 'TL Piano');

--
-- Table structure for table `exercise_to_methods`
--

CREATE TABLE `exercise_to_methods` (
  `exercise_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


/*****Change usser branch*/

ALTER TABLE `toneline_my`.`users` ADD COLUMN `is_teacher` tinyint NOT NULL DEFAULT 0 AFTER `simplybook_serviceprovider_id`;