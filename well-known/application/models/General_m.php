<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class General_m extends CI_Model
{
    const TONELINE_SCHOOL_USER = '202';

	public function pull_exercises($type,$user_id,$sort = 1,$style = 0,$role = 0,$search = null)
	{
		// 1 - A-Z
		// 2 - Z-A
		// 3 - Newest
		// 4 - Oldest		
		$music_styles = $this->pull_music_styles();
		$roles = $this->pull_role();
		$types = $this->pull_exercise_types();
        $methods = $this->pull_exercise_methods();
		if ($type=='me' || $type=='tl') {
			switch($sort)
			{
				case 1: $order_by = 'name asc'; break;
				case 2: $order_by = 'name desc'; break;
				case 4: $order_by = 'created_datetime asc'; break;
				default: $order_by = 'created_datetime desc'; break;
			}
			$more_params = '';
			if ($role!=0) {
				$more_params = ' and role_id='.$role;
			}
			if ($style!=0) {
				$more_params .= ' and music_style_id='.$style;
			}
            $tl_user_id='';
            if($type == 'tl'){
			    $tl_user_id= ',202';
            }
			if ($search!=null) {
				$query = $this->db->query('select * from exercises where owner_id in('.$user_id.$tl_user_id.')  and (name LIKE "%'.$search.'%" or description LIKE "%'.$search.'%") '.$more_params.' order by '.$order_by);
			} else {
				$query = $this->db->query('select * from exercises where owner_id in('.$user_id.$tl_user_id.') '.$more_params.' order by '.$order_by);
			}
			foreach ($query->result() as $k=>$v)
			{
				$v->music_style = '';
				foreach ($music_styles as $key=>$val)
				{
					if ($val->id==$v->music_style_id) {
						$v->music_style = $val->name;
					}
				}
				$v->role = '';
				foreach ($roles as $key=>$val)
				{
					if ($val->id==$v->role_id) {
						$v->role = $val->name;
					}
				}
				$v->exercise_types = $this->pull_exercise_types($v->id);
                $v->exercise_methods = $this->pull_exercise_methods($v->id);
			}
			return array('total'=>$query->num_rows(),'result'=>$query->result(),'types'=>$types,'methods'=>$methods);
		}
		if ($type=='all') {
			switch($sort)
			{
				/*case 1: $order_by = 'name asc'; break;
				case 2: $order_by = 'name desc'; break;*/
				case 4: $order_by = 'created_datetime asc'; break;
				default: $order_by = 'created_datetime desc'; break;
			}
			$more_params = '';
			if ($role!=0) {
				$more_params = ' and role_id='.$role;
			}
			if ($style!=0) {
				$more_params .= ' and music_style_id='.$style;
			}
			
			$query = $this->db->get_where('sessions', array('student_id' => $user_id));
			
			$exercises = array();
			$tmp_exercises = array();
			foreach ($query->result() as $k=>$v)
			{
				$session_id = $v->id;
				$tmp_exercises[] = $this->pull_session_exercises($session_id,'no_total');
			}
			$tmp_exercises_ids = '';
			foreach ($tmp_exercises as $key=>$val)
			{
				foreach ($val as $k=>$v)
				{
					if ($tmp_exercises_ids!='') {
						$tmp_exercises_ids.=' OR ';
					}
					$tmp_exercises_ids .= 'id = '.$v->exercise_id;
					/*$tmp = $this->pull_exercise($v->exercise_id);
					if ($tmp != null) {
						$exercises[] = $tmp;
					}*/
				}				
			}
			if ($search!=null) {
				$query = $this->db->query('select * from exercises where (name LIKE "%'.$search.'%" or description LIKE "%'.$search.'%") and ('.$tmp_exercises_ids.') '.$more_params.' order by '.$order_by);
			} else {
				if (empty($tmp_exercises_ids)) {
					$query = $this->db->query('select * from exercises where 0 '.$more_params.' order by '.$order_by);
				} else {
					$query = $this->db->query('select * from exercises where ('.$tmp_exercises_ids.') '.$more_params.' order by '.$order_by);
				}
			}
			foreach ($query->result() as $k=>$v)
			{
				$v->music_style = '';
				foreach ($music_styles as $key=>$val)
				{
					if ($val->id==$v->music_style_id) {
						$v->music_style = $val->name;
					}
				}
				$v->role = '';
				foreach ($roles as $key=>$val)
				{
					if ($val->id==$v->role_id) {
						$v->role = $val->name;
					}
				}
				$v->exercise_types = $this->pull_exercise_types($v->id);
                $v->exercise_methods = $this->pull_exercise_methods($v->id);
				
				if ($role!=0) {
					if ($v->role_id!=$role)
					{
						unset($exercises[$k]);
					}
				}
				if ($style!=0) {
					if ($v->music_style_id!=$style)
					{
						unset($exercises[$k]);
					}
				}
			}
			return array('total'=>$query->num_rows(),'result'=>$query->result(),'types'=>$types,'methods'=>$methods);
		}		
	}

	public function update_teachers_notes($data)
	{
		$this->db->where('teacher_id',$data['teacher_id']);
		$this->db->where('user_id',$data['user_id']);
		$this->db->delete('teachers_notes');
		
		$this->db->insert('teachers_notes',$data);
	}
	
	public function complete_lesson_with_session($user_id,$teacher_id)
	{
		$this->db->where('teacher_id',$teacher_id);
		$this->db->where('user_id',$user_id);
		$data = array(
			'session_id' => $session_id,
			'session_datetime' => date('Y-m-d H:i')
		);
		$this->db->update('lessons',$data);
	}
	
	public function set_lesson($datetime,$end_datetime,$user_id,$program_id,$unit_id,$teacher_id)
	{
		$data = array(
			'datetime' => $datetime,
			'end_datetime' => $end_datetime,
			'user_id' => $user_id,
			'program_id' => $program_id,
			'unit_id' => $unit_id,
			'teacher_id' => $teacher_id
		);
		$this->db->insert('lessons',$data);
	}
	
	public function pull_teachers_notes($data)
	{
		$this->db->where('teacher_id',$data['teacher_id']);
		$this->db->where('user_id',$data['user_id']);
		$query = $this->db->get('teachers_notes');
		return $query->result();
	}
	
	public function remove_file($id,$type)
	{
		$this->db->where('id',$id);
		$data = array();
		if ($type == 'notes') { $data['notes'] = ''; }
		if ($type == 'audio') { $data['audio'] = ''; }
		if ($type == 'video') { $data['video'] = ''; }
		$this->db->update('exercises',$data);
	}
	
	public function delete_user($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('users');
		$this->db->where('user_id',$id);
		$this->db->delete('teachers_notes');
		$this->db->where('student_id',$id);
		$this->db->delete('sessions');
		$this->db->where('owner_id',$id);
		$this->db->delete('sessions');
		$this->db->where('user_id',$id);
		$this->db->delete('programs_to_users');
		$this->db->where('teacher_id',$id);
		$this->db->delete('programs');
		$this->db->where('owner_id',$id);
		$this->db->delete('exercises');		
	}
	
	public function students($owner_id)
	{
		$query_sessions = $this->db->query('select count(*) as total,owner_id from sessions group by owner_id');
		$query = $this->db->query('select * from users where users.id in (select sessions.student_id from sessions where owner_id='.$owner_id.')');
		foreach ($query->result() as $k=>$v)
		{
			$data = $this->db->query('select id,datetime from sessions where student_id='.$v->id.' and owner_id='.$owner_id.' order by datetime desc limit 1');
			$v->session_id = 0;
			foreach ($data->result() as $key=>$val)
			{
				$v->session_id = $val->id;
				$v->session_datetime = $val->datetime;
			}			
		}
		return array('total'=>$query->num_rows(),'results'=>$query->result(),'sessions'=>$query_sessions->result());
	}
	
	public function pull_total_sessions($user_id,$type)
	{
		if ($type == 'me') {
			$query = $this->db->get_where('sessions', array('owner_id' => $user_id));
		} else {
			$query = $this->db->get_where('sessions', array('student_id' => $user_id));
		}
		return $query->num_rows();
	}
	
	function add_exercises_to_session($exercises,$session_id)
	{
		foreach ($exercises as $k=>$v)
		{
			$data = array(
				'session_id' => $session_id,
				'exercise_id' => $v
			);
			$this->db->insert('exercises_to_sessions',$data);
		}
	}
	
	public function remove_session($session_id,$user_id)
	{
		$this->db->where('id', $session_id);
		$this->db->where('owner_id', $user_id);
		$this->db->delete('sessions'); 
	}
	
	public function remove_exercise($exercise_id,$user_id)
	{
		$this->db->where('id', $exercise_id);
		$this->db->where('owner_id', $user_id);
		$this->db->delete('exercises'); 
	}
	
	public function times_exercise_sent_to_user($exercise_id,$student_id)
	{
		$total = 0;
		$query = $this->db->query('select COUNT(*) as total,session_id as sid from exercises_to_sessions where exercise_id='.$exercise_id.' group by session_id');
		foreach ($query->result() as $k=>$v)
		{
			$ses = $this->pull_session_noowner($v->sid);
			if (isset($ses[0])) {
				if (($v->sid == $ses[0]->id)&&($ses[0]->student_id == $student_id)) {
					$total++;
				}
			}
		}
		if ($total == '1') {
			$total = '<span style="background-color:yellow;">1</span>';
		}
		return $total;
	}
	
	public function pull_exercises_sent($exercise_id)
	{
		$this->db->where('exercise_id',$exercise_id);
		$query = $this->db->get('exercises_to_sessions');
		return $query->result();
	}
	
	public function pull_recipients($exercise_id)
	{
		$query = $this->db->query('select * from exercises_to_sessions where exercise_id='.$exercise_id);
		$sessions = array();
		foreach ($query->result() as $k=>$v)
		{
			$sessions[] = $v->session_id;
		}
		if ($sessions!=null) {
			$sessions_data = $this->pull_sessions_by_ids($sessions);
			$students = array();
			foreach ($sessions_data as $k=>$v)
			{
				$students[] = $this->pull_user($v->student_id);
			}
			return array('recipients'=>true,'sessions'=>$sessions_data,'students'=>$students);
		} else {
			return array('recipients'=>false);
		}		
	}
	
	public function pull_session_exercises($session_id,$total = null)
	{
		$this->db->join('exercises', 'exercises.id = exercises_to_sessions.exercise_id', 'left');
		$this->db->select('*,exercises_to_sessions.exercise_id as id');
		$query = $this->db->get_where('exercises_to_sessions', array('session_id' => $session_id));
		
		$ses = $this->pull_session_noowner($session_id);
		$student_id = $ses[0]->student_id;
		
		$music_styles = $this->pull_music_styles();
		$roles = $this->pull_role();
		$types = $this->pull_exercise_types();
        $methods = $this->pull_exercise_methods();
		
		foreach ($query->result() as $k=>$v)
		{
			/*
				exercise_id = $v->id;
				
			*/
			$exercises_sent = $this->pull_exercises_sent($v->id);
			$sessions[] = array();
			foreach ($exercises_sent as $key=>$val)
			{
				$sessions[] = $val->session_id;
			}
			$all_sessions = $this->pull_sessions_by_ids($sessions);
			
			$sent = false;
			$counter = 0;
			foreach ($all_sessions as $key=>$val)
			{
				if (($counter == 0) && ($val->id == $session_id)) {
					$sent = true;
				}
				$counter++;
			}
			
			$v->first_time = $sent;
			$v->num_of_times = $this->times_exercise_sent_to_user($v->id,$student_id);
			$v->music_style = '';
			foreach ($music_styles as $key=>$val)
			{
				if ($val->id==$v->music_style_id) {
					$v->music_style = $val->name;
				}
			}
			$v->role = '';
			foreach ($roles as $key=>$val)
			{
				if ($val->id==$v->role_id) {
					$v->role = $val->name;
				}
			}
			$v->exercise_types = $this->pull_exercise_types($v->exercise_id);
            $v->exercise_methods = $this->pull_exercise_methods($v->exercise_id);
		}
		if ($total=='no_total') {
			return $query->result();
		} else {
			return array('total'=>$query->num_rows(),'result'=>$query->result(),'types'=>$types,'methods'=>$methods);
		}
	}
	
	public function pull_exercise($exercise_id)
	{
		$query = $this->db->get_where('exercises', array('id' => $exercise_id));
		return $query->result();
	}
	
	public function update_user($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}
	
	public function pull_exercise_by_id($exercise_id,$user_id)
	{
		$query = $this->db->get_where('exercises', array('id' => $exercise_id, 'owner_id' => $user_id));
		return $query->result();
	}
	
	public function pull_sessions_by_ids($sessions)
	{
		$query = $this->db->query('select * from sessions where id IN (' . implode(',', array_map('intval', $sessions)) . ') order by datetime desc');
		return $query->result();
	}
	
	public function pull_session_noowner($session_id)
	{
		$query = $this->db->query('select * from sessions where id='.$session_id);
		return $query->result();
	}
	
	public function pull_session($session_id,$user_id)
	{
		$query = $this->db->query('select * from sessions where id='.$session_id.' and owner_id='.$user_id);
		if ($query->num_rows()>0) {
		    $student_id = $query->result();
			$student_id = $student_id[0]->student_id;
			return array('type'=>'me','result'=>$query->result(),'target_user'=>$this->pull_user($student_id));
		} else {
			$query = $this->db->query('select * from sessions where id='.$session_id.' and student_id='.$user_id);
			$owner_id = $query->result();
			if ($owner_id!=null) {
				$owner_id = $owner_id[0]->owner_id;
				return array('type'=>'teacher','result'=>$query->result(),'target_user'=>$this->pull_user($owner_id));
			} else {
				return null;
			}
		}		
	}
	
	public function pull_total_exercises($type,$user_id)
	{
		if ($type == 'me') {
			$query = $this->db->query('select * from exercises where owner_id='.$user_id);
		} else {
			$query = $this->db->get_where('sessions', array('student_id' => $user_id));
			
			$exercises = array();
			$tmp_exercises = array();
			foreach ($query->result() as $k=>$v)
			{
				$session_id = $v->id;
				$tmp_exercises[] = $this->pull_session_exercises($session_id,'no_total');
			}
			$tmp_exercises_ids = '';
			foreach ($tmp_exercises as $key=>$val)
			{
				foreach ($val as $k=>$v)
				{
					if ($tmp_exercises_ids!='') {
						$tmp_exercises_ids.=' OR ';
					}
					$tmp_exercises_ids .= 'id = '.$v->exercise_id;
				}				
			}
			if (empty($tmp_exercises_ids)) {
				return 0;
			} else {
				$query = $this->db->query('select count(id) from exercises where ('.$tmp_exercises_ids.')');
			}
		}
		return $query->num_rows();
	}
	
	public function pull_sessions($type,$user_id,$sort = 3,$search = null)
	{
		// 3 - Newest
		// 4 - Oldest
		switch($sort)
		{
			case 4: $this->db->order_by('datetime','asc'); break;
			default: $this->db->order_by('datetime','desc'); break;
		}		
		if ($type=='me') {
			$query = $this->db->get_where('sessions', array('owner_id' => $user_id));
		} else {
			$query = $this->db->get_where('sessions', array('student_id' => $user_id));
		}
		return array('total'=>$query->num_rows(),'result'=>$query->result());
	}
	
	public function remove_user_programs($user_id)
	{
		$this->db->where('user_id',$user_id);
		$this->db->delete('programs_to_users');
	}
	
	public function set_user_program($user_id,$program_id)
	{
		$data = array(
			'user_id' => $user_id,
			'program_id' => $program_id
		);
		$this->db->insert('programs_to_users',$data);
	}
	
	public function pull_inactive_user_programs($user_id)
	{
		$query = $this->db->get_where('programs_to_users', array('user_id' => $user_id, 'is_active' => 0));
		$resultset = $query->result();
		foreach ($resultset as $k=>$program)
		{
			$query_program = $this->db->get_where('programs', array('id' => $program->program_id));
			$query_resultset = $query_program->result();
			if ($query_resultset!=null) {
				$program->program = $query_resultset[0];
			}
		}
		return $resultset;
	}
	
	public function pull_user_programs($user_id,$is_administrator = false)
	{
		if ($is_administrator) {
			$query = $this->db->get_where('programs');
			$resultset = $query->result();
			foreach ($resultset as $k=>$program)
			{
				$program->program = new stdClass();
				$program->program->name = $program->name;
				$program->program->simplybook_program_id = $program->simplybook_program_id;				
			}
		} else {
			$query = $this->db->get_where('programs_to_users', array('user_id' => $user_id, 'is_active' => 1));
			$resultset = $query->result();
			foreach ($resultset as $k=>$program)
			{
				$query_program = $this->db->get_where('programs', array('id' => $program->program_id));
				$query_resultset = $query_program->result();
				if ($query_resultset!=null) {
					$program->program = $query_resultset[0];
				}
			}
		}
		return $resultset;
	}
	
	public function pull_user($user_id)
	{
		$query = $this->db->get_where('users', array('id' => $user_id));
		return $query->result();
	}
	
	public function pull_music_styles()
	{
		$query = $this->db->get('music_styles');
		return $query->result();
	}
	
	public function pull_session_comments($session_id)
	{
		$query = $this->db->get_where('sessions', array('id' => $session_id));
		foreach ($query->result() as $k=>$v)
		{
			return $v->comments;
		}
	}
	
	public function check_duplicate_name($exercise_name,$exercise_id,$user_id)
	{
		if ($exercise_id==0) {
			$query = $this->db->query('select * from exercises where name="'.$exercise_name.'" and owner_id='.$user_id);
			if ($query->num_rows()>0) {
				return 0;
			} else {
				return 1;
			}
		} else {
			$query = $this->db->query('select * from exercises where name="'.$exercise_name.'" and owner_id='.$user_id.' and id!='.$exercise_id);
			if ($query->num_rows()>0) {
				return 0;
			} else {
				return 1;
			}
		}
	}
	
	public function update_exercise($data,$exercise_id)
	{
		if ($exercise_id==0) {
			$query = $this->db->query('select * from exercises where name="'.$data['name'].'" and owner_id='.$data['owner_id']);
			if ($query->num_rows()>0) {
				return 0;
			} else {
				$this->db->insert('exercises',$data);
				$exercise_id = $this->db->insert_id();
			}
		} else {
			$query = $this->db->query('select * from exercises where name="'.$data['name'].'" and owner_id='.$data['owner_id'].' and id!='.$exercise_id);
			if ($query->num_rows()>0) {
				return 0;
			} else {
				$this->db->where('id',$exercise_id);
				$this->db->update('exercises',$data);
			}
		}
		return $exercise_id;
	}
	
	public function pull_user_id_by_email($email)
	{
		$query = $this->db->get_where('users', array('email' => $email));
		return $query->result();
	}
	
	public function set_exercise_type($exercise_types,$exercise_id)
	{
		$this->db->where('exercise_id', $exercise_id);
		$this->db->delete('exercise_to_types'); 
		
		if ($exercise_types!=null) {
			foreach ($exercise_types as $k=>$v)
			{
				$data = array(
					'exercise_id' => $exercise_id,
					'type_id' => $v
				);
				$this->db->insert('exercise_to_types',$data);
			}
		}
	}
    public function set_exercise_method($exercise_methods,$exercise_id)
    {
        $this->db->where('exercise_id', $exercise_id);
        $this->db->delete('exercise_to_methods');

        if ($exercise_methods!=null) {
            foreach ($exercise_methods as $k=>$v)
            {
                $data = array(
                    'exercise_id' => $exercise_id,
                    'method_id' => $v
                );
                $this->db->insert('exercise_to_methods',$data);
            }
        }
    }
	
	public function update_sessions($data,$session_id)
	{
		unset($data['session_id']);
		if ($session_id==0) {
			$this->db->insert('sessions',$data);
			return $this->db->insert_id();
		} else {
			$this->db->where('id',$session_id);
			$this->db->update('sessions',$data);
			return $session_id;
		}
	}
	
	public function pull_exercise_types($exercise_id = null)
	{
		if ($exercise_id == null) {
			$query = $this->db->get('exercise_types');
			return $query->result();
		} else {
			$query = $this->db->get_where('exercise_to_types', array('exercise_id' => $exercise_id));
			return $query->result();
		}
	}

    public function pull_exercise_methods($exercise_id = null)
    {
        if ($exercise_id == null) {
            $query = $this->db->get('exercise_methods');
            return $query->result();
        } else {
            $query = $this->db->get_where('exercise_to_methods', array('exercise_id' => $exercise_id));
            return $query->result();
        }
    }
	
	public function pull_role()
	{
		$query = $this->db->get('role');
		return $query->result();
	}
}