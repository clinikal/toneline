<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_m extends CI_Model
{	
	public function set_program($data,$id)
	{
		$this->db->where('id',$id);
		$results = $this->db->get('programs');
		if ($results->num_rows() == 0) {
			$data['id'] = $id;
			$this->db->insert('programs',$data);
		} else {
			$this->db->where('id',$id);
			$this->db->update('programs',$data);
		}
	}
	
	public function update($tbl,$data,$where)
	{
		foreach ($where as $k=>$v)
		{
			$this->db->where($v['field'],$v['value']);
		}		
		$this->db->update($tbl,$data);
	}
	
	public function insert($tbl,$data)
	{
		$this->db->insert($tbl,$data);		
	}
	
	public function set_location($data,$id)
	{
		if (($id == null)||($id == 0)) {
			$this->db->insert('locations',$data);
		} else {
			$this->db->where('id',$id);
			$this->db->update('locations',$data);
		}
	}
	
	public function remove_location($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('locations');
		header('Location:'.base_url().'admin/view/locations');
	}
	
	public function remove_program($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('programs');
		header('Location:'.base_url().'admin/view/programs');
	}
	
	public function set_instrument($data,$id)
	{
		if (($id == null)||($id == 0)) {
			$this->db->insert('instruments',$data);
		} else {
			$this->db->where('id',$id);
			$this->db->update('instruments',$data);
		}
	}
	
	public function remove_instrument($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('instruments');
		header('Location:'.base_url().'admin/view/instruments');
	}
	
	public function get($data)
	{
		if (isset($data['where'])) {
			foreach ($data['where'] as $k=>$v)
			{
				$this->db->where($k,$v);
			}
		}
		if (isset($data['order_by'])) {
			$this->db->order_by($data['order_by']);
		}
		$query = $this->db->get($data['tbl']);
		return array('total'=>$query->num_rows(),'results'=>$query->result());
	}
	
	public function data($params,$type)
	{		
		if ($type == 'programs') {
			$users = $this->db->get('programs');
			
			$query = $this->db->query('select * from programs');
			return array('total'=>$query->num_rows(),'results'=>$query->result());
		}
		if ($type == 'instruments') {
			$users = $this->db->get('instruments');
			
			$query = $this->db->query('select * from instruments');
			return array('total'=>$query->num_rows(),'results'=>$query->result());
		}
		if ($type == 'locations') {
			$users = $this->db->get('locations');
			
			$query = $this->db->query('select * from locations');
			return array('total'=>$query->num_rows(),'results'=>$query->result());
		}
		if ($type == 'students') {
			$users = $this->db->get('users');
			
			$query = $this->db->query('select * from exercises order by created_datetime desc');
			return array('total'=>$query->num_rows(),'results'=>$query->result(),'users'=>$users->result());
		}
		if ($type == 'teachers') {
			$query = $this->db->query('select * from sessions order by datetime desc');
			
			$users = $this->db->get('users');
			$exercises = $this->db->query('select count(*) as total,session_id as sessionid from exercises_to_sessions group by session_id');
			return array('total'=>$query->num_rows(),'results'=>$query->result(),'users'=>$users->result(),'exercises'=>$exercises->result());
		}
	}
	
	public function users()
	{
		$query_sessions = $this->db->query('select count(*) as total,owner_id from sessions group by owner_id');
		$query = $this->db->query('select * from users');
		return array('total'=>$query->num_rows(),'results'=>$query->result(),'sessions'=>$query_sessions->result());
	}
}