<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<table id="dtTable">
            	<thead>
                	<th>Name</th>
                	<th>Location</th>
                	<th>Teacher</th>
                	<th>Instrument</th>
                    <th>Time Length</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </thead>
            </table>			
            <br>
            <input class="btn btn-success" id="create_program" type="button" value="Create Program" />
    </div><!--/container-->		
    <!--=== End Content Part ===-->    

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<div class="modal fade" id="add" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <input type="hidden" name="program_id" id="program_id" value="0" />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="program_title" class="modal-title">Add New Program</h4>
            </div>
            <div class="modal-body">
            	<fieldset class="form-group">
                	<label for="program_name">Name:</label>
                	<input class="form-control" type="text" name="program_name" id="program_name" placeholder="Enter Program's Name" />
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Select Teacher:</label><br>
                	<select class="form-control" name="program_teacher_id" id="program_teacher_id" placeholder="Select Program's Teacher" style="width: 100%;">
                    	<option value="0">Select Teacher</option>
                        <?
						foreach ($teachers['results'] as $k=>$v)
						{
							?><option value="<?=$v->id?>"><?=$v->first_name?> <?=$v->last_name?></option><?
						}
						?>
                    </select>
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Select Location:</label>
                	<select class="form-control" name="program_location_id" id="program_location_id" placeholder="Select Location">
                    	<option value="0">Select Location</option>
                        <?
						foreach ($locations['results'] as $k=>$v)
						{
							?><option value="<?=$v->id?>"><?=$v->name?></option><?
						}
						?>
                    </select>
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Select Instrument:</label>
                	<select class="form-control" name="program_instrument_id" id="program_instrument_id" placeholder="Select Instrument">
                    	<option value="0">Select Instrument</option>
                        <?
						foreach ($instruments['results'] as $k=>$v)
						{
							?><option value="<?=$v->id?>"><?=$v->name?></option><?
						}
						?>
                    </select>
                </fieldset>
                <input type="hidden" name="simplybook_program_id" id="simplybook_program_id" value="" />                
                <fieldset class="form-group">
                	<label for="program_name">Basic Time Unit:</label>
                	<input class="form-control" type="text" name="time_length" id="time_length" placeholder="Enter Program's Length in Minutes" maxlength="2" />
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Note:</label>
                	<textarea class="form-control" name="program_notes" id="program_notes" placeholder="Program's Notes" style="resize: none; min-height: 150px;"></textarea>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button id="save_new_program" class="btn-u" type="button">Save</button>&nbsp;
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dt.sort.date-uk.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    function drawDatatable() {
		$('#create_program').click(function(e){
			$('#program_id').val(0);
			$('#program_title').html('Create Program');
			$('#program_name').val('');
			$('#program_notes').val('');
			$('#program_location_id').val('');
			$('#program_teacher_id').val('');
			$('#program_instrument_id').val('');
			$('#time_length').val('');
			$('#simplybook_program_id').val('');
			$('#add').modal('show');
		});
		$('#dtTable .edit').click(function(e){
			var program_name = ($(this).data('name'));
			$('#program_id').val($(this).data('id'));
			$('#program_location_id').val($(this).data('locationid'));
			//$('#program_teacher_id').val($(this).data('teacherid'));
			$('#program_teacher_id').val($(this).data('teacherid')).trigger('change');
			$('#program_instrument_id').val($(this).data('instrumentid'));
			$.getJSON('<?=base_url();?>admin/programs/'+$('#program_id').val(),
			{
			},function(e){
				$('#program_title').html('Edit Program');
				$('#program_name').val(program_name);
				$('#program_notes').val(e.results[0].notes);
				$('#program_location_id').val(e.results[0].location_id);
				//$('#program_teacher_id').val(e.results[0].teacher_id);
				$('#program_teacher_id').val(e.results[0].teacher_id).trigger('change');
				$('#program_instrument_id').val(e.results[0].instrument_id);
				$('#time_length').val(e.results[0].time_length);
				$('#simplybook_program_id').val($('#program_id').val());
				$('#add').modal('show');
			});
		});
		$('#dtTable .delete').click(function(e){
			var confirm_remove = confirm('Please confirm removal of current program');
			if (confirm_remove) {
				window.location = '<?=base_url();?>admin/remove_program/' + $(this).data('id');
			}
		});
	}
	jQuery.fn.ForceNumericOnly =
	function()
	{
		return this.each(function()
		{
			$(this).keydown(function(e)
			{
				var key = e.charCode || e.keyCode || 0;
				// allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
				// home, end, period, and numpad decimal
				return (
					key == 8 || 
					key == 9 ||
					key == 13 ||
					key == 46 ||
					key == 110 ||
					key == 190 ||
					(key >= 35 && key <= 40) ||
					(key >= 48 && key <= 57) ||
					(key >= 96 && key <= 105));
			});
		});
	};	
	jQuery(document).ready(function() {
        App.init();      
		$("#time_length").ForceNumericOnly();
		$('#program_teacher_id').select2();
		$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>admin/data/programs"
            },
			"columnDefs": [
				{ "bSortable": false, targets: 5 },
				{ "bSortable": false, targets: 6 }
            ],
            dom: 'Bfrtip',
			"fnDrawCallback": drawDatatable,
		});
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
				
		$('#save_new_program').click(function(e){
			var length = null;			
			var btu = $('#time_length').val();
			if ((btu<10)||(btu>60)) {
				alert('Basic Time Unit allowed to be between 10 to 60 minutes');
			} else {
				if (btu%5>0) {
					alert('Basic Time Unit should be with-in duplication of 5 minutes (10, 15, 20, 25, ... 55, 60)');
				} else {					
					$.post('<?=base_url();?>admin/set_program/' + $('#program_id').val(),
					{
						program_name: $('#program_name').val(),
						location_id: $('#program_location_id').val(),
						teacher_id: $('#program_teacher_id').val(),
						instrument_id: $('#program_instrument_id').val(),
						time_length: $('#time_length').val(),
						notes: $('#program_notes').val(),
						simplybook_program_id: $('#simplybook_program_id').val(),
						length: length,
					},function(e){
						$('#program_name').val('');
						$('#add').modal('hide');
						window.location.reload();
					});			
				}
			}
		});
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
