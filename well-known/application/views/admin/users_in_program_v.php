<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?> <?=$program['results'][0]->name?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<table id="dtTable">
            	<thead>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>E-mail</th>
                    <th>&nbsp;</th>
                </thead>
            </table>			
            <input type="button" value="Back" class="btn" onclick="window.location='<?=base_url();?>admin/view/programs';" />
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- Add / Edit Plan Modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <input type="hidden" name="program_id" id="program_id" value="<?=$program['results'][0]->id?>" />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="plan_title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
            	<fieldset class="form-group">
                	<label for="program_name">Program Name</label>
                	<input class="form-control" disabled type="text" name="program_name" id="program_name" placeholder="Enter Program's Name" />
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">User Name</label>
                	<input class="form-control" disabled type="text" name="user_name" id="user_name" placeholder="Enter User's Name" />
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Basic Time Unit</label>
                	<input class="form-control" disabled type="text" name="basic_time_unit" id="basic_time_unit" placeholder="Enter Basic Time Unit" />
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Basic Time Unit(s) for User*</label>
                	<select class="form-control" name="basictime_units" id="basictime_units" placeholder="Select Basic Time Units for User"></select>
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name"></label>
                	<input type="radio" name="time_interval" id="weekly_item" value="weekly">&nbsp;Weekly&nbsp;&nbsp;&nbsp;<input name="time_interval" value="biweekly" id="biweekly_item" type="radio">&nbsp;Bi-Weekly
                    &nbsp;&nbsp;&nbsp;<input name="time_interval" value="other" id="other_item" type="radio">&nbsp;Other
                </fieldset>
                <fieldset class="form-group">
                	<label for="program_name">Monthly Price*</label>
                	<input class="form-control" maxlength="5" type="text" name="price" id="price" placeholder="Monthly Price" maxlength="2" />
                </fieldset>
                <fieldset class="form-group">
                	<div class="plans">
                        <label for="previous_plans_title">Previous Plans</label><br>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
                        <tr>
                            <th>Date/Time</th>
                            <th>End Date/Time</th>
                            <th>Basic Time Unit(s)</th>
                            <th>Monthly Price</th>
                        </tr>
                        <tbody id="plans">
                        </tbody>
                        </table>
                        </div>
                    <div class="plans_message"></div>
                </fieldset>
                <div class="modal_status"></div>
            </div>
            <div class="modal-footer">
                <button id="save_plan" class="btn-u" type="button">Save</button>&nbsp;
                <button style="display:none;" id="deattach_plan" class="btn-u" type="button">De-attach</button>&nbsp;
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<!-- END OF Add / Edit Plan Modal -->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dt.sort.date-uk.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
	var user_id = 0;
	var status = '';
	var toMmDdYy = function(input) {
		var ptrn = /(\d{4})\-(\d{2})\-(\d{2})/;
		if(!input || !input.match(ptrn)) {
			return null;
		}
		return input.replace(ptrn, '$3/$2/$1');
	};
	function drawDatatable()
	{
		$('#dtTable .attach_to_program,#dtTable .edit_plan').click(function(e){
			$('.plans_message').html('');
			var th = $(this);
			user_id = ($(this).data('id'));
			var program_id = ($(this).data('programid'));
			status = ($(this).data('status'));
			var price = ($(this).data('price'));
			var basictime_units = ($(this).data('basictime_units'));
			var time_interval = ($(this).data('time_interval'));
			$.getJSON('<?=base_url();?>admin/user_plans/'+user_id+'/'+program_id,
			{
			},function(plans){
				var previous_plans = '';
				var previous_counter = 0;
				$.each(plans,function(k,v){
					if (v.endtime == '0000-00-00 00:00:00') {
					} else {
						previous_plans += '<tr>';
						previous_plans += '<td>'+toMmDdYy(v.starttime)+'</td>';
						previous_plans += '<td>'+toMmDdYy(v.endtime)+'</td>';
						previous_plans += '<td>'+v.basictime_units+'</td>';
						previous_plans += '<td>'+v.price+'</td>';
						previous_plans += '</tr>';
						previous_counter++;
					}					
				});
				if (previous_counter == 0) {
					$('.plans').hide();
				} else {
					$('#plans').html(previous_plans);
					$('.plans').show();
				}				
				if (status == 'attached') {
					$('#save_plan').hide();				
					$('#deattach_plan').show();
					var username = th.parent().parent().children('td'). html()+' '+th.parent().parent().children('td:nth-child(2)'). html();
					$.getJSON('<?=base_url();?>admin/programs/'+$('#program_id').val(),
					{
					},function(e){
						$('#program_name').val(e.results[0].name);
						$('#user_name').val(username);
						$('#basic_time_unit').val(e.results[0].time_length);
						$('#basictime_units').find('option').remove().end();
						$('#basictime_units').append($('<option>', {
							value: 1,
							text: '1 ('+e.results[0].time_length+' Minutes)'
						}));
						$('#basictime_units').append($('<option>', {
							value: 2,
							text: '2 ('+(e.results[0].time_length*2)+' Minutes)'
						}));
						$('#basictime_units').append($('<option>', {
							value: 3,
							text: '3 ('+(e.results[0].time_length*3)+' Minutes)'
						}));
						$('#price').val(price);
						$('#basictime_units').val(basictime_units);					
						$('#plan_title').html('DeAttach from program');					
						$('#price').prop('disabled',true);
						$('input[type=radio]').prop('disabled',true);
						$('#basictime_units').prop('disabled',true);
						$('#add').modal('show');
					});
				} else {
					$('#save_plan').show();
					$('#deattach_plan').hide();
					var username = th.parent().parent().children('td'). html()+' '+th.parent().parent().children('td:nth-child(2)'). html();
					$.getJSON('<?=base_url();?>admin/programs/'+$('#program_id').val(),
					{
					},function(e){
						$('#program_name').val(e.results[0].name);
						$('#user_name').val(username);
						$('#basic_time_unit').val(e.results[0].time_length);
						$('#basictime_units').find('option').remove().end();
						$('#basictime_units').append($('<option>', {
							value: 1,
							text: '1 ('+e.results[0].time_length+' Minutes)'
						}));
						$('#basictime_units').append($('<option>', {
							value: 2,
							text: '2 ('+(e.results[0].time_length*2)+' Minutes)'
						}));
						$('#basictime_units').append($('<option>', {
							value: 3,
							text: '3 ('+(e.results[0].time_length*3)+' Minutes)'
						}));
						$('#price').val(price);
						if (basictime_units == null) {
							basictime_units = 1;
						}
						$('#basictime_units').val(basictime_units);
						if (time_interval == 'weekly') {
							$('#weekly_item').prop('checked',true);
						}
						if (time_interval == 'biweekly') {
							$('#biweekly_item').prop('checked',true);
						}
						if (time_interval == 'other') {
							$('#other_item').prop('checked',true);
						}
						if (status == 'update') {
							$('#price').val(price);
							$('#plan_title').html('Update plan');
						} else {
							$('#plan_title').html('Attach to program');
						}					
						$('#price').prop('disabled',false);
						$('input[type=radio]').prop('disabled',false);
						$('#basictime_units').prop('disabled',false);
						$('#add').modal('show');
					});
				}	
			});					
		});
	}
    jQuery(document).ready(function() {
		$("#price").ForceNumericOnly();
        App.init();      		
		$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>admin/data/users_in_program/<?=$program['results'][0]->id?>"
            },
			"columnDefs": [
				{ "bSearchable": false, targets: 3 }
            ],
            dom: 'Bfrtip',
			"fnDrawCallback": drawDatatable,
		});
		$('#deattach_plan').click(function(e){
			var confirm_deattach = confirm('Please confirm deattaching user from program (Ending Plan)');
			if (confirm_deattach) {
				$.getJSON('<?=base_url();?>admin/set_to_program',
				{
					program_id: $('#program_id').val(),
					user_id: user_id,
					status: 'attached'
				},function(e){
					window.location.reload();
				});
			}
		});
		$('#save_plan').click(function(e){
			if (!$('input[name=time_interval]:checked').val() ) {
				alert('Please choose Time interval (Weekly/Bi-Weekly)');
			} else {
				if ($('#price').val()=='') {
					alert('Please set price');
				} else {
					$.getJSON('<?=base_url();?>admin/set_to_program',
					{
						program_id: $('#program_id').val(),
						user_id: user_id,
						status: status,
						basictime_units: $('#basictime_units').val(),
						time_interval: $('input[name=time_interval]:checked').val(),
						price: $('#price').val(),
					},function(e){
						window.location.reload();
					});
				}
			}
		});
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
	jQuery.fn.ForceNumericOnly =
	function()
	{
		return this.each(function()
		{
			$(this).keydown(function(e)
			{
				var key = e.charCode || e.keyCode || 0;
				// allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
				// home, end, period, and numpad decimal
				return (
					key == 8 || 
					key == 9 ||
					key == 13 ||
					key == 46 ||
					key == 110 ||
					key == 190 ||
					(key >= 35 && key <= 40) ||
					(key >= 48 && key <= 57) ||
					(key >= 96 && key <= 105));
			});
		});
	};
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
