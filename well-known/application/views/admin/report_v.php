<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<table id="dtTable">
            	<thead>
                	<?
					$arr = array();
                    switch($type)
					{
						case 'students': $arr = array('Date creation (dd/mm/yy)','Date edited (dd/mm/yy)','User','Name'); break;
						default: $arr = array('Date creation (dd/mm/yy)','Teacher','Student','Exercises');
					}
					
					foreach ($arr as $k=>$v)
					{
						echo '<th>'.$v.'</th>';
					}
					?>
                </thead>
            </table>			
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
    <!-- Large modal -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_exercises">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="myLargeModalLabel2" class="modal-title">Session's Exercises</h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Large modal -->

</div><!--/wrapper-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dt.sort.date-uk.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();      
		$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>admin/data/<?=$type?>"
            },
			"columnDefs": [
                { "type": "date-uk", targets: 0 }
            ],
            dom: 'Bfrtip',
		});
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
    });
	function generate_exercises_modal(session_id)
	{
		$.getJSON('<?=base_url();?>admin/exercises/'+session_id,
		function(e){
			var data = '<table>';
			data+='<tr><th>Exercise Name</th><th>Number of times sent to user</th></tr>';
			/*<th>First time sent to user</th></tr>';*/
			$(e).each(function(k,v){
				data += '<tr>';
				data += '<td>' + v.name + '</td>';
				data += '<td>' + v.num_of_times + '</td>';
				/*data += '<td>' + v.first_time + '</td>';*/
				data += '</tr>';
			});
			data+='</table>';
			$('#modal_exercises .modal-body').html(data);
			$('#modal_exercises').modal('show');
		});
	}
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
