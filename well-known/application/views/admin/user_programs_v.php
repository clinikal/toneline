<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html> 
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Edit User...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/select.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->     
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Edit User</h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="notes_full" style="position:fixed;top:0;left:0;z-index:500;"></div>

    <!--=== Content Part ===-->
    <div class="container content">
    	<div class="alert alert-block alert-danger fade in system-error-msg_ph" style="display:none;">
            <h4 class="system-error-msg"></h4>
            <a class="btn-u btn-u-blue" href="javascript:close_msg();">Close</a>
        </div>
        
        <div class="row">
            <!-- Begin Content -->
            <div class="col-md-12">
                <!-- Create New Exercise -->
                <form action="<?=base_url();?>admin/update_user_programs" class="sky-form" id="update_user_programs" name="update_user_programs" method="post">
                    <header>User Programs</header>
                    <input type="hidden" name="user_id" id="user_id" value="<?=$id?>">
                    <input type="hidden" name="selected_programs" id="selected_programs" value="<?=$id?>">
                    
                    <fieldset>
                        <section>
                            <label class="label">Programs *</label>
                            <label class="input">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="data_table" id="dtTable">
                                	<thead>
                                	<tr>
                                    	<th>&nbsp;</th>
                                    	<th>Program</th>
                                        <th>Teacher</th>
                                        <th>Location</th>
                                        <th>Instrument</th>
                                        <th>Length</th>
                                    </tr>
                                    </thead>
                                </table>                                
                            </label>
                        </section>
                        
                    </fieldset>
                    
                    <footer>
                        <button type="button" class="btn-u btn-u-default" onclick="window.location='<?=base_url();?>admin/view/users';">Back</button>
                    </footer>
                </form>
                <!-- General Unify Forms -->

                <div class="margin-bottom-60"></div>
            </div>
            <!-- End Content -->
            <div class="col-md-3">&nbsp;</div>
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

    <!--=== Footer Version 1 ===-->
    <?
		footer_f(array('page'=>$page));
	?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/dataTables.select.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dt.sort.date-uk.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    	function drawDatatable()
		{
		}
		var dtTable;
		var selected = [];
		<?
		foreach ($user_selected_programs as $k=>$v)
		{
			?>selected.push('dtr<?=$v->program_id?>');<?
		}
		?>		
		$(document).ready(function(e) {
			dtTable = $('#dtTable').DataTable({
				 "ajax": {
					"url": "<?=base_url();?>admin/data/user_programs/<?=$user_id?>"
				},
				"processing": true,
				"serverSide": true,
				/*select: {
					style:    'multi',
					selector: 'td:first-child'
				},*/
				"rowCallback": function( row, data ) {
					if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
						$(row).addClass('selected');
					}
				},
				"columnDefs": [{
						orderable: false,
						className: 'select-checkbox',
						targets:   0
					},{ "bSortable": false, targets: 2 },
					{ "bSortable": false, targets: 3 }
				],
				dom: 'Bfrtip',
				"fnDrawCallback": drawDatatable,
			});
		});
		jQuery(document).ready(function() {
		$('.button_submit').click(function(e){
			var selected_programs = new Array();
			dtTable.rows().eq(0).each( function (idx) {
  				var row = dtTable.row( idx );
				if ($(row.node()).hasClass('selected')) {
					var data = $(row.data());
					var program_id = data[0][1];
					selected_programs.push(program_id);
				}
			});
			$('#selected_programs').val(selected_programs);
			$('#update_user_programs').submit();
		});
        App.init();      
		
		$('#watch').on('hidden.bs.modal', function () {
			$("video").each(function () { this.pause() });
		});
		$('#listen').on('hidden.bs.modal', function () {
			$("audio").each(function () { this.pause() });
		});		
		/*$('form').submit(function () {
			window.onbeforeunload = null;
		});*/
    });
	function set_notes(filename)
	{
		$('.notes_full').css('width','100%');
		$('.notes_full').css('height','100%');
		if (filename.substr(filename.length - 3)=='pdf') {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="no" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		} else {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="yes" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		}
		$('body').css('overflow','hidden');
	}
	function close_msg() {
		$('.system-error-msg_ph').hide();
	}
	function close_notes()
	{
		$('.notes_full').css('width','0');
		$('.notes_full').css('height','0');
		$('.notes_full').html('');
		$('body').css('overflow','auto');
	}
	function remove_file(id,type)
	{
		$.post('<?=base_url();?>exercises/remove_file',
		{
			id: id,
			type: type
		},function(e){
			$('.view_file_'+type).hide();
			$('.attach_file_'+type).show();
		});
	}
	function closeEditorWarning(e){
		return ''
	}
	//window.onbeforeunload = closeEditorWarning;
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>