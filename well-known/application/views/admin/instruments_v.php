<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<table id="dtTable">
            	<thead>
                	<th>Name</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </thead>
            </table>			
            <input class="btn btn-success" id="add_new_instrument" type="button" value="Add New Instrument" />
    </div><!--/container-->		
    <!--=== End Content Part ===-->    

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <input type="hidden" name="instrument_id" id="instrument_id" value="0" />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Add New Instrument</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
                	<input class="form-control" type="text" name="instrument_name" id="instrument_name" placeholder="Enter Instrument's Name" />
                </div>
            </div>
            <div class="modal-footer">
                <button id="save_new_instrument" data-dismiss="modal" class="btn-u" type="button">Save</button>&nbsp;
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>js/jquery.dt.sort.date-uk.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    function drawDatatable() {
		$('#dtTable .edit').click(function(e){
			var instrument_name = ($(this).data('name'));
			$('#instrument_id').val($(this).data('id'));
			$('#instrument_name').val(instrument_name);
			$('#add').modal('show');
		});
		$('#dtTable .delete').click(function(e){
			var confirm_remove = confirm('Please confirm removal of current instrument');
			if (confirm_remove) {
				window.location = '<?=base_url();?>admin/remove_instrument/' + $(this).data('id');
			}
		});
	}
	jQuery(document).ready(function() {
        App.init();      
		$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>admin/data/instruments"
            },
			"columnDefs": [
				{ "bSortable": false, targets: 1 },
				{ "bSortable": false, targets: 2 }
            ],
            dom: 'Bfrtip',
			"fnDrawCallback": drawDatatable,
		});
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
		
		$('#add_new_instrument').click(function(e){
			$('#instrument_id').val(0);
			$('#instrument_name').val('');
			$('#add').modal('show');
		});		
		
		$('#save_new_instrument').click(function(e){
			$.post('<?=base_url();?>admin/set_instrument/' + $('#instrument_id').val(),
			{
				instrument_name: $('#instrument_name').val()
			},function(e){
				$('#instrument_name').val('');
				$('#add').modal('hide');
				window.location.reload();
			});			
		});
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
