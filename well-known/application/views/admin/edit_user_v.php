<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html> 
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Edit User...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->     
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Edit User</h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="notes_full" style="position:fixed;top:0;left:0;z-index:500;"></div>

    <!--=== Content Part ===-->
    <div class="container content">
    	<div class="alert alert-block alert-danger fade in system-error-msg_ph" style="display:none;">
            <h4 class="system-error-msg"></h4>
            <a class="btn-u btn-u-blue" href="javascript:close_msg();">Close</a>
        </div>
        
        <div class="row">
            <!-- Begin Content -->
            <div class="col-md-9">
                <!-- Create New Exercise -->
                <form action="<?=base_url();?>admin/update_user" enctype="multipart/form-data" class="sky-form" id="user_frm" method="post">
                    <header>Edit User</header>
                    <input type="hidden" name="user_id" id="user_id" value="<?=$id?>">
                    
                    <fieldset>
                        <section>

                            <div class="inline-group">
                                    <label class="checkbox">
                                        <input  <?if($user[0]->is_teacher == "1") echo "checked";?> type="checkbox" value="1" name="is_teacher"  >
                                        <i class=""></i> Teacher
                                    </label>
                            </div>
                        </section>
                        <section>
                            <label class="label">First Name *</label>
                            <label class="input">
                                <input type="text" name="first_name" id="first_name" value="<?=$user[0]->first_name?>">
                                <b class="tooltip tooltip-top-right">Enter first name</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Last Name *</label>
                            <label class="input">
                                <input type="text" name="last_name" id="last_name" value="<?=$user[0]->last_name?>">
                                <b class="tooltip tooltip-top-right">Enter last name</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">E-mail *</label>
                            <label class="input disabled">
                                <input type="text" name="email" id="email" class="disabled" value="<?=$user[0]->email?>" disabled>
                                <b class="tooltip tooltip-top-right">User's E-mail</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Phone *</label>
                            <label class="input">
                                <input type="text" name="phone" id="phone" class="" value="<?=$user[0]->phone?>">
                                <b class="tooltip tooltip-top-right">User's Phone</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Skype Id</label>
                            <label class="input">
                                <input type="text" name="skype_id" id="skype_id" value="<?=$user[0]->skype_id?>">
                                <b class="tooltip tooltip-top-right">Enter Skype Id</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Password</label>
                            <label class="input">
                                <input type="password" name="password" id="password">
                                <b class="tooltip tooltip-top-right">Enter Password, must contain 8 chars (blank = no change)</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Password Confirm</label>
                            <label class="input">
                                <input type="password" name="confirm_password" id="confirm_password">
                                <b class="tooltip tooltip-top-right">Re-Enter Password</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">SimplyBook.me User</label>
                            <label class="input">
                                <select name="simplybook_me_id" id="simplybook_me_id" class="form-control">
                                	<option value="0">Please Choose</option>
                                	<?
									foreach ($simplybook_users as $k=>$v)
									{
										?><option <? if ($v->id == $user[0]->simplybook_me_id) { ?>selected<? } ?> value="<?=$v->id?>"><?=$v->name?> (<?=$v->email?>)</option><?
									}
									?>
                                </select>
                            </label>
                        </section>
                        
                    </fieldset>
                    
                    <footer>
                        <button type="button" class="btn-u button_submit">Save</button>&nbsp;&nbsp;
                        <button type="button" class="btn-u btn-u-default" onclick="window.location='<?=base_url();?>admin/view/users';">Back</button>
                    </footer>
                </form>
                <!-- General Unify Forms -->

                <div class="margin-bottom-60"></div>
            </div>
            <!-- End Content -->
            <div class="col-md-3">&nbsp;</div>
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

    <!--=== Footer Version 1 ===-->
    <?
		footer_f(array('page'=>$page));
	?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<div class="modal fade" id="watch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Watch</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
            	<video width="500" height="375" controls class="video_modal_item">
            		<source src="<?=base_url();?>tl-files/video/<?=$exercise['video']?>" type="video/mp4">
                </video>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<div class="modal fade" id="listen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Listen</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
                <audio controls class="audio_modal_item">
                  <source src="<?=base_url();?>tl-files/audio/<?=$exercise['audio']?>" type="audio/mpeg">
                Your browser does not support the audio element.
                </audio>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-audio-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
		$('.button_submit').click(function(e){
			var first_name = $('#first_name').val();
			var last_name = $('#last_name').val();
			var password = $('#password').val();
			var confirm_password = $('#confirm_password').val();
			if ((first_name=='')||(last_name=='')) {
				$('.system-error-msg').html('Please enter First name and Last name');
				$('.system-error-msg_ph').show();
				window.scrollTo(0,0);
			} else {
				var submit_confirm = true;
				if (password == '') {
				} else {
					if (password.length<8) {
						$('.system-error-msg').html('Password must contain at least 8 chars');
						$('.system-error-msg_ph').show();
						window.scrollTo(0,0);
						submit_confirm = false;
					} else {
						if (password!=confirm_password) {
							$('.system-error-msg').html('Passwords do not match');
							$('.system-error-msg_ph').show();
							window.scrollTo(0,0);
							submit_confirm = false;
						}
					}
				}
				
				if (submit_confirm) {
					$('#user_frm').submit();
				}
			}
		});
        App.init();      
		
		$('#watch').on('hidden.bs.modal', function () {
			$("video").each(function () { this.pause() });
		});
		$('#listen').on('hidden.bs.modal', function () {
			$("audio").each(function () { this.pause() });
		});		
		/*$('form').submit(function () {
			window.onbeforeunload = null;
		});*/
    });
	function set_notes(filename)
	{
		$('.notes_full').css('width','100%');
		$('.notes_full').css('height','100%');
		if (filename.substr(filename.length - 3)=='pdf') {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="no" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		} else {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="yes" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		}
		$('body').css('overflow','hidden');
	}
	function close_msg() {
		$('.system-error-msg_ph').hide();
	}
	function close_notes()
	{
		$('.notes_full').css('width','0');
		$('.notes_full').css('height','0');
		$('.notes_full').html('');
		$('body').css('overflow','auto');
	}
	function remove_file(id,type)
	{
		$.post('<?=base_url();?>exercises/remove_file',
		{
			id: id,
			type: type
		},function(e){
			$('.view_file_'+type).hide();
			$('.attach_file_'+type).show();
		});
	}
	function closeEditorWarning(e){
		return ''
	}
	//window.onbeforeunload = closeEditorWarning;
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>