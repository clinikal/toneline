<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">My Lessons</h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    	<?		
		if ($cmd == 'schedule') {
		?>
		<div id="timeline" class="bootstrap">                    
			<div style="overflow:hidden;">
				<div class="form-group">
					<div class="row">
						<div id="step_info_container">
						<div class="col-md-4">
							<div id="date_time_header" class="step-header active" style="width: 100%;">
								<div class="step-title">
								<span class="step-number">1.</span>
								<div>
									<span class="step-name">Time</span>
									<div class="step-number-container">
										<span class="step-number">1.</span>
									</div>
									<span class="step-info"></span>
								</div>
							</div>
							</div>
						</div>                                
						<div class="col-md-8">
							<div id="client_info_header" class="step-header" style="width: 100%;">
							<div class="step-title">
							<span class="step-number">2.</span>
								<div>
									<span class="step-name">Details</span>
									<div class="step-number-container">
										<span class="step-number">2.</span>
									</div>
									<span class="step-info"></span>
								</div>
							</div>
							</div>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div id="datetimepicker12"></div>
						</div>
						<div class="col-md-8" id="time_select">
							<div id="available_times">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" id="set_a_lesson">
						
						</div>
					</div>
				</div>                    
			</div>
		</div>
		<!--<script type="text/javascript" src="//toneline.simplybook.me/iframe/pm_loader_v2.php?width=960&url=//toneline.simplybook.me&theme=bootstrap__joy_orchid&layout=bootstrap__joy&timeline=modern&mode=auto&mobile_redirect=0&hidden_steps=event,unit&event=10&unit=2"></script>-->
		<?
		} else {
			?>
            <div class="modal fade" id="lessonData" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <input type="hidden" name="event_id" id="event_id" value="0" />
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                            <h4 id="lesson_details" class="modal-title">Lesson Details</h4>
                        </div>
                        <div class="modal-body">
                            <fieldset class="form-group">
                                <label for="program_name">Program:</label>
                                <input class="form-control" type="text" name="program_name" id="program_name" disabled placeholder="" />
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="program_name">Teacher:</label><br>
                                <input class="form-control" type="text" name="teacher_name" id="teacher_name" disabled placeholder="" />
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="program_name">Date & Time:</label>
                                <input class="form-control" type="text" name="datetime" id="datetime" disabled placeholder="" />
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button id="cancel_lesson" class="btn-u btn-danger" type="button">Cancel Lesson</button>&nbsp;
                            <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
                        </div>
                      </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                	<?					
					if ((!$is_administrator)&&(sizeof($my_programs)>0)) {
                		?><input type="button" value="Schedule Next Lesson" class="btn btn-primary" id="schedule_next_lesson" /><?
					}
					?>
                </div>
                <div class="col-md-6" align="right">
                	<select name="filter_program" id="filter_program" class="form-control">
                    	<option value="0">Choose Program</option>
                    	<?
						$my_programs_data = array();
						foreach ($my_programs as $k=>$v)
						{
							if (isset($v->simplybook_program_id)) {
								$my_programs_data[] = array('simplybook_program_id'=>$v->simplybook_program_id,'name'=>$v->name);
							} else {
								$my_programs_data[] = array('simplybook_program_id'=>$v->program->simplybook_program_id,'name'=>$v->program->name);
							}
						}
						foreach($programs as $key=>$val) 
						{
							/*foreach ($my_programs as $k=>$v)
							{
								if ($v->program->simplybook_program_id == $program_id) {
									?><option selected value="<?=$v->program->simplybook_program_id?>"><?=$v->program->name?></option><?
								} else {
									?><option value="<?=$v->program->simplybook_program_id?>"><?=$v->program->name?></option><?
								}
							}*/
							//var_dump($my_programs_data);
							//exit();
							if (!$is_administrator) {
								$found = false;
								foreach ($my_programs_data as $k=>$v)
								{
									if ($v['simplybook_program_id'] == $val->id)
									{
										$found = true;
									}
								}
								if ($found) {
									?><option <?=($program_id==$val->id)?'selected':''?> value="<?=$val->id?>"><?=$val->name?></option><?
								}
							} else {
								?><option <?=($program_id==$val->id)?'selected':''?> value="<?=$val->id?>"><?=$val->name?></option><?
							}
						}						
						?>
                    </select>
                </div>
            </div>
			<br><br>
			<div id="calendar">
			</div>    	
			<?
		}
		?>
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
    
    <!-- Large modal -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_set_a_lesson">
    	<input type="hidden" name="next_lesson_date" id="next_lesson_date" value="">
        <input type="hidden" name="next_lesson_time" id="next_lesson_time" value="">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="myLargeModalLabel2" class="modal-title">Set a new lesson</h4>
                </div>
                <div class="modal-body">
                	<label class="input sky-form" style="width: 100%;">
                    	<p id="set_new_lesson_modal_txt">
                        	You're about to set a new lesson on the <span class="next_lesson_date_txt"></span> at <span class="next_lesson_time_txt"></span>,<br>Please confirm.
                            <br><br>
                            <span class="set_lesson_status"></span>
                    	</p>
                    </label>
                    <button type="button" class="btn-u button_save_lesson">Confirm Next Lesson</button>&nbsp;&nbsp;
                    <button type="button" class="btn-u btn-u-default" onclick="javascript:close_modal();">Back</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Large modal -->
<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
	function show_lesson(event_id,program_name,teacher_name,datetime,active)
	{
		if (!active) {
			$('#lesson_details').html('<font style="color:red;">Cancelled Lesson</font>');
			$('#cancel_lesson').hide();
		} else {
			var d = new Date();

			var month = d.getMonth()+1;
			var day = d.getDate();
			
			var today = d.getFullYear() + '/' +
				month + '/' +
				day;
				
			var datetime_tmp = datetime.split(' ');
			var d=new Date(datetime_tmp[0].split("/").reverse().join("-"));
			var dd=d.getDate();
			var mm=d.getMonth()+1;
			var yy=d.getFullYear();
			var newdate=yy+"/"+mm+"/"+dd;
			if (today>newdate) {
				$('#cancel_lesson').hide();
			} else {
				$('#lesson_details').html('Lesson Details');
				$('#cancel_lesson').show();
			}
		}
		$('#event_id').val(event_id);
		$('#program_name').val(program_name);
		$('#teacher_name').val(teacher_name);
		$('#datetime').val(datetime);
		$('#lessonData').modal('show');
	}
    jQuery(document).ready(function() {
        App.init();      		
		/*$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>students/users"
            },
			"columnDefs": [
                { "type": "date-uk", targets: 4 }
            ],
            dom: 'Bfrtip',
		});*/
		$('#cancel_lesson').click(function(e){			
			var d = new Date();

			var month = d.getMonth()+1;
			var day = d.getDate();
			var datetime_tmp = $('#datetime').val().split(' ');
			var d=new Date(datetime_tmp[0].split("/").reverse().join("-"));
			var dd=d.getDate();
			var mm=d.getMonth()+1;
			var yy=d.getFullYear();
			var newdate=yy+"/"+mm+"/"+dd;
			var today = d.getFullYear() + '/' +
				month + '/' +
				day;
			if (today==newdate) {
				alert('Your lesson has been booked for today. If you want to cancel, please contact your teacher');
			} else {
				var confirm_cancel = confirm('Please confirm cancellation of current lesson');
				if (confirm_cancel) {								
					$.post('<?=base_url();?>lessons/cancel/'+$('#event_id').val(),
					{
					},function(e){
						window.location.reload();
					});
				}
			}			
		});
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});		
		$('#schedule_next_lesson').click(function(e){
			var program_id = $('#filter_program').val();
			if (program_id == 0) {
				alert('Please choose program to schedule a lesson to');
			} else {
				window.location='<?=base_url();?>lessons/index/'+program_id+'/schedule';
			}
		});
		$('#filter_program').change(function(e){
			var program_id = $(this).val();
			if (program_id == 0) {
				window.location = '<?=base_url();?>lessons';
			} else {
				window.location = '<?=base_url();?>lessons/index/'+program_id;
			}
		});
		<?
		if ($events!=null) {
		?>		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			defaultDate: '<?=date('Y-m-d');?>',
			navLinks: true, // can click day/week names to navigate views
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: [
				<?
				$counter = 0;				
				foreach ($events as $k=>$v)
				{
					if (($v->event_id == $program_id)||($program_id == null)) {
						if ($v->is_confirm)
						{
							if ($counter>0) {
								echo ',';
							}
							if ($is_administrator) {
								$lesson_name = $v->client.' ('.$v->unit.')';
							} else {
								$lesson_name = $v->unit;
							}
							?>
							{
								title: '<?=str_replace("'","",$lesson_name)?>',
								start: '<?=date('Y-m-d\TH:i:s',strtotime($v->start_date));?>',
								url: 'javascript:show_lesson(<?=$v->id?>,"<?=$v->event?>","<?=$v->unit?>","<?=date('d/m/Y H:i',strtotime($v->start_date))?>",1);'
							}
							<?
							$counter++;
						}						
					}					
				}
				?>
			]
		});
		<?
		}
		?>
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
	function process_notes(id)
	{
		$('#notes_user_id').val('');
		if (id!='') {
			$.post('<?=base_url();?>students/pull_notes',
			{
				user_id: id
			},function(e){
				$('#notes').val(e)
				$('#notes_user_id').val(id);
				$('#modal_exercises').modal('show');
			});			
		}
	}
	function close_modal()
	{
		$('#next_lesson_date').val('');
		$('#next_lesson_time').val('');
		$('#modal_set_a_lesson').modal('hide');
	}	
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
<script type="text/javascript">
	$(function () {
		$('.button_save_lesson').click(function(e){
			$('.set_lesson_status').html('Please wait ...');
			$.getJSON('<?=base_url();?>lessons/set_lesson/<?=$program_id?>',
			{
				date: $('#next_lesson_date').val(),
				time: $('#next_lesson_time').val()
			},function(data){
				if (data.status) {
					$('.set_lesson_status').html('Lesson has been scheduled, Redirecting...');
					window.location = '<?=base_url();?>lessons/index/<?=$program_id?>';
				} else {
					$('.set_lesson_status').html(data.message);
				}
			});
		});
		$('#datetimepicker12').datepicker({
			inline: true,
			startDate: '+0d',
			sideBySide: false
		}).on('changeDate',function(e){
			$('#set_a_lesson').html('');
			$('#available_times').html('Loading ...');
			var selected_date = e.date;			
			selected_date = moment(selected_date,'yyyy-mm-dd').format();			
			$.getJSON('<?=base_url();?>lessons/get_available_times/<?=$program_id?>',
			{
				selected_date: selected_date
			},function(data){
				$('#next_lesson_date').val(data.date);
				$('.next_lesson_date_txt').html(data.date);
				$('#available_times').html('');
				if (data.times.length == 0) {
					$('#available_times').append(data.message);
				} else {
					$(data.times).each(function(k,v){
						$('#available_times').append('<div class="time-select-item">'+v+'</div>');
					});
				}
				$('#available_times div').click(function(e){					
					$('#next_lesson_time').val($(this).html());
					$('.next_lesson_time_txt').html($(this).html());
					var th = $(this);
					$('#available_times div').removeClass('active');
					th.addClass('active');
					$('#set_a_lesson').html('<input id="button_set_a_lesson" type="button" value="Set a lesson" />');
					$('#button_set_a_lesson').click(function(e){
						$('#modal_set_a_lesson').modal('show');
					});
				});
			});
		});
	});
</script>
</body>
</html> 
