<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->     
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Create new session</h1>           
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
    	<div class="alert alert-block alert-danger fade in system-error-msg_ph" style="display:none;">
            <h4 class="system-error-msg"></h4>
            <a class="btn-u btn-u-blue" href="javascript:close_msg();">Close</a>
        </div>
        
        <div class="row">
            <!-- Begin Content -->
            <div class="col-md-12">
                <!-- Create New Exercise -->
                <form class="sky-form" style="border: 0;" id="search_frm" method="post">
                <div class="row margin-bottom-30">                	
                    <div class="col-md-4">
                    <section>
                        <label class="input">
                            <input placeholder="Enter exercise name or description or parts of them" type="text" name="search" id="search" value="<?=$search?>">
                            <b class="tooltip tooltip-top-right">Enter exercise name or description or parts of them</b>
                        </label>
                    </section>
                    </div>
                    <div class="col-md-2">
                        <section>
                            <label class="select">
                                <select id="set_method" name="set_method">
                                    <option value="0" selected>All Methods</option>
                                    <?
                                    foreach ($methods as $k=>$v)
                                    {
                                        ?><option value="<?=$v->id?>" <? if ($method == $v->id) { ?>selected<? } ?>><?=$v->name?></option><?
                                    }
                                    ?>
                                </select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="col-md-2">
                        <section>
                            <label class="select">
                                <select id="set_sorting" name="set_sorting">
                                    <!--<option value="1" <? if ($sort == 1) { ?>selected<? } ?>>Sort by A-Z</option>
                                <option value="2" <? if ($sort == 2) { ?>selected<? } ?>>Sort by Z-A</option>-->
                                    <option value="3" <? if ($sort == 3) { ?>selected<? } ?>>Sort by Newest</option>
                                    <option value="4" <? if ($sort == 4) { ?>selected<? } ?>>Sort by Oldest</option>
                                </select>
                                <i></i>
                            </label>
                        </section>
                    </div>
                    <div class="col-md-1">
                    <section>
                        <button type="button" class="btn-u button_search" id="button_search">Search</button>
                    </section>
                    </div>
                    <div class="col-md-1">&nbsp;</div>

                </div>
                </form>
                <br>
                <form action="<?=base_url();?>sessions/update" class="" id="session_frm" method="post">
                	<div class="sky-form">
                	<input type="hidden" name="session_id" id="session_id" value="0">
                    <fieldset>
                    	<section>
                        	<label class="label">User to send session *</label>
                            <label class="input">
                            	<?
								$user_email = '';
								if ($target_user_id!=null) {
									$user_email = $target_user_id[0]->email;
								}
								?>
                               	<input value="<?=$user_email?>" type="text" name="session_target" maxlength="50" id="session_target" >
                                <b class="tooltip tooltip-top-right">User's e-mail to attach session to</b>                                
                                
                            </label>
                            <div class="note"><strong>Note:</strong> User's e-mail to attach session to</div>
                        </section>                        
                    </fieldset>
                    
                    <div class="row">
                    	<div class="col-md-12">
                        </div>
                    </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                        <fieldset>
                            <section style="min-height: 250px; max-height: 250px; overflow:scroll;overflow-x: hidden; border:solid 1px silver; padding: 15px;">
                                <label class="label label_exercises">Add Exercises to Session *</label>
                                <div class="row">
                                    <div>
                                        <div class="exercises_notin_session" id="exercises_before">
                                        <?
                                        foreach ($sessions['result'] as $k=>$v)
                                        {
                                        ?>
                                        <label class=""><input value="<?=$v->id?>" type="checkbox" id="exercise<?=$v->id?>" name="exercises[]">&nbsp;&nbsp;<?=$v->name?></label><br>
                                        <?
                                        }
                                        ?>
                                        </div>
                                    </div>                                        
                                </div>
                            </section>  
                            <br>
                            <button type="button" id="add_to_session" class="btn-u">Add to Session</button>
                            <br><br>
                        </fieldset>
                        </div>
                        <div class="col-md-6">
                        <fieldset>
                            <section style="min-height: 250px; max-height: 250px; overflow:scroll;overflow-x: hidden; border:solid 1px silver; padding: 15px;">
                                <label class="label label_exercises">Exercises added to Session (<span class="total_added"><?=sizeof($ex_in_session);?></span>) *</label>
                                <div class="row">
                                    <div>
                                        <div class="exercises_in_session" id="exercises_after">
                                            <?
                                            foreach ($ex_in_session as $k=>$v)
                                            {
                                                echo $v;
                                            }
                                            ?>
                                        </div>                                            
                                    </div>                                        
                                </div>
                            </section>
                            <br>
                            <button type="button" id="remove_from_session" class="btn-u btn-u-default">Remove from Session</button>
                            <br><br>
                        </fieldset>
                        </div>
                    </div>
                    <div class="sky-form">
                    <fieldset>
                    	<section>
                        	<label class="label">Comments</label>
                            <label class="textarea">
                                <textarea rows="3" name="session_comments" id="session_comments"></textarea>
                                <b class="tooltip tooltip-top-right">Enter comments and practicing instructions to the session</b>
                            </label>
                            <div class="note"><strong>Note:</strong> Enter comments and practicing instructions to the session</div>
                        </section>                        
                    </fieldset>
                    
                    <footer>
                        <button type="button" class="btn-u button_submit">Submit</button>
                        <button type="button" class="btn-u btn-u-default" onclick="window.location='<?=base_url();?>sessions/view/me';">Back</button>
                    </footer>
                    </div>
                </form>
                <!-- General Unify Forms -->

                <div class="margin-bottom-60"></div>
            </div>
            <!-- End Content -->
            <div class="col-md-3">&nbsp;</div>
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

    <!--=== Footer Version 1 ===-->
    <?
		footer_f(array('page'=>$page));
	?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
		var my_students = [
			<?
			foreach ($my_students['results'] as $k=>$v)
			{
			?>{ value: '<?=$v->email?>', data: '<?=$v->email?>' },<?
			}
			?>
		];


$('#session_target').autocomplete({
			minChars: 0,
			lookup: my_students,
			onSelect: function (suggestion) { }
		});
		$('#add_to_session').click(function(e){
			var cb = $("#exercises_before input[name='exercises\\[\\]']");
			var cb_after = $("#exercises_after input[name='exercises\\[\\]']");
			var total_added = parseInt($('.total_added').html());
			var items_to_add = '';
			$(cb).each(function(k,v){
				var id = $(v).attr('id');
				if ($('#exercises_before #'+id).prop('checked')) {
					var found = false;
					$(cb_after).each(function(key,val){
						if ($(v).attr('id') == $(val).attr('id')) {
							found = true;							
						}						
					});
					if (!found) {
						var store_id = $(v).attr('id');
						items_to_add += $(this).parent()[0].outerHTML;
						total_added++;
						$.post('<?=base_url();?>sessions/store',
						{
							id: id
						},function(e){							
						});
					}
				}
				$('.total_added').html(total_added)
			});
			$(cb).prop('checked',false);
			$('.exercises_in_session').append(items_to_add);
		});
		$('#remove_from_session').click(function(e){
			var cb = $("#exercises_after input[name='exercises\\[\\]']");
			var total_added = parseInt($('.total_added').html());
			$(cb).each(function(k,v){
				var id = $(v).attr('id');
				if ($('#exercises_after #'+id).prop('checked')) {
					total_added--;
					$(this).parent().remove();
					$.post('<?=base_url();?>sessions/store_remove',
					{
						id: id
					},function(e){						
					});					
				}
			});
			$('.total_added').html(total_added)
		});
		$("#session_target").keydown(function (e) {
			 if (e.keyCode == 32) { 
			   $(this).val($(this).val()); // append '-' to input
			   return false; // return false to prevent space from being added
			 }
		});
		/*$('form').submit(function () {
			window.onbeforeunload = null;
		});*/
		$('.button_submit').click(function(e){
			var session_target = $('#session_target').val();
			
			if (isEmail(session_target)) {
				var false_address = '';
				var counter = 0;
				var exercises = 0;
				
				if ($('#exercises_after').html()!='') {
					$.post('<?=base_url();?>sessions/validate_account',
					{
						account: session_target
					},function(e){
						if (e=='true') {
							$('#session_frm').submit();
						} else {
							if (e=='duplicate') {
								$('.system-error-msg').html('You may not send a session to yourself');
								$('.system-error-msg_ph').show();
								window.scrollTo(0,0);
							} else {
								$('.system-error-msg').html('Account was not found:\n'+false_address);
								$('.system-error-msg_ph').show();
								window.scrollTo(0,0);
							}
						}
					});
				} else {
					$('.system-error-msg').html('Please choose at least 1 exercise');
					$('.system-error-msg_ph').show();
					window.scrollTo(0,0);
				}
			} else {
				$('.system-error-msg').html('Account e-mail address is not valid');
				$('.system-error-msg_ph').show();
				window.scrollTo(0,0);
			}
		});
        App.init();      
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});		
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
    });
	function close_msg() {
		$('.system-error-msg_ph').hide();
	}
	function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	function closeEditorWarning(){
		return 'Are you sure?'
	}
	//window.onbeforeunload = closeEditorWarning;
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>