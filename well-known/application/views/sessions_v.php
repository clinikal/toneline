<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<?
			if (sizeof($sessions['result'])>0) {
			} else {
				if ($total_sessions>0) {
				?>
				<div class="alert alert-block alert-warning fade in">
					<h4>There are no sessions that match your search.</h4>
					<?
					/*if ($subtitle == 'Sessions created by me') {
						?><a class="btn-u btn-u-green" href="<?=base_url();?>sessions/create">Create first session</a><?
					}*/
					?>
				</div>
				<?
				}
			}
			?>
			
    		 <div class="alert alert-danger fade in remove_session" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>About to delete a session</h4>
                <p>Please note you are about to delete the session, click the button to confirm action or cancel</p>
                <p>
                    <a class="btn-u btn-u-red" href="javascript:complete_remove();">Delete session</a>&nbsp;&nbsp;
                    <a class="btn-u btn-u-blue" href="javascript:discard_action();">Cancel</a>
                </p>
            </div>            
            
        <!-- Funny Boxes -->
        	<?
			if ($total_sessions == 0) {
			?>
            <div class="alert alert-block alert-warning fade in">
                <?
				if ($type=='me') {
					?>
                    <h4>You haven't created yet sessions for you students</h4>
                    <a class="btn-u btn-u-green" href="<?=base_url();?>sessions/create">Create first session</a>
					<?
				} else {
					?><h4>Your teacher(s) haven’t sent you yet a session  of excercises to practice</h4><?
				}
				?>
                
            </div>        
            <?
			}
			
			if (($type!='me')&&($sessions['total'] == 0)) {
			?>
            <p class="margin-bottom-20">In order to get a session from your teacher(s), start by giving your teacher your Toneline user ID (the email with which you registered in Toneline).<br>
            Once your teacher will create a session for you, you will see it in this list. </p><br>
            
            <?
			}
			if ($sessions['total']>0) {
			?>
            <form class="sky-form" style="border: 0;" id="search_frm" method="post">
            <div class="row margin-bottom-30">                	
            	<div class="col-md-6">
                <section>
                    <label class="input">
                    	<? if ($type=='me') { ?>
                        <input placeholder="Enter student name or part of it" type="text" name="search" id="search" value="<?=$search?>">
                        <b class="tooltip tooltip-top-right">Enter student name or part of it</b>
                        <? } else { ?>
                        <input placeholder="Enter teacher name or part of it" type="text" name="search" id="search" value="<?=$search?>">
                        <b class="tooltip tooltip-top-right">Enter teacher name or part of it</b>
                        <? } ?>
                    </label>
                </section>
                </div>
                <div class="col-md-1">
                <section>
                    <button type="button" class="btn-u button_search" id="button_search">Search</button>
                </section>
                </div>
            	<div class="col-md-3">&nbsp;</div>
                <div class="col-md-2">
                <section>
                    <label class="select">
                        <select id="set_sorting" name="set_sorting">
                            <option value="3" <? if ($sort == 3) { ?>selected<? } ?>>Sort by Newest</option>
                            <option value="4" <? if ($sort == 4) { ?>selected<? } ?>>Sort by Oldest</option>
                        </select>
                        <i></i>
                    </label>
                </section>
                </div>
            </div>
            </form>
            <br>
            <?
			}
			?>

            <div class="row margin-bottom-30">
                <!-- Colored Funny Boxes -->
                	<div style="display:none;" class="col-md-12"><h2>Sessions</h2></div>
                    <?
					if ($sessions['total']>0) {
						foreach ($sessions['result'] as $k=>$v)
						{
						?>
                        <div class="col-md-12">
                            <div class="funny-boxes funny-boxes-top-sea">
                                <div class="row">
                                    <div class="col-md-3 funny-boxes-img">
                                        <a href="<?=base_url();?>exercises/session/<?=$v->id?>"><img alt="" src="<?=base_url();?>assets/img/bg/session_guitar.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="col-md-3">
                                        <h2><a href="<?=base_url();?>exercises/session/<?=$v->id?>"><?=date('d/m/Y',strtotime($v->datetime))?></a></h2>
                                        <p>
                                            <ul class="list-unstyled">
                                            <br>
                                            <?
											if ($type!='me') {
                                            	?>
                                                Session from <?=$v->teacher[0]->first_name?>&nbsp;<?=$v->teacher[0]->last_name?>
                                                <?
											} else {
												?>
                                                <h2>Sent to student</h2>
                                                <div style="height: 49px;">
                                                <?=$v->student[0]->first_name?>&nbsp;<?=$v->student[0]->last_name?><br>
                                                <?=$v->student[0]->email?>
												</div>
                                                <br>
                                                <input onclick="window.location='<?=base_url();?>sessions/duplicate/<?=$v->id?>';" type="button" value="Duplicate" class="btn-u btn-u-sea">&nbsp;&nbsp;
                                                <input onclick="javascript:remove_session(<?=$v->id?>);" type="button" value="Delete" class="btn-u btn-u-red">
												<?
											}
											?>
                                        <br><br>
                                        <ul class="list-unstyled funny-boxes-rating">
                                        </ul>
                                        </p>
                                    </div>
                                    <div class="col-md-3" style="font-size: 13px; max-height: 170px; overflow: hidden;">
                                    	<?
										if ($v->comments!='') {
											?><h2>Comments</h2><?
											echo nl2br($v->comments);
										}
										?>
                                    </div>
                                    <div class="col-md-3" style="font-size: 13px; max-height: 170px; overflow: hidden;">
                                    	<?
										$counter = 0;
										if (sizeof($v->exercises['result'])>0) {
											?><h2>Exercises (<?=sizeof($v->exercises['result'])?>)</h2><ul style="margin-left: 0px; padding-left: 17px;"><?
											foreach ($v->exercises['result'] as $key=>$val)
											{
												if ($counter<5) {
													if ($val->name == null) {
														echo '<li id="'.$val->id.'"><i style="color:red;">Exercise deleted</i></li>';
													} else {
														echo '<li id="'.$val->id.'">'.$val->name.'</li>';
													}
												}
												$counter++;
											}
											$total = sizeof($v->exercises['result']);
											$total_left = $total-5;
											if ($total>5) {
												echo $total_left.' more...';
											}
											?>
                                            </ul>
                                            <?
										}
										?>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <?
						}
						?>
                        <div class="row">&nbsp;</div>
                        <div class="row" style="display:none;">
                        	<div class="col-md-3"><a class="btn-u btn-u-default" href="<?=base_url();?>">Back</a></div>
                        </div>
                        <?
					}
					?>
                </div>
                <!--End Colored Funny Boxes -->
            </div>
        <!-- End Funny Boxes -->
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">


    function clean_state (){
        // Change the session->duplicate_state state
        // state other than "duplicate"
        // will make the page render itself form the session

        $.post('<?=base_url();?>sessions/duplicate_flag_clean',
            {
                state: "clean"
            },function(e){
            });
    }


    jQuery(document).ready(function() {


        App.init();

        clean_state();

		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
<!-- sessions_v.php -->