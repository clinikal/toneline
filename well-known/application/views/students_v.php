<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
    		<table id="dtTable">
            	<thead>
                	<th>First Name</th>
                    <th>Last Name</th>
                    <th>E-mail</th>
                    <th>Skype</th>
                    <th>Last Session(dd/mm/yy)</th>
                    <th>Last Session</th>
                	<th>Create New Session</th>
                    <th>Notes</th>
                </thead>
            </table>			
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
    
    <!-- Large modal -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal_exercises">
    	<input type="hidden" name="notes_user_id" id="notes_user_id" value="">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 id="myLargeModalLabel2" class="modal-title">User's Notes</h4>
                </div>
                <div class="modal-body">
                	<label class="input sky-form" style="width: 100%;">
                    <p><textarea name="notes" id="notes" style="width: 100%; min-height: 350px;"></textarea></p>
                    </label>
                    <button type="button" class="btn-u button_save_notes">Save</button>&nbsp;&nbsp;
                        <button type="button" class="btn-u btn-u-default" onclick="javascript:close_modal();">Back</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Large modal -->
</div><!--/wrapper-->

<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();      
		$('#dtTable').DataTable({
			 "ajax": {
                "url": "<?=base_url();?>students/users"
            },
			"columnDefs": [
                { "type": "date-uk", targets: 4 }
            ],
            dom: 'Bfrtip',
		});
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
		
		$('.button_save_notes').click(function(e){
			var user_id = $('#notes_user_id').val();
			if (user_id!='') {
			} else {
				alert('Error identifying user');
			}
			$.post('<?=base_url();?>students/save_notes',
			{
				user_id: $('#notes_user_id').val(),
				notes: $('#notes').val()
			},function(e){
				$('#notes').val('');
				$('#notes_user_id').val('');
				$('#modal_exercises').modal('hide');
			});
		});
    });
	function remove_session(session_id)
	{
		current_session_action_id = session_id;
		$('.remove_session').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_session_action_id = 0;
		$('.remove_session').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>sessions/remove/'+current_session_action_id;
	}
	function process_notes(id)
	{
		$('#notes_user_id').val('');
		if (id!='') {
			$.post('<?=base_url();?>students/pull_notes',
			{
				user_id: id
			},function(e){
				$('#notes').val(e)
				$('#notes_user_id').val(id);
				$('#modal_exercises').modal('show');
			});			
		}
	}
	function close_modal()
	{
		$('#notes').val('');
		$('#notes_user_id').val('');
		$('#modal_exercises').modal('hide');
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 
