<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Exercise Name: <?=$exercise[0]->name?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="notes_full" style="position:absolute;top:0;left:0;z-index:500;"></div>
    
    <!--=== News Block ===-->
    <div class="container content-sm" style="padding-top: 15px;">
        <div class="container">
            <div class="row">
            	<?
				if (isset($referer_page_type)) {
					?><div class="col-md-2" align="left"><button type="button" class="btn-u btn-u-default" onclick="window.location='<?=$referer_page_type?>';">Back</button></div><?
				} else {
            		?><div class="col-md-2" align="left"><button type="button" class="btn-u btn-u-default" onclick="window.location='<?=$back_url?>';">Back</button></div><?
				}
				?>
                <div class="col-md-10" align="right">
                	<?
					if ($exercise[0]->owner_id == $this->ion_auth->get_user_id()) {
						?>
                        <a href="javascript:view_recipients(<?=$exercise[0]->owner_id?>,<?=$exercise[0]->id?>);" class="btn-u"><i class="fa fa-users"></i>&nbsp;&nbsp;Recipients</a>
                        <?
					}
					?>
                	<? if ($exercise[0]->video!='') { ?>
                    	<a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#watch" class="btn-u"><i class="fa fa-youtube-play"></i>&nbsp;&nbsp;Watch</a>
                    <? } ?>
                    <? if ($exercise[0]->audio!='') { ?>
                    	<a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#listen" class="btn-u"><i class="fa fa-headphones"></i>&nbsp;&nbsp;Listen</a>
                    <? } ?>
                    <? if ($exercise[0]->link!='') { ?>
                    	<a href="<?=$exercise[0]->link?>" target="_blank" class="btn-u"><i class="fa fa-chain"></i>&nbsp;&nbsp;Link</a>
                    <? } ?>
                    <? if ($exercise[0]->notes!='') { ?>
                        <?
						$notes = $exercise[0]->notes;
						?>
                    	<a href="javascript:set_notes('<?=base_url();?>exercises/pdf/<?=$notes?>');" class="btn-u"><i class="fa fa-expand"></i>&nbsp;&nbsp;Full Screen Notes</a>
                    <? } ?>
                </div>
            </div>
            <?
				$notes = $exercise[0]->notes;
				if ($notes!='') {
			?>
            <br><br>
            <h2 class="title-v2">Exercise Description</h2>
            <p style="<? if ($exercise[0]->lang == 'he') { ?>direction: rtl; text-align: right;<? } ?>"><?=nl2br($exercise[0]->description);?></p>
            <br><br>
            <div class="row notes_ph">
            	<div class="panel panel-u">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-tasks"></i> Notes</h3>
                    </div>
                    <div class="panel-body">
                        <iframe src="<?=base_url();?>exercises/pdf/<?=$notes?>" frameborder="0" width="100%" height="500"></iframe>
                    </div>
                </div>
            </div>
            <?
				}
			?>
            <footer>
            	<?
				if (isset($referer_page_type)) {
					?><button type="button" class="btn-u btn-u-default" onclick="window.location='<?=$referer_page_type?>';">Back</button><?
				} else {
            		?><button type="button" class="btn-u btn-u-default" onclick="window.location='<?=$back_url?>';">Back</button><?
				}
				?>            	
            </footer>
        </div>
    </div>
    <!--=== End News Block ===-->

    <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->
<div class="modal fade" id="recipients" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Recipients</h4>
            </div>
            <div class="modal-body">
            	<div id="recipients_data">
            	
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<div class="modal fade" id="watch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Watch</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
                <video class="video-js vjs-default-skin" poster="" width="500" height="375" controls>
                    <source src="<?=base_url();?>tl-files/video/<?=$exercise[0]->video?>" type="video/mp4">
                    <!--<source src="http://player.h-cdn.org/static/mp4/tl-files/video/<?=$exercise[0]->video?>" type="video/mp4">-->
                </video>
                <script>
					window.hola_player();
				</script>
                <!--
            	<video width="500" height="375" controls class="video_modal_item">
            		<source src="<?=base_url();?>tl-files/video/<?=$exercise[0]->video?>" type="video/mp4">
                </video>
                -->
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<div class="modal fade" id="listen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Listen</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
                <audio controls class="audio_modal_item">
                  <source src="<?=base_url();?>tl-files/audio/<?=$exercise[0]->audio?>" type="audio/mpeg">
                Your browser does not support the audio element.
                </audio>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-audio-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        App.initCounter();
        App.initParallaxBg();
        StyleSwitcher.initStyleSwitcher();
		
		$('#watch,#listen').on('shown.bs.modal', function() {
			$('.notes_ph').hide();
		});
		$('#watch').on('hidden.bs.modal', function () {
			$('.notes_ph').show();
			$("video").each(function () { this.pause() });
		});
		$('#listen').on('hidden.bs.modal', function () {
			$('.notes_ph').show();
			$("audio").each(function () { this.pause() });
		});
    });
	function view_recipients(owner_id,exercise_id)
	{
		$.getJSON('<?=base_url();?>exercises/pull_recipients',
		{
			owner_id: owner_id,
			exercise_id: exercise_id
		},function(dt){
			console.log(dt.length);
			if (dt.length != null) {
				var data = '<table width="100%">';
				data+='<tr><th>Date Sent</th><th>Student\'s Full Name</th></tr>';
				$(dt).each(function(k,v){
					data+='<tr>';
					data+='<td>'+v.date+'</td>';
					data+='<td>'+v.fullname+'</td>';
					data+='</tr>';
				});
				data+='</table>';
				$('#recipients_data').html(data);
				$('#recipients').modal('show');
			} else {
				alert('This exercise has not recipients yet');
			}
		});			
	}
	function set_notes(filename)
	{
		$('.notes_full').css('width','100%');
		$('.notes_full').css('height','100%');
		if (filename.substr(filename.length - 3)=='pdf') {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="no" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		} else {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="yes" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		}
		$('body').css('overflow','hidden');
	}
	function close_notes()
	{
		$('.notes_full').css('width','0');
		$('.notes_full').css('height','0');
		$('.notes_full').html('');
		$('body').css('overflow','scroll-y');
	}
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>
<!-- exercise_v.php -->