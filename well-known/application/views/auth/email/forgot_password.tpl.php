<html>
<body style="font-family: Arial; font-size: 14px;">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
    	<td style="height: 50px; background-color: #eee;">&nbsp;</td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td>
        	<div align="center">
            <table cellpadding="0" cellspacing="0" border="0" width="600">
            <tr>
            <td style="font-family: Arial; font-size: 16px; text-align: left; direction: ltr; line-height: 24px;">
            <p>
            <strong style="font-size: 25px;">Dear ToneLine user,</strong><br>
            You've recently requested to reset your password for your ToneLine account.<br>
            Click the button below to reset it.
            <br><br>
            <div align="center">
            <a href="<?=base_url();?>auth/reset_password/<?=$forgotten_password_code?>" style="border:0; line-height: 30px; height: 30px; padding: 5px;font-size: 16px; font-weight: normal; text-decoration: none; background-color: #5fb611; color: white;">CLICK TO RESET PASSWORD</a>
            </div>
            <br>
            If you did not request a password reset, please ignore this email or reply to let us know. This password reset is only valid for the next 30 minutes.
            <br><br>
            Thanks,<br>
            ToneLine Support.
            </p>
            </td>
            </tr>
            </table>
            </div>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td style="height: 50px; background-color: #eee;">&nbsp;</td>
    </tr>
</body>
</html>