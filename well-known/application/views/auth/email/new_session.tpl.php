<html>
<body>
	<h1>Hey, You have got a new session!</h1>
	<p>
    	<?
			$user_fullname = '';
			foreach ($user as $k=>$v)
			{
				$user_fullname = $v->first_name.' '.$v->last_name;
			}
			$teacher_fullname = '';
			foreach ($teacher as $k=>$v)
			{
				$teacher_fullname = $v->first_name.' '.$v->last_name;
			}
		?>
    	Dear <?=$user_fullname?>,<br><br>
        Your ToneLine teacher <?=$teacher_fullname?> has sent you a session of exercises.<br>
        <br>
        Click <a href="<?=base_url();?>exercises/session/<?=$session_id?>">here</a> to enter the session.<br>
        Have a nice day and enjoy your playing,<br>
        ToneLine
	</p>
</body>
</html>