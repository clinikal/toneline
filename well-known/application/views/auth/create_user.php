<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | <?php echo lang('create_user_heading');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/pages/page_log_reg_v2.css">    

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
</head> 

<body>
<!--=== Content Part ===-->    
<?php echo form_open("auth/create_user");?>
<div class="container">
    <!--Reg Block-->
    <div class="reg-block">
        <div class="reg-block-header">
            <h2>ToneLine | <?php echo lang('create_user_heading');?></h2>
            <p>Please enter the user's information below.</p>            
        </div>

        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" placeholder="First Name" name="firstname" value="<?=$firstname['value'];?>" id="firstname">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="<?=$lastname['value'];?>" id="lastname">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="text" class="form-control" placeholder="Email" name="email" value="<?=$email['value']?>" id="email">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            <input type="phone" class="form-control" placeholder="Phone" name="phone" value="<?=$phone['value']?>" id="phone">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-skype"></i></span>
            <input type="text" class="form-control" placeholder="Skype Id" name="skype_id" value="<?=$skype_id['value']?>" id="skype_id">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" class="form-control" placeholder="Password" value="<?=$password['value'];?>" name="password" id="password">
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" class="form-control" placeholder="Confirm Password" value="<?=$password_confirm['value'];?>" name="password_confirm" id="password_confirm">
        </div>
        <hr>
        <div id="infoMessage"><?php echo $message;?></div>

        <div class="checkbox">
            <label>
                <input <? if ($terms_and_conditions['value']=='on') { ?>checked<? } ?> type="checkbox" name="terms_and_conditions" id="terms_and_conditions"> 
                <p>I agree to the <a href="#">Terms and conditions</a></p>
            </label>            
        </div>
                                
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <button type="submit" name="submit" class="btn-u btn-block">Create Account</button>
            </div>            
        </div>
        <div class="row">&nbsp;</div>
        <div align="center" class="row"><p>
        	Already own an account? <a href="<?=base_url();?>">Click here</a><br><br>
        	<a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
        </div>
    </div>
    <!--End Reg Block-->
</div><!--/container-->
<?php echo form_close();?>
<!--=== End Content Part ===-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/countdown/jquery.countdown.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/backstretch/jquery.backstretch.min.js"></script>
<script type="text/javascript">
    $.backstretch([
      "<?=base_url();?>assets/img/bg/guitar.jpg",
      ], {
        fade: 1000,
        duration: 7000
    });
</script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();      
    });
</script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29166220-1']);
  _gaq.push(['_setDomainName', 'htmlstream.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html> 