<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>ToneLine | Welcome...</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/theme-colors/default.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/custom.css">
    <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29166220-1']);
  _gaq.push(['_setDomainName', 'my.tone-line.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head> 

<body>
<div class="wrapper">
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page));
	?>
    <!--=== End Header ===-->    

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Welcome </h1>
            <ul class="pull-right breadcrumb">
                <li><a href="<?=base_url();?>">Home</a></li>
                <li class="active">Welcome</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
        <!-- Funny Boxes -->
            <div class="alert alert-block alert-warning fade in" style="display:none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <h4>Info!</h4>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
            </div>        

            <p class="margin-bottom-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><br>

            <div class="row margin-bottom-30">
                <!-- Bordered Funny Boxes -->
                <div class="col-md-6">
                	<h2>Sessions</h2>
                    <?
					if ($sessions!=null) {
						foreach ($sessions as $k=>$v)
						{
						?>
						<div class="funny-boxes funny-boxes-top-blue">
							<div class="row">
								<div class="col-md-4 funny-boxes-img">
									<img alt="" src="<?=base_url();?>assets/img/bg/guitar.jpg" class="img-responsive">
									<ul class="list-unstyled">
									   <li><i class="fa fa-briefcase"></i> <?=$v->datetime?></li>
									</ul>
								</div>
								<div class="col-md-8">
									<h2><a href="<?=base_url();?>sessions/index/<?=$v->id?>">3 Exercises</a></h2>
									
								</div>
							</div>                            
						</div>
						<?
						}
						?>
                        <div class="row">
                        	<div class="col-md-12"><a class="btn-u btn-u-blue" href="<?=base_url();?>sessions/create">Create new session</a></div>
                        </div>
                        <?
					} else {
						?>
                        <div class="row">
                        	<div class="col-md-12">No sessions were found.</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-12"><a class="btn-u btn-u-blue" href="<?=base_url();?>sessions/create">Create first session</a></div>
                        </div>
                        <?
					}
					?>
                </div>
                <!-- End Bordered Funny Boxes -->

                <!-- Colored Funny Boxes -->
                <div class="col-md-6">
                	<h2>Exercises</h2>
                    <?
					if ($exercises!=null) {
						foreach ($exercises as $k=>$v)
						{
						?>
                        <div class="funny-boxes funny-boxes-top-sea">
                            <div class="row">
                                <div class="col-md-4 funny-boxes-img">
                                    <img alt="" src="<?=base_url();?>assets/img/bg/guitar.jpg" class="img-responsive">                                                                    </div>
                                <div class="col-md-8">
                                    <h2><a href="#"><?=$v->name?></a></h2>
                                    <p>
                                    	<ul class="list-unstyled">
                                       <?
									   $exercise_type = $v->exercise_type_id;
									   $exercise_type_name = '';
									   foreach ($exercise_types as $key=>$val)
									   {
										   if ($val->id==$exercise_type) {
											   $exercise_type_name = $val->name;
										   }
									   }
									   ?>
                                       <? if ($exercise_type_name!='') { ?>
                                       	 <li><i class="fa fa-briefcase"></i> Exercise Type: <?=$exercise_type_name?></li>
                                       <? } ?>
                                    </ul>
										<?
                                        if ($v->description!='')
										{
											echo $v->description;
										} else {
											?><i>No description were found</i><?
										}
										?>
                                    <br><br>
                                    <ul class="list-unstyled funny-boxes-rating">
                                    	<li><i class="icon-custom icon-sm rounded-x icon-bg-grey icon-line fa fa-music"></i></li>
                                        <li><i class="icon-custom icon-sm rounded-x icon-bg-u icon-line fa fa-youtube-play "></i></li>
                                        <li><i class="icon-custom icon-sm rounded-x icon-bg-u icon-line fa fa-headphones"></i></li>
                                        <li><i class="icon-custom icon-sm rounded-x icon-bg-u icon-line fa fa-mail-forward"></i></li>
                                        <li><i class="icon-custom icon-sm rounded-x icon-bg-u icon-line fa">3</i></li>
                                    </ul>
                                    </p>
                                </div>
                            </div>                            
                        </div>
                        <?
						}
						?>
                        <div class="row">
                        	<div class="col-md-12"><a class="btn-u btn-u-green" href="<?=base_url();?>exercises/create">Create new exercise</a></div>
                        </div>
                        <?
					} else {
						?>
                        <div class="row">
                        	<div class="col-md-12">No exercises were found.</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-12"><a class="btn-u btn-u-green" href="<?=base_url();?>exercises/create">Create first exercise</a></div>
                        </div>
						<?
					}
					?>
                </div>
                <!--End Colored Funny Boxes -->
            </div>
        <!-- End Funny Boxes -->
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<?=base_url();?>assets/plugins/back-to-top.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="<?=base_url();?>assets/js/custom.js"></script>
<!-- JS Page Level -->           
<script type="text/javascript" src="<?=base_url();?>assets/js/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();      
    });
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 