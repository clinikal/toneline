<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->    
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left"><?=$subtitle?></h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
        <!-- Funny Boxes -->
        	<? if ($this->session->flashdata('message')!='') { ?>
        	<div class="alert alert-block alert-warning fade in">
                <h4><?=$this->session->flashdata('message');?></h4>
            </div>    
            <? } ?>

        <?
        if ($total_exercises_created == 0 && $subtitle != 'Exercises created by ToneLine School') {
            ?>
				<div class="alert alert-block alert-warning fade in">
					<?
					if ($subtitle == 'Exercises created by me') {
						?><h4>You haven't created exercises</h4>
                            <a class="btn-u btn-u-green" href="<?=base_url();?>exercises/create">Create first exercise</a>
                        <?
					} else {
						?><h4>No exercises were sent to you</h4><?
					}
					?>
				</div>
				<?
			} else {
				if ($exercises['total']>0) {
				} else {				
				?>
				<div class="alert alert-block alert-warning fade in">
					<h4>There are no excercises that match your selection.</h4>
					<?
					if (($subtitle == 'Exercises created by me')&&($total_exercises_created == 0)) {
						?><a class="btn-u btn-u-green" href="<?=base_url();?>exercises/create">Create first exercise</a><?
					}
					?>
				</div>
				<?				
				}
			}
			
			if ($subtitle == 'Exercises created by me') {
			} else {
			if ($exercises['total']>0)
			{} else {
				if ($total_exercises_created == 0 && $exercise_id!= 'tl') {
			?>
            <p class="margin-bottom-20">Start by giving your teacher your Toneline user ID (the email with which you registered in Toneline).<br>
            Once your teacher will create a session for you, you will see its excercises in this list.</p><br>
            <?
				}
			}
			}
			?>
        	<? if (isset($comments) && ($comments!='')) { ?>
            <div class="alert alert-block alert-warning fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <h4>Comments for this session</h4>
                <?=nl2br($comments)?>
            </div>        
            <? } ?>

            <div class="alert alert-danger fade in remove_exercise" style="display:none;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>About to delete an exercise</h4>
                <p>Please note you are about to delete the exercise, click the button to confirm action or cancel</p>
                <p>
                    <a class="btn-u btn-u-red" href="javascript:complete_remove();">Delete exercise</a>&nbsp;&nbsp;
                    <a class="btn-u btn-u-blue" href="javascript:discard_action();">Cancel</a>
                </p>
            </div>            
            
            <?
			/*if ($exercises['total']>0) {*/
			if (($search_panel) && (($total_exercises_created>0 || $exercises['total'] > 0  ) || $exercise_id =="tl") ) {
			?>
            <form class="sky-form" style="border: 0;" id="search_frm" method="post">
            <div class="row margin-bottom-30">                	
            	<div class="col-md-4">
                <section>
                    <label class="input">
                        <input placeholder="Enter exercise name or description or parts of them" type="text" name="search" id="search" value="<?=$search?>">
                        <b class="tooltip tooltip-top-right">Enter exercise name or description or parts of them</b>
                    </label>
                </section>
                </div>
                <div class="col-md-2">
                    <section>
                        <label class="select">
                            <select id="set_method" name="set_method">
                                <option value="0" selected>All Methods</option>
                                <?
                                foreach ($methods as $k=>$v)
                                {
                                    ?><option value="<?=$v->id?>" <? if ($method == $v->id) { ?>selected<? } ?>><?=$v->name?></option><?
                                }
                                ?>
                            </select>
                            <i></i>
                        </label>
                    </section>
                </div>
                <div class="col-md-2">
                    <section>
                        <label class="select">
                            <select id="set_sorting" name="set_sorting">
                                <!--<option value="1" <? if ($sort == 1) { ?>selected<? } ?>>Sort by A-Z</option>
                            <option value="2" <? if ($sort == 2) { ?>selected<? } ?>>Sort by Z-A</option>-->
                                <option value="3" <? if ($sort == 3) { ?>selected<? } ?>>Sort by Newest</option>
                                <option value="4" <? if ($sort == 4) { ?>selected<? } ?>>Sort by Oldest</option>
                            </select>
                            <i></i>
                        </label>
                    </section>
                </div>
                <div class="col-md-1">
                <section>
                    <button type="button" class="btn-u button_search" id="button_search">Search</button>
                </section>
                </div>
                <div class="col-md-1">&nbsp;</div>

            </div>
            </form>
            <br>
            <?
			}
			?>

            <div class="row margin-bottom-30">
                <!-- Colored Funny Boxes -->
                	<div style="display:none;" class="col-md-12"><h2><?=$subtitle?></h2></div>
                    <?
					if ($exercises['total']>0) {
						$cnt = 0;
						$org = array();
						foreach ($exercises['result'] as $k=>$v)
						{
							if (is_object($v)) {
							} else {
								$v = $v[0];
							}
						?>
                        <div class="col-md-12">
                            <div class="funny-boxes funny-boxes-top-sea">
                                <div class="row">
                                    <div class="col-md-3 funny-boxes-img">
                                        <a href="<?=base_url();?>exercises/view/<?=$v->id?>/<?=$exercise_id?>"><img alt="" src="<?=base_url();?>assets/img/bg/guitar.jpg" class="img-responsive"  title="Click to practice"></a>
                                        </div>
                                    <div class="col-md-3">
                                        <h2 <? if ($v->lang == 'he') { ?>style="direction: rtl; text-align: right"<? } ?>><a title="Click to practice" href="<?=base_url();?>exercises/view/<?=$v->id?>/<?=$exercise_id?>"><?=$v->name?></a></h2>
                                        <p>
                                            <ul class="list-unstyled">
											   <?
                                               $exercise_type_name = '';
											   $exercise_type = '';
                                               foreach ($exercise_types as $key=>$val)
                                               {
                                                   if ($val->id==$exercise_type) {
                                                       $exercise_type_name = $val->name;
                                                   }
                                               }
                                               ?>
                                               <? if ($exercise_type_name!='') { ?>
                                                 <li><i class="fa fa-briefcase"></i> Exercise Type: <?=$exercise_type_name?></li>
                                               <? } ?>
                                            </ul>
											<div style="height: 39px;">&nbsp;</div>
                                            <ul class="list-unstyled funny-boxes-rating">
                                                <li><i class="icon-custom icon-sm rounded-x <? if ($v->notes!='') { ?>icon-bg-u<? } else { ?>icon-bg-grey <? } ?> icon-line fa fa-music" title="Notes"></i></li>
                                                <li><i class="icon-custom icon-sm rounded-x <? if ($v->video!='') { ?>icon-bg-u<? } else { ?>icon-bg-grey <? } ?> icon-line fa fa-youtube-play " title="Video"></i></li>
                                                <li><i class="icon-custom icon-sm rounded-x <? if ($v->audio!='') { ?>icon-bg-u<? } else { ?>icon-bg-grey <? } ?> icon-line fa fa-headphones" title="Audio"></i></li>
                                                <li><i class="icon-custom icon-sm rounded-x <? if ($v->link!='') { ?>icon-bg-u<? } else { ?>icon-bg-grey <? } ?> icon-line fa fa-chain" title="Link"></i></li>
                                            </ul>
                                            <br>
                                            <?
											if ($panel) {
											?>
                                            <input onclick="window.location='<?=base_url();?>exercises/edit/<?=$v->id?>';" type="button" value="Edit" class="btn-u">&nbsp;&nbsp;
                                            <input onclick="window.location='<?=base_url();?>exercises/duplicate/<?=$v->id?>';" type="button" value="Duplicate" class="btn-u btn-u-yellow">&nbsp;&nbsp;
                                            <input onclick="javascript:remove_exercise(<?=$v->id?>);" type="button" value="Delete" class="btn-u btn-u-red">
                                            <?
											}
											?>
                                        </p>
                                    </div>
                                    <div class="col-md-3" style="font-size: 16px; max-height: 170px; overflow: hidden; <? if ($v->lang == 'he') { ?>direction: rtl; text-align: right;<? } ?>">
                                    	<?=nl2br($v->description)?>
                                    </div>
                                    <div class="col-md-3 funny-boxes-img right_side_data">
                                    	<div>
                                    	<strong>Created On: </strong> <?=date('d/m/Y',strtotime($v->created_datetime));?>
                                        <br><br>

                                            <? if ($v->exercise_methods!=null) { ?>
                                                <strong>Method: </strong>
                                                <?
                                                $counter = 0;
                                                foreach ($v->exercise_methods as $key=>$val)
                                                {
                                                    if ($exercises['methods']!=null) {
                                                        foreach ($exercises['methods'] as $key_in=>$val_in)
                                                        {
                                                            if ($val_in->id == $val->method_id) {
                                                                if ($counter>0) {
                                                                    echo ', ';
                                                                }
                                                                echo $val_in->name;
                                                                $counter++;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                ?><strong>Method: </strong> <i>Not Set</i><?
                                            } ?>
                                            <br><br>
										<? if ($v->music_style!='') { ?>
                                    	<strong>Style:</strong> <?=$v->music_style?><br>
                                        <? } else { ?>
                                        <strong>Style:</strong> <i>Not Set</i><br>
                                        <? } ?>
                                        <br>
                                        <? if ($v->role!='') { ?>
                                        <strong>Role:</strong> <?=$v->role?>
                                        <? } else {
										?><strong>Role:</strong> <i>Not Set</i><?
										}?>
                                        <br><br>
                                        <? if ($v->exercise_types!=null) { ?>
                                        <strong>Type: </strong>
                                        <?
										$counter = 0;
										foreach ($v->exercise_types as $key=>$val)
										{
											if ($exercises['types']!=null) {												
												foreach ($exercises['types'] as $key_in=>$val_in)
												{
													if ($val_in->id == $val->type_id) {
														if ($counter>0) {
															echo ', ';
														}
														echo $val_in->name;
														$counter++;
													}
												}
											}
										}
										} else {
											?><strong>Type: </strong> <i>Not Set</i><?
										} ?>

                                        <br>
                                        <br>
                                        </div>                                        
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <?
							if ($org!='') {
								$v = $org;
							}
							$cnt++;
						}
						?>
                        <div class="row">&nbsp;</div>
                        <div class="row">
                        	<!--<div class="col-md-2"><a class="btn-u btn-u-green" href="<?=base_url();?>exercises/create">Create new exercise</a></div>
                            &nbsp;&nbsp;-->
                            <?
							if (($subtitle=='Exercises created by me')||($subtitle=='Exercises sent by Toneline teachers'))
							{
							} else {
								if (isset($referer_page_type)) {
									?><div class="col-md-3"><a class="btn-u btn-u-default" href="<?=$referer_page_type?>">Back</a></div><?
								} else {
									?><div class="col-md-3"><a class="btn-u btn-u-default" href="<?=base_url();?>">Back</a></div><?
								}
							}
							?>
                        </div>
                        <?
					} else {
						?>
                        <?
						if ($subtitle == 'Exercises created by me') {
						?>
                        <br>
                        <div class="row">
                        	<div class="col-md-12"></div>
                        </div>
						<?
						}
					}
					?>
                </div>
                <!--End Colored Funny Boxes -->
            </div>
        <!-- End Funny Boxes -->
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
</div><!--/wrapper-->

<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init(); 
		$('#set_sorting').change(function(e){
			$('#search_frm').submit();
		});		
		$('#button_search').click(function(e){
			$('#search_frm').submit();
		});
    });
	var current_exercise_action_id = 0;
	function remove_exercise(exercise_id)
	{
		current_exercise_action_id = exercise_id;
		$('.remove_exercise').show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	function discard_action()
	{
		current_exercise_action_id = 0;
		$('.remove_exercise').hide();
	}
	function complete_remove()
	{
		window.location = '<?=base_url();?>exercises/remove/'+current_exercise_action_id;
	}
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 