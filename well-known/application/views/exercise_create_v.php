<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Check if is login as user from admin to edit methoda
$CI = & get_instance();
$s = $CI->session->userdata('logged_as_admin');
$isLoginAsUser=false;
if (isset($s)) {
    $isLoginAsUser=true;
}

?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(array('page'=>$page,'first_name'=>$first_name,'is_administrator'=>$is_administrator,'total_sessions_created_by_me'=>$total_sessions_created_by_me,'is_teacher'=>$is_teacher));
	?>
    <!--=== End Header ===-->     
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Exercises</h1>            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="notes_full" style="position:fixed;top:0;left:0;z-index:500;"></div>

    <!--=== Content Part ===-->
    <div class="container content">
    	<div class="alert alert-block alert-danger fade in system-error-msg_ph" style="display:none;">
            <h4 class="system-error-msg"></h4>
            <a class="btn-u btn-u-blue" href="javascript:close_msg();">Close</a>
        </div>
        
        <div class="row">
            <!-- Begin Content -->
            <div class="col-md-9">
                <!-- Create New Exercise -->
                <form action="<?=base_url();?>exercises/update" enctype="multipart/form-data" class="sky-form" id="exercise_frm" method="post">
                    <header>
                    	<? if ($state == 'Create') { ?>
                        	Create New Exercise
                        <? } else { ?>
                        	Edit Exercise
                        <? } ?>
                    </header>
                    <input type="hidden" name="exercise_id" id="exercise_id" value="<?=$exercise['id']?>">
                    
                    <fieldset>
                        <section>
                            <label class="label">Name *</label>
                            <label class="input">
                                <input type="text" name="exercise_name" id="exercise_name" value="<?=$exercise['name']?>">
                                <b class="tooltip tooltip-top-right">Enter the full name of the exercise</b>
                            </label>
                        </section>
                        
                        <section>
                            <label class="label">Description</label>
                            <label class="textarea">
                                <textarea rows="5" name="exercise_desc" id="exercise_desc"><?=$exercise['description']?></textarea>
                                <b class="tooltip tooltip-top-right">Enter the full description of the exercise</b>
                            </label>
                            <div class="note"><strong>Note:</strong> Please enter the full description of the exercise</div>
                        </section>
                        
                        <section>
                            <label class="label">Add Notes</label>
                            <?
							$hide_file = false;
							?>
                            <? if ($exercise['notes']!='') { ?>
                                <div class="view_file_notes"><a href="javascript:set_notes('<?=base_url();?>exercises/pdf/<?=$exercise['notes']?>');">View File</a>, <a href="javascript:remove_file(<?=$exercise['id']?>,'notes');" target="_blank">Remove File</a></div>
                                <?
                                $hide_file = true;
                                ?>
                            <? } ?>
                            <div class="attach_file_notes" <? if ($hide_file) { ?>style="display:none;"<? } ?>>
                                <label for="file" class="input input-file">                            	
                                        <div class="button"><input name="exercise_notes" id="exercise_notes" accept="application/pdf,image/gif,image/jpeg,image/png" type="file" id="file" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input type="text" readonly>
                                        <b class="tooltip tooltip-top-right">Use ToneLine notes, PDF, JPEG or PNG Files</b>                            </label>
                                <div class="note"><strong>Note:</strong> Use ToneLine notes, PDF, JPEG or PNG Files</div>
                            </div>
                        </section>                  
                        
                        <section>
                            <label class="label">Add Video</label>
                            <?
							$hide_file = false;
							?>
                            <? if ($exercise['video']!='') { ?>
                                <div class="view_file_video"><a data-toggle="modal" data-target="#watch" href="#" target="_blank">View File</a>, <a href="javascript:remove_file(<?=$exercise['id']?>,'video');" target="_blank">Remove File</a></div>
                                <?
                                $hide_file = true;
                                ?>
                            <? } ?>
                            <div class="attach_file_video" <? if ($hide_file) { ?>style="display:none;"<? } ?>>
                            <label for="file" class="input input-file">
                                <div class="button"><input type="file" name="exercise_video" accept="video/mp4" id="exercise_video" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input type="text" readonly>
                                <? if ($exercise['video']!='') { ?>
                                	<a data-toggle="modal" data-target="#watch" href="#" target="_blank">View File</a>
                                <? } ?>
                                <b class="tooltip tooltip-top-right">Load MP4 file</b>
                            </label>                            
                            <div class="note"><strong>Note:</strong> Load MP4 file</div>
                            </div>
                        </section>                        
                        
                        <section>
                            <label class="label">Add Audio</label>
                            <?
							$hide_file = false;
							?>
                            <? if ($exercise['audio']!='') { ?>
                                <div class="view_file_audio"><a data-toggle="modal" data-target="#listen" href="" target="_blank">View File</a>, <a href="javascript:remove_file(<?=$exercise['id']?>,'audio');" target="_blank">Remove File</a></div>
                                <?
                                $hide_file = true;
                                ?>
                            <? } ?>
                            <div class="attach_file_audio" <? if ($hide_file) { ?>style="display:none;"<? } ?>>
                            <label for="file" class="input input-file">
                                <div class="button"><input type="file" accept="audio/*" name="exercise_audio" id="exercise_audio" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input type="text" readonly>
                                <b class="tooltip tooltip-top-right">Load MP3 file</b>
                            </label>
                            <div class="note"><strong>Note:</strong> Load MP3 file</div>
                            </div>
                        </section>
                        
                        <section>
                            <label class="label">Add Link</label>
                            <label class="input">
                                <input type="text" name="exercise_link" id="exercise_link" value="<?=$exercise['link']?>" placeholder="http://www.youtube.com">
                                <b class="tooltip tooltip-top-right">Use YouTube / Vimeo links</b>
                            </label>
                            <div class="note"><strong>Note:</strong> Use YouTube / Vimeo links</div>
                        </section>
                        <section>
                            <label class="label">Exercise Method</label>
                            <div class="inline-group">
                                <?
                                $counter = 1;
                                $disabled = false;
                                foreach ($methods as $k=>$v)
                                {
                                    $marked = false;
                                    if ($exercise_methods!=null) {
                                        foreach ($exercise_methods as $key=>$val)
                                        {
                                            if ($val->method_id == $v->id) {
                                                $marked = true;
                                            }
                                        }
                                    }
                                    if(!$is_administrator && !$is_tonelineschool && !$isLoginAsUser){
                                        if ($exercise_methods==null) {
                                            if ($v->id == 1) {
                                                $marked = true;
                                            }

                                        }
                                        $disabled = true;
                                    }

                                   ?>
                                    <label class="checkbox">
                                        <input  <? if ($marked) { ?>checked<? } ?> type="checkbox" value="<?=$v->id?>" name="exercise_methods[]"  <? if ($disabled) { ?>disabled<? } ?>>
                                        <i class=""></i><?=$v->name?>
                                    </label>
                                    <?
                                    $counter++;
                                }
                                if($disabled && $exercise_methods==null){ //Add private method?>
                                    <input  type="hidden" value="1" name="exercise_methods[]">
                                <?}elseif($disabled && $exercise_methods!=null){
                                    foreach ($exercise_methods as $key=>$val)
                                    {?>
                                            <input  type="hidden" value="<?=$val->method_id?>" name="exercise_methods[]">

                                    <?}
                                }
                                ?>
                            </div>
                        </section>
                        
                        <section>
                            <label class="label">Music Style</label>
                            <div class="inline-group">
                            	<?
								$counter = 1;
								foreach ($music_styles as $k=>$v)
								{									
								?><label class="radio"><input <? if ($exercise['music_style_id']==$v->id) { ?>checked<? } ?> type="radio" name="music_style" value="<?=$v->id?>"><i class="rounded-x"></i><?=$v->name?></label><?
								$counter++;
								}
								?>
                            </div>
                        </section>
                        
                        <section>
                            <label class="label">Exercise Type</label>
                            <div class="inline-group">
                            	<?
								$counter = 1;
								foreach ($types as $k=>$v)
								{
									$marked = false;
									if ($exercise_types!=null) {
										foreach ($exercise_types as $key=>$val)
										{
											if ($val->type_id == $v->id) {
												$marked = true;
											}
										}
									}
								?>
                                <label class="checkbox"><input <? if ($marked) { ?>checked<? } ?> type="checkbox" value="<?=$v->id?>" name="exercise_types[]"><i class=""></i><?=$v->name?></label>
                                <?
								$counter++;
								}
								?>
                            </div>
                        </section>

                        
                        <section>
                            <label class="label">Role</label>
                            <div class="inline-group">
                            	<?
								$counter = 1;
								foreach ($role as $k=>$v)
								{
								?>
                                <label class="radio"><input <? if ($exercise['role_id']==$v->id) { ?>checked<? } ?> type="radio" name="exercise_role" value="<?=$v->id?>"><i class="rounded-x"></i><?=$v->name?></label>
                                <?
								$counter++;
								}
								?>
                            </div>
                        </section>
                    </fieldset>
                    
                    <footer>
                        <button type="button" class="btn-u button_submit">Save</button>&nbsp;&nbsp;
                        <button type="button" class="btn-u btn-u-default" onclick="window.location='<?=base_url();?>exercises/view/me';">Back</button>
                    </footer>
                </form>
                <!-- General Unify Forms -->

                <div class="margin-bottom-60"></div>
            </div>
            <!-- End Content -->
            <div class="col-md-3">&nbsp;</div>
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

    <!--=== Footer Version 1 ===-->
    <?
		footer_f(array('page'=>$page));
	?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<div class="modal fade" id="watch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Watch</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
            	<video width="500" height="375" controls class="video_modal_item">
            		<source src="<?=base_url();?>tl-files/video/<?=$exercise['video']?>" type="video/mp4">
                </video>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-video-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<div class="modal fade" id="listen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="myModalLabel1" class="modal-title">Listen</h4>
            </div>
            <div class="modal-body">
            	<div align="center">
                <audio controls class="audio_modal_item">
                  <source src="<?=base_url();?>tl-files/audio/<?=$exercise['audio']?>" type="audio/mpeg">
                Your browser does not support the audio element.
                </audio>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-u btn-u-default close-audio-modal" type="button">Close</button>
            </div>
          </div>
    </div>
</div>
<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
		$('.button_submit').click(function(e){
			var exercise_name = $('#exercise_name').val();
			if (exercise_name=='') {
				$('.system-error-msg').html('Please enter exercise name');
				$('.system-error-msg_ph').show();
				window.scrollTo(0,0);
			} else {				
				$.post('<?=base_url();?>exercises/check_duplicate_name',$('#exercise_frm').serialize(),function(e){				
					if (e=='1') {
						$('#exercise_frm').submit();
					} else {
						$('.system-error-msg').html('Exercise name already exist, please set another name');
						$('.system-error-msg_ph').show();
						window.scrollTo(0,0);
					}
				});
			}
		});
        App.init();      
		
		$('#watch').on('hidden.bs.modal', function () {
			$("video").each(function () { this.pause() });
		});
		$('#listen').on('hidden.bs.modal', function () {
			$("audio").each(function () { this.pause() });
		});		
		/*$('form').submit(function () {
			window.onbeforeunload = null;
		});*/
    });
	function set_notes(filename)
	{
		$('.notes_full').css('width','100%');
		$('.notes_full').css('height','100%');
		if (filename.substr(filename.length - 3)=='pdf') {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="no" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		} else {
			$('.notes_full').html('<div style="background-color:white; cursor: pointer; font-size: 16px; font-weight: bold;" onclick="javascript:close_notes();" align="center">CLOSE</div><iframe scrolling="yes" style="height: 100% !important;background-color: black;" frameborder="0" width="100%" height="100%" src="'+filename+'"></iframe>');
		}
		$('body').css('overflow','hidden');
	}
	function close_msg() {
		$('.system-error-msg_ph').hide();
	}
	function close_notes()
	{
		$('.notes_full').css('width','0');
		$('.notes_full').css('height','0');
		$('.notes_full').html('');
		$('body').css('overflow','auto');
	}
	function remove_file(id,type)
	{
		$.post('<?=base_url();?>exercises/remove_file',
		{
			id: id,
			type: type
		},function(e){
			$('.view_file_'+type).hide();
			$('.attach_file_'+type).show();
		});
	}
	function closeEditorWarning(e){
		return ''
	}
	//window.onbeforeunload = closeEditorWarning;
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>