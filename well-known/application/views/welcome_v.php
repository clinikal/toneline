<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<? $this->load->view('libs/header_v');?>
    <!--=== Header ===-->    
    <?
		header_h(
			array(
				'page'=>$page,
				'first_name'=>$first_name,
				'is_administrator'=>$is_administrator,
				'total_sessions_created_by_me'=>$total_sessions_created_by_me,
                'is_teacher'=>$is_teacher
			)
		);
	?>
    <!--=== End Header ===-->   
    
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Welcome to Toneline</h1>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">		
        <!-- Funny Boxes -->
            <div class="alert alert-block alert-warning fade in" style="display:none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <h4>Info!</h4>
                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.
            </div>        

            <p class="margin-bottom-20">You can use Toneline in several ways</p>

            <div class="row margin-bottom-30">
                <!-- Bordered Funny Boxes -->
                <div class="col-md-12">
                    <? if ($is_teacher== 1){?>
                        <h2>Teachers</h2>
                        <div class="funny-boxes funny-boxes-top-blue">
                            <div class="row">
                                <div class="col-md-2"><h2 style="text-transform: uppercase; line-height: 30px;"><a href="<?=base_url();?>exercises/create">Create exercises</a></h2></div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-7">
                                    Start by creating your collection of musical exercises in Toneline.<br>
                                    An exercise can include one or more of the following components:<br>
                                    Notes <br>
                                    An audio file<br>
                                    A video file<br>
                                    A link to a Youtube clip<br>
                                    Give your exercise a name and a description, and set its parameters. <br>
                                </div>
                                <div class="col-md-2"><a class="btn-u btn-u-blue" href="<?=base_url();?>exercises/create">Create Exercises</a></div>
                            </div>
                        </div>
                        <div class="funny-boxes funny-boxes-top-blue">
                            <div class="row">
                                <div class="col-md-2"><h2 style="text-transform: uppercase; line-height: 30px;"><a href="<?=base_url();?>sessions/create/">Create sessions for your students</a></h2></div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-7">
                                    A session is a set of several exercises which you send to each one of your students periodically.<br>
                                    The recommended use is creating a weekly session of approximately 5 exercises for each student.<br>
                                    Create a session by selecting the required exercises from your collection and attaching them to your student.<br>
                                    Toneline will send the session to your student, in order for him/her to practice the exercises during the coming week.<br>
                                </div>
                                <div class="col-md-2"><a class="btn-u btn-u-blue" href="<?=base_url();?>sessions/create">Create Sessions</a></div>
                            </div>
                        </div>
                    <?} else{ ?>
                        <h2>Students</h2>
                        <div class="funny-boxes funny-boxes-top-sea">
                            <div class="row">
                                <div class="col-md-2"><h2 style="text-transform: uppercase; line-height: 30px;"><a href="<?=base_url();?>sessions/view/me">Give your ID</a></h2></div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-7">
                                    Start by giving your teacher your Toneline user ID (the email with which you registered in Toneline).<br>
                                    Your teacher will send you a session of exercises periodically.<br>
                                    Open your last session and practice each of the musical exercises in the session. <br>
                                    The recommended use is getting a session of approximately 5 exercises every week.<br>
                                </div>
                                <div class="col-md-2"><a class="btn-u btn-u" href="<?=base_url();?>sessions/view/me">My Sessions</a></div>
                            </div>
                        </div>
                        <div class="funny-boxes funny-boxes-top-sea">
                            <div class="row">
                                <div class="col-md-2"><h2 style="text-transform: uppercase; line-height: 30px;"><a href="<?=base_url();?>sessions/index/">Create exercises</a></h2></div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-7">
                                    In addition, you can use Toneline to create your own musical exercises.<br>
                                    An exercise can include one or more of the following components:<br>
                                    Notes <br>
                                    An audio file<br>
                                    A video file<br>
                                    A link to a Youtube clip<br>
                                    Give your exercise a name and a description, and set its parameters.<br>
                                    After creating your exercises, you can use Toneline to play and practice your exercises.  <br>
                                </div>
                                <div class="col-md-2"><a class="btn-u btn-u" href="<?=base_url();?>exercises/create">Create Exercises</a></div>
                            </div>
                        </div>
                    <?}?>
                </div>
                <!--End Colored Funny Boxes -->
            </div>
        <!-- End Funny Boxes -->
    </div><!--/container-->		
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
     <?
		footer_f(array('page'=>$page));
	 ?>     
    <!--=== End Footer Version 1 ===-->
<? $this->load->view('libs/footer_v');?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();      
    });
</script>
<!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/plugins/respond.js"></script>
    <script src="<?=base_url();?>assets/plugins/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html> 