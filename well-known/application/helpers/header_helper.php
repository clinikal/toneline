<?
function header_h($data)
{
	$CI = & get_instance();
	$s = $CI->session->userdata('logged_as_admin');
	if (isset($s)) {
		?>
		<div class="logged_as_admin alert alert-danger fade in">
			You are logged as Administrator, Don't forget to <a href="welcome/logout">Logout</a>
		</div>
		<?
	}
	?>
    <div class="header">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <!-- Topbar Navigation -->
                <ul class="loginbar pull-right">
                	<li><a href="<?=base_url();?>" style="font-weight: bold;">HELLO <?=$data['first_name']?></a></li>
                    <li class="topbar-devider"></li>
                    <!--   
                    <li><a href="#">Edit Account</a></li>  
                    <li class="topbar-devider"></li>   
                    -->
                    <li><a href="mailto:yonibleich@gmail.com">Support</a></li>  
                    <li class="topbar-devider"></li>   
                    <li><a href="<?=base_url();?>welcome/logout">Logout</a></li>   
                </ul>
                <!-- End Topbar Navigation -->
            </div>
        </div>
        <!-- End Topbar -->
    
        <!-- Navbar -->
        <div class="navbar navbar-default mega-menu" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="<?=base_url();?>">
                    	<h1 style="padding:0;margin:0;">ToneLine</h1>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li class="<? if ($data['page']=='home') { ?>active<? } ?>">
                            <a href="<?=base_url();?>">
                                Home
                            </a>                            
                        </li>
                        <!-- My Lessons -->
                        <li class="<? if ($data['page']=='my_lessons') { ?>active<? } ?>">
                            <a href="<?=base_url();?>lessons">
                                My Lessons
                            </a>                            
                        </li>
                        <!-- End Home -->
                        <?
						if ($data['is_administrator'])
						{
							?>
                            <!-- Sessions -->                        
                            <li class="<? if ($data['page']=='admin') { ?>active<? } ?> dropdown">
                                <a href="<?=base_url();?>">
                                    Admin
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=base_url();?>admin/view/teachers">Sessions Sent in ToneLine</a></li>
                                    <li><a href="<?=base_url();?>admin/view/students">Exercises Created and Edited in ToneLine</a></li>
                                    <li><a href="<?=base_url();?>admin/view/users">Registered Users</a></li>
                                    <li><a href="<?=base_url();?>admin/view/programs">Programs</a></li>
                                    <li><a href="<?=base_url();?>admin/view/locations">Locations</a></li>
                                    <li><a href="<?=base_url();?>admin/view/instruments">Instruments</a></li>
                                </ul>
                            </li>
                            <!-- End Sessions -->
                            <?
						}
						?>
                        <!-- Home -->
                         <? if ($data['is_teacher'] == 1) {?>
                                <li class="<? if ($data['page']=='students') { ?>active<? } ?>">
                                    <a href="<?=base_url();?>students/view">
                                        Students
                                    </a>
                                </li>
                                <?}?>
                                <!-- End Home -->

                        <!-- Sessions -->                        
                        <li class="<?
                        if ($data['page']=='sessions') { ?>active<? } ?> dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                Sessions
                            </a>
                            <ul class="dropdown-menu">
                            	<li><a href="<?=base_url();?>sessions/view/all">Sessions created by my teachers</a></li>
                               <? if ($data['is_teacher'] == 1)
                                {?>
                                    <li><a href="<?=base_url();?>sessions/view/me">Sessions created by me</a></li>
                                    <li><a href="<?=base_url();?>sessions/create">Create new session</a></li>
                                <?}?>
                            </ul>
                        </li>
                        <!-- End Sessions -->

                        <!-- Exercises -->
                        <li class="<? if ($data['page']=='exercises') { ?>active<? } ?> dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                Exercises
                            </a>
                            <ul class="dropdown-menu">
                            	<li><a href="<?=base_url();?>exercises/view/all">Exercises sent by Toneline teachers</a></li>
                                <li><a href="<?=base_url();?>exercises/view/me">Exercises created by me</a></li>
                                <? if($data['is_teacher'] == 1){?>
                                    <li><a href="<?=base_url();?>exercises/view/tl">Exercises created by ToneLine School</a></li>
                                <?}?>
                                <li><a href="<?=base_url();?>exercises/create">Create new exercise</a></li>
                            </ul>
                        </li>
                        <!-- End Exercises -->

                        <!-- Search Block -->
                        <li style="display:none;">
                            <i class="search fa fa-search search-btn"></i>
                            <div class="search-open">
                                <div class="input-group animated fadeInDown">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <span class="input-group-btn">
                                        <button class="btn-u" type="button">Go</button>
                                    </span>
                                </div>
                            </div>    
                        </li>
                        <!-- End Search Block -->
                    </ul>
                </div><!--/navbar-collapse-->
            </div>    
        </div>            
        <!-- End Navbar -->
    </div>

    <?php

}
?>