<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function send_email($recipient, $sender, $subject, $message, $bcc = null)
{
    require_once("phpmailer/class.phpmailer.php");

    $mail = new PHPMailer();
    $body = $message;
    $mail->IsSMTP();
	$mail->FromName = $sender;
    $mail->From = $sender;
    $mail->Subject = $subject;
    $mail->AltBody = strip_tags($message);
    $mail->MsgHTML($body);
	$mail->CharSet = 'UTF-8';
	$mail->AddAddress($recipient);
	if ($bcc!=null) {
		foreach ($bcc as $k=>$v)
		{
			$mail->AddBCC($v);
		}
	}
	$mail->SMTPAuth   = false;

    if ( ! $mail->Send())
    {
		echo "Mailer Error: " . $mail->ErrorInfo;
        echo 'Failed to Send';
		exit();
    }
    else
    {
        //echo 'Mail Sent';
    }
}
?>