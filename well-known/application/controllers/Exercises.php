<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercises extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
		$groups = $this->ion_auth->get_users_groups()->row()->id;
		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
        $this->is_tonelineschool = false;
        if ($this->ion_auth->user()->row()->id == General_m::TONELINE_SCHOOL_USER) //tonelineschool
        {
            $this->is_tonelineschool=$this->ion_auth->user()->row()->id = true;
        }
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
	}
	
	public function remove_file()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		
		$exercise = $this->general_m->pull_exercise_by_id($id,$this->ion_auth->get_user_id());
		if (($exercise[0]->notes!='')&&($type=='notes')) {
			unlink('./tl-files/notes/'.$exercise[0]->notes);
			$this->general_m->remove_file($id,'notes');
		}
		if (($exercise[0]->audio!='')&&($type=='audio')) {
			unlink('./tl-files/audio/'.$exercise[0]->audio);
			$this->general_m->remove_file($id,'audio');
		}
		if (($exercise[0]->video!='')&&($type=='video')) {
			unlink('./tl-files/video/'.$exercise[0]->video);
			$this->general_m->remove_file($id,'video');
		}
	}
	
	public function set_sort($type,$sort_val)
	{
		// 1 - A-Z
		// 2 - Z-A
		// 3 - Newest
		// 4 - Oldest
		
		$data = array(
                   'sort'  => $sort_val
               );

		$this->session->set_userdata($data);
		redirect(base_url().'exercises/view/me');
	}
	
	public function remove($exercise_id)
	{
		$exercise = $this->general_m->pull_exercise_by_id($exercise_id,$this->ion_auth->get_user_id());
		if ($exercise!=null) {
			if ($exercise[0]->notes!='') {
				if (file_exists('./tl-files/notes/'.$exercise[0]->notes)) {
					unlink('./tl-files/notes/'.$exercise[0]->notes);
				}
			}
			if ($exercise[0]->audio!='') {
				if (file_exists('./tl-files/audio/'.$exercise[0]->audio)) {
					unlink('./tl-files/audio/'.$exercise[0]->audio);
				}
			}
			if ($exercise[0]->video!='') {
				if (file_exists('./tl-files/video/'.$exercise[0]->video)) {
					unlink('./tl-files/video/'.$exercise[0]->video);
				}
			}
		}
		
		$this->general_m->remove_exercise($exercise_id,$this->ion_auth->get_user_id());
		redirect(base_url().'exercises/view/me');
	}
	
	public function pdf($filename = null)
	{
		if ($filename!=null) {
			if (substr($filename,strlen($filename)-3,3)=='pdf') {
				?><embed src="<?=base_url();?>tl-files/notes/<?=$filename?>" width="100%" height="100%" type='application/pdf'></embed><?
			} else {
				?><div align="center"><img style="max-width: 100%;" src="<?=base_url();?>tl-files/notes/<?=$filename?>"></div><?
			}
		}
	}
	
	public function session($session_id = null,$ref = null)
	{
		$data = '';
		$data['is_administrator'] = $this->is_administrator;
        $data['is_tonelineschool'] = $this->is_tonelineschool;
        $data['is_teacher'] = $this->is_teacher;
		$data['page'] = 'Exercises';
		$data['panel'] = false;
		$data['search_panel'] = false;
		$data['search'] = '';
		$data['exercise_types'] = $this->general_m->pull_exercise_types();
        $data['exercise_methods'] = $this->general_m->pull_exercise_methods();
		$data['total_exercises_created'] = 1;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		
		if ($session_id == null) {
			redirect(base_url().'sessions/view/me');
		} else {
			$data['session'] = $this->general_m->pull_session($session_id,$this->ion_auth->get_user_id());
			
			$data['exercises'] = $this->general_m->pull_session_exercises($session_id,null);
			$data['title'] = 'Exercises';
			/*****
			Complete
			*****/
			if ($data['session'] == null) {
				redirect(base_url().'sessions/view/me');
				exit();
			}
			$target_user = $data['session']['target_user'][0]->first_name.' '.$data['session']['target_user'][0]->last_name;
			if ($data['session']['type']=='me') {
				$data['subtitle'] = '<strong>Session</strong>: Date '.date('d/m/Y',strtotime($data['session']['result'][0]->datetime)).', Sent to '.$target_user;
				$data['breadcrumbs'] = array(
					'Sessions created by me' => base_url().'sessions/view/me'
				);
				if ($ref!=null) {
					$data['referer_page_type'] = base_url().'students/view';
				} else {
					$data['referer_page_type'] = base_url().'sessions/view/me';
				}
			} else {
				$data['subtitle'] = '<strong>Session</strong>: Date '.date('d/m/Y',strtotime($data['session']['result'][0]->datetime)).', Sent by '.$target_user;
				$data['breadcrumbs'] = array(
					'Sessions created by my teachers' => base_url().'sessions/view/all'
				);
				$data['referer_page_type'] = base_url().'sessions/view/all';
			}
			$data['panel'] = false;
			$data['exercise_id'] = 'me';
			$data['first_name'] = $this->first_name;
			$data['comments'] = $this->general_m->pull_session_comments($session_id);
			$this->load->view('exercises_v',$data);
		}
	}
	
	public function pull_recipients()
	{
		$owner_id = $this->input->get('owner_id');
		$exercise_id = $this->input->get('exercise_id');
		
		if ($owner_id == $this->ion_auth->get_user_id()) {
			$data = $this->general_m->pull_recipients($exercise_id);
			
			if ($data['recipients'] == 'true') {
				echo '[';
				$counter = 0;
				foreach ($data['sessions'] as $k=>$v)
				{
					if ($counter>0) {
						echo ',';
					}
					$fullname = '';
					foreach ($data['students'] as $key=>$val)
					{					
						if ($val[0]->id == $v->student_id)
						{
							$fullname = $val[0]->first_name.' '.$val[0]->last_name;
						}
					}
					echo '{"date": "'.date('d/m/Y H:i',strtotime($v->datetime)).'", "fullname": "'.$fullname.'"}';
					$counter++;
				}
				echo ']';
			} else {
				echo '{}';
			}
		} else {
			echo '{}';
		}
	}
	
	public function view($exercise_id = 'all', $ref = null)
	{
		$sort = $this->input->post('set_sorting');
		if (isset($sort)&& ($sort!=null)) {
			$data = array(
			   'sort'  => $sort
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('sort')!=null) {
				$sort = $this->session->userdata('sort');
			} else {
				$sort = 0;
			}
		}
		
		$style = $this->input->post('set_style');
		if (isset($style)&& ($style!=null)) {
			$data = array(
			   'style'  => $style
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('style')!=null) {
				$style = $this->session->userdata('style');
			} else {
				$style = 0;
			}
		}
		
		$role = $this->input->post('set_role');
		if (isset($role)&& ($role!=null)) {
			$data = array(
			   'role'  => $role
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('role')!=null) {
				$role = $this->session->userdata('role');
			} else {
				$role = 0;
			}
		}
        $method = $this->input->post('set_method');
        if (isset($method)&& ($method!=null)) {
            $data = array(
                'method'  => $method
            );

            $this->session->set_userdata($data);
        } else {
            if ($this->session->userdata('method')!=null) {
                $method = $this->session->userdata('method');
            } else {
                $method = 0;
            }
        }
		
		$search = $this->input->post('search');
		if (isset($sort)) {
		} else {
			$search = null;
		}
		
		$data = '';
		$data['is_administrator'] = $this->is_administrator;
        $data['is_tonelineschool'] = $this->is_tonelineschool;
        $data['is_teacher'] = $this->is_teacher;
		$data['search_panel'] = true;
		$data['page'] = 'Exercises';
		$data['panel'] = false;
		$data['exercise_types'] = $this->general_m->pull_exercise_types();
        $data['exercise_methods'] = $this->general_m->pull_exercise_methods();
        $data['methods'] = $this->general_m->pull_exercise_methods();
		$data['breadcrumbs'] = array();
		$data['exercise_id'] = $exercise_id;
		$data['sort'] = $sort;
		$data['search'] = $search;
		$data['music_styles'] = $this->general_m->pull_music_styles();
		$data['roles'] = $this->general_m->pull_role();
		$data['role'] = $role;
		$data['style'] = $style;
        $data['method'] = $method;
		$data['total_exercises_created'] = 0;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		
		if ($exercise_id == 'me') { /* Exercises created by me */
            $data['total_exercises_created'] = $this->general_m->pull_total_exercises('me', $this->ion_auth->get_user_id());
            $data['exercises'] = $this->general_m->pull_exercises('me', $this->ion_auth->get_user_id(), $sort, $style, $role, $search);
            if($method != 0 ){
                $data['exercises']=  $this->filter_by_method($data['exercises'],array($method));
            }
            $data['title'] = 'Exercises';
            $data['subtitle'] = 'Exercises created by me';
            $data['panel'] = true;
            $data['breadcrumbs'] = array(
                'Exercises created by me' => base_url() . 'exercises/view/me'
            );
            $data['first_name'] = $this->first_name;
            $this->load->view('exercises_v', $data);
        }elseif ($exercise_id == 'tl'){
            $data['total_exercises_created'] = $this->general_m->pull_total_exercises('all', 202);
            $data['exercises'] = $this->general_m->pull_exercises('me', 202, $sort, $style, $role, $search);
            if($method != 0 ){
                $data['exercises'] =  $this->filter_by_method($data['exercises'],array($method));
            }
            $data['title'] = 'Exercises';
            $data['subtitle'] = 'Exercises created by ToneLine School';

            $data['panel'] = false;
            $data['breadcrumbs'] = array(
                'Exercises created ToneLine School' => base_url() . 'exercises/view/tl'
            );
            $data['first_name'] = $this->first_name;
            $this->load->view('exercises_v', $data);
		} else {
			if ($exercise_id == 'all') { /* Exercises sent by Toneline teachers */
				$data['total_exercises_created'] = $this->general_m->pull_total_exercises('all',$this->ion_auth->get_user_id());
				$data['exercises'] = $this->general_m->pull_exercises('all',$this->ion_auth->get_user_id(),$sort,$style,$role,$search);
                if($method != 0 ){
                    $data['exercises'] =  $this->filter_by_method($data['exercises'],array($method));
                }
				$data['title'] = 'Exercises';
				$data['subtitle'] = 'Exercises sent by Toneline teachers';
				$data['breadcrumbs'] = array(
					'Exercises sent by Toneline teachers' => base_url().'exercises/view/all'
				);
				$data['first_name'] = $this->first_name;
				$this->load->view('exercises_v',$data);
			} else {
				$referer_url = $_SERVER['HTTP_REFERER'];
				if ($referer_url!='') {
					if (strpos($referer_url,'exercises/session')>0) {
						$data['referer_page_type'] = $referer_url;
					}
				}
				$data['title'] = 'Session from bla bla, sent on 2015-01-01';
				$data['exercise'] = $this->general_m->pull_exercise($exercise_id);
				$exercise_name = '';
				if ($data['exercise']!=null) {
					$exercise_name = $data['exercise'][0]->name;
				} else {
					redirect(base_url());
				}
				
				if ($ref=='all' ){
					$data['breadcrumbs'] = array(
						'Exercises sent by Toneline teachers' => base_url().'exercises/view/all',
						$exercise_name => base_url().'exercises/view/'.$exercise_id.'/all'
					);
					$data['back_url'] = base_url().'exercises/view/all';
				} else {
					$data['breadcrumbs'] = array(
						'Exercises created by me' => base_url().'exercises/view/me',
						$exercise_name => base_url().'exercises/view/'.$exercise_id.'/me'
					);
					$data['back_url'] = base_url().'exercises/view/me';
				}
				$data['first_name'] = $this->first_name;
				$this->load->view('exercise_v',$data);
			}
		}
	}
    private function filter_by_method($exercises,$methods = array()){
        $filtered = $exercises;
        $filtered['result']=[];
        $filtered['total']=0;

        foreach ($exercises['result'] as  $key=>$val){
            foreach($val->exercise_methods as $k=>$exercise_method){
                if (in_array($exercise_method->method_id,$methods)){

                    $filtered['result'][$key] = $val;
                    $filtered['total']++;
                }
            }

        }

        return $filtered;
    }
	public function index($session_id = null)
	{
		if ($session_id == null)
		{
			redirect(base_url());
		}
		$data = '';
		$data['exercises'] = $this->general_m->pull_session_exercises($session_id);
		$data['exercise_types'] = $this->general_m->pull_exercise_types();
        $data['exercise_methods'] = $this->general_m->pull_exercise_methods();
		$data['page'] = 'Exercises';
		$data['first_name'] = $this->first_name;

		$this->load->view('exercises_v',$data);
	}
	
	public function upload_file($folder,$formats,$item)
	{
		/*
			Method is responsible of uploading and renaming files
			Folder - Folder to save the file in
			Formats - Accepted formats for current upload
			Item - Field name
		*/
		$config['upload_path'] = $folder;
		$config['allowed_types'] = $formats;
		
		$this->upload->initialize($config);
		$new_file_name = '';
		if ( ! $this->upload->do_upload($item))
		{
			$error = array('error' => $this->upload->display_errors());			
		}
		else
		{
			$data = $this->upload->data();			
			$file_name = $data['file_name'];			
			$new_file_name = sha1(rand()).'.'.substr($file_name,strlen($file_name)-3,3);			
			rename($config['upload_path']."/".$file_name, $config['upload_path'].'/'.$new_file_name);
		}		
		return $new_file_name;
	}
	
	public function update()
	{
		/*
			Method will create / update an exercise depends on exercise_id field
		*/
		$this->load->library('upload');
		$lang = 'he';
		$exercise_id = $this->input->post('exercise_id');
		$exercise_name = $this->input->post('exercise_name');
		if(preg_match('/^[a-zA-Z]/', $exercise_name)) {
			$lang = 'en';
		}
		$exercise_desc = $this->input->post('exercise_desc');
		if(preg_match('/^[a-zA-Z]/', $exercise_desc)) {
			$lang = 'en';
		}
		$exercise_video = $this->input->post('exercise_video');
		$exercise_link = $this->input->post('exercise_link');
		$exercise_types = $this->input->post('exercise_types');
        $exercise_methods = $this->input->post('exercise_methods');
		$exercise_role = $this->input->post('exercise_role');
		$music_style = $this->input->post('music_style');
		
		if ($exercise_id>0) {
			$data = array(
				'name' => $exercise_name,
				'description' => $exercise_desc,
				'role_id' => $exercise_role,
				'link' => $exercise_link,
				'music_style_id' => $music_style,
				'lang' => $lang,
				'owner_id' => $this->ion_auth->get_user_id(),
				'edited_datetime' => date('Y-m-d H:i:s')
			);
			if ($_FILES['exercise_video']['name']!='')
			{
				$exercise_video = $this->upload_file(realpath('./tl-files/video/'),'mp4','exercise_video');
				$data['video'] = $exercise_video;
			}
			if ($_FILES['exercise_audio']['name']!='')
			{
				$exercise_audio = $this->upload_file(realpath('./tl-files/audio/'),'mp3','exercise_audio');
				$data['audio'] = $exercise_audio;
			}
			if ($_FILES['exercise_notes']['name']!='')
			{
				$exercise_notes = $this->upload_file(realpath('./tl-files/notes/'),'gif|jpg|png|pdf','exercise_notes');
				$data['notes'] = $exercise_notes;
			}
		} else {
			$exercise_notes = $this->upload_file(realpath('./tl-files/notes/'),'gif|jpg|png|pdf','exercise_notes');
			$exercise_audio = $this->upload_file(realpath('./tl-files/audio/'),'mp3','exercise_audio');
			$exercise_video = $this->upload_file(realpath('./tl-files/video/'),'mp4','exercise_video');
			$data = array(
				'name' => $exercise_name,
				'description' => $exercise_desc,
				'role_id' => $exercise_role,
				'link' => $exercise_link,
				'notes' => $exercise_notes,
				'audio' => $exercise_audio,
				'video' => $exercise_video,
				'lang' => $lang,
				'music_style_id' => $music_style,
				'owner_id' => $this->ion_auth->get_user_id(),
				'created_datetime' => date('Y-m-d H:i:s')
			);
		}
		
		$exercise_id = $this->general_m->update_exercise($data,$exercise_id);
		if ($exercise_id == 0) {
		} else {
			$this->general_m->set_exercise_type($exercise_types,$exercise_id);
            $this->general_m->set_exercise_method($exercise_methods,$exercise_id);
		}
		redirect(base_url().'exercises/view/me');
	}
	
	public function check_duplicate_name()
	{
		$exercise_name = $this->input->post('exercise_name');
		$exercise_id = $this->input->post('exercise_id');
		$user_id = $this->ion_auth->get_user_id();
		
		$result = $this->general_m->check_duplicate_name($exercise_name,$exercise_id,$user_id);
		echo $result;
	}
	
	public function create()
	{
		/*
			Exercise create view
		*/
		$data = '';
		$data['music_styles'] = $this->general_m->pull_music_styles();
		$data['types'] = $this->general_m->pull_exercise_types();
        $data['methods'] = $this->general_m->pull_exercise_methods();
		$data['role'] = $this->general_m->pull_role();
		$data['page'] = 'exercises';
		$data['exercise'] = array(
			'id' => 0,
			'name' => '',
			'description' => '',
			'link' => '',
			'notes' => '',
			'video' => '',
			'audio' => '',
			'music_style_id' => '',
			'role_id' => ''
		);
		$data['state'] = 'Create';
		$data['exercise_types'] = array();
        $data['exercise_methods'] = array();
		$data['first_name'] = $this->first_name;
		$data['is_administrator'] = $this->is_administrator;
        $data['is_tonelineschool'] = $this->is_tonelineschool;
        $data['is_teacher'] = $this->is_teacher;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		$this->load->view('exercise_create_v',$data);
	}
	
	public function duplicate($exercise_id)
	{
		$exercise = $this->general_m->pull_exercise_by_id($exercise_id,$this->ion_auth->get_user_id());
		if ($exercise!=null)
		{
			$exercise_types = $this->general_m->pull_exercise_types($exercise[0]->id);

			$new_types = array();
			foreach ($exercise_types as $k=>$v)
			{
				$new_types[] = $v->type_id;
			}
            $exercise_methods = $this->general_m->pull_exercise_methods($exercise[0]->id);
            foreach ($exercise_methods as $k=>$v)
            {
                $new_methods[] = $v->method_id;
            }
			$exercise[0]->name .= ' (temporary)';
			$notes_new_file_name = '';
			if ($exercise[0]->notes!='') {
				$notes_new_file_name = sha1(rand()).'.'.substr($exercise[0]->notes,strlen($exercise[0]->notes)-3,3);
				if (file_exists(realpath('./tl-files/notes/').'/'.$exercise[0]->notes)) {
					copy(realpath('./tl-files/notes/').'/'.$exercise[0]->notes, realpath('./tl-files/notes/').'/'.$notes_new_file_name);
				}
			}
			$audio_new_file_name = '';
			if ($exercise[0]->audio!='') {
				$audio_new_file_name = sha1(rand()).'.'.substr($exercise[0]->audio,strlen($exercise[0]->audio)-3,3);
				if (file_exists(realpath('./tl-files/audio/').'/'.$exercise[0]->audio)) {
					copy(realpath('./tl-files/audio/').'/'.$exercise[0]->audio, realpath('./tl-files/audio/').'/'.$audio_new_file_name);
				}
			}
			$video_new_file_name = '';
			if ($exercise[0]->video!='') {
				$video_new_file_name = sha1(rand()).'.'.substr($exercise[0]->video,strlen($exercise[0]->video)-3,3);
				if (file_exists(realpath('./tl-files/video/').'/'.$exercise[0]->video)) {
					copy(realpath('./tl-files/video/').'/'.$exercise[0]->video, realpath('./tl-files/video/').'/'.$video_new_file_name);
				}
			}
			$data = array(
				'name' => $exercise[0]->name,
				'description' => $exercise[0]->description,
				'role_id' => $exercise[0]->role_id,
				'notes' => $notes_new_file_name,
				'audio' => $audio_new_file_name,
				'video' => $video_new_file_name,
				'music_style_id' => $exercise[0]->music_style_id,
				'owner_id' => $this->ion_auth->get_user_id(),
				'created_datetime' => date('Y-m-d H:i:s')
			);
			$exercise_id = $this->general_m->update_exercise($data,0);
			
			$this->general_m->set_exercise_type($new_types,$exercise_id);
            $this->general_m->set_exercise_method($new_methods,$exercise_id);
		}
		$this->session->set_flashdata('message', 'Your exercise has been duplicated. Please edit the new exercise.');
		redirect(base_url().'exercises/view/me');
	}
	
	public function edit($exercise_id)
	{
		/*
			Exercise edit view
		*/
		$data = '';
		$data['music_styles'] = $this->general_m->pull_music_styles();
		$data['types'] = $this->general_m->pull_exercise_types();
        $data['methods'] = $this->general_m->pull_exercise_methods();
		$data['role'] = $this->general_m->pull_role();
		$data['page'] = 'exercises';
		$data['state'] = 'Edit';
		
		$data['exercise'] = $this->general_m->pull_exercise_by_id($exercise_id,$this->ion_auth->get_user_id());
		if (isset ($data['exercise'][0])) {
            $data['exercise'] = (array)$data['exercise'][0];
        }
		$data['exercise_types'] = $this->general_m->pull_exercise_types($exercise_id);
        $data['exercise_methods'] = $this->general_m->pull_exercise_methods($exercise_id);
		$data['first_name'] = $this->first_name;
		$data['is_administrator'] = $this->is_administrator;
        $data['is_tonelineschool'] = $this->is_tonelineschool;
        $data['is_teacher'] = $this->is_teacher;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		$this->load->view('exercise_create_v',$data);
	}
}
