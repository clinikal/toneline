<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
		$groups = $this->ion_auth->get_users_groups()->row()->id;
		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
	}
	
	public function test()
	{
		/*
			create user in SimplyBook.me
			*/
			$this->load->library('SimplyBook');
			$SimplyBook = new SimplyBook();
			$SimplyBook->book();
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url().'auth/logout');
	}
	
	public function index()
	{
		$data = '';
		/*$data['sessions'] = $this->general_m->pull_sessions($this->ion_auth->get_user_id());*/
		$data['exercise_types'] = $this->general_m->pull_exercise_types();
        $data['exercise_methods'] = $this->general_m->pull_exercise_methods();
		$data['page'] = 'home';
		$data['first_name'] = $this->first_name;
		$data['is_administrator'] = $this->is_administrator;
        $data['is_teacher'] = $this->is_teacher;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		$this->load->view('welcome_v',$data);
	}
}
