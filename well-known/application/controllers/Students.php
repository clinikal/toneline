<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
		$this->email = $this->ion_auth->user()->row()->email;
		$groups = $this->ion_auth->get_users_groups()->row()->id;
		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
		
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
		$this->load->model('Admin_m');
	}
	
	public function index()
	{
		
	}
	
	public function pull_notes()
	{
		$user_id = $this->input->post('user_id');
		$data = array(
			'user_id' => $user_id,
			'teacher_id' => $this->ion_auth->get_user_id()
		);
		$result = $this->general_m->pull_teachers_notes($data);
		if ($result!=null) {
			echo $result[0]->notes; 
		} else {
			echo '';
		}
	}
	
	public function save_notes()
	{
		$user_id = $this->input->post('user_id');
		$notes = $this->input->post('notes');
		
		$data = array(
			'user_id' => $user_id,
			'teacher_id' => $this->ion_auth->get_user_id(),
			'notes' => $notes
		);
		
		$this->general_m->update_teachers_notes($data);
	}
	
	public function users()
	{		
		$data = $this->general_m->students($this->ion_auth->get_user_id());
		$display_data = '';
		$counter = 0;
		$row_counter = 0;
		$session_date = '';
		foreach ($data['results'] as $k=>$v)
		{
			$session_datetime = '0000-00-00 00:00:00';
			if (isset($v->session_datetime)) {
				$session_datetime = $v->session_datetime;
			}
			$v->type = 'Student';
			foreach ($data['sessions'] as $key=>$val)
			{				
				if ($val->owner_id == $v->id) {
					$v->type = 'Teacher';					
				}
			}
			($counter>0)?$display_data .= ',':null;
			$display_data .= '{"'.($row_counter).'":"'.$v->first_name.'",';
			$display_data .= '"'.($row_counter+1).'":"'.$v->last_name.'",';
			$display_data .= '"'.($row_counter+2).'":"'.$v->email.'",';
			$display_data .= '"'.($row_counter+3).'":"<i class=\"fa-skype\"></i><a href=\"skype:'.$v->skype_id.'?call\">'.$v->skype_id.'</a>",';
			$display_data .= '"'.($row_counter+4).'":"'.date('d/m/Y',strtotime($session_datetime)).'",';			
			$display_data .= '"'.($row_counter+5).'":"<a href=\"'.base_url().'exercises/session/'.$v->session_id.'/students\">Go to session</a>",';			
			$display_data .= '"'.($row_counter+6).'":"<a href=\"'.base_url().'sessions/create/'.$v->id.'\">Create new session</a>",';
			$display_data .= '"'.($row_counter+7).'":"<a href=\"javascript:process_notes('.$v->id.');\">Notes</a>"}';
			$counter++;
		}
		echo '{"recordsTotal":'.$data['total'].',"recordsFiltered":'.$data['total'].',"data":['.$display_data.']}';
	}
	
	public function view()
	{
		$data['is_administrator'] = $this->is_administrator;
        $data['is_teacher'] = $this->is_teacher;
		$data['total_sessions'] = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),null);
		$data['breadcrumbs'] = array(
			'Sessions Report' => base_url().'admin/view/teachers'
		);
		
		$data['subtitle'] = 'Your Students in ToneLine';
		
		$data['page'] = 'students';
		$data['first_name'] = $this->first_name;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		
		$this->load->view('students_v',$data);
	}
}
