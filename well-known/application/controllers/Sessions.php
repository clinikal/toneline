<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;

		$this->email = $this->ion_auth->user()->row()->email;
		$groups = $this->ion_auth->get_users_groups()->row()->id;
		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
	}
	
	public function index()
	{
		
	}
	
	public function remove($session_id)
	{
		$this->general_m->remove_session($session_id,$this->ion_auth->get_user_id());
		redirect(base_url().'sessions/view/me');
	}
	
	public function view($type = 'me')
	{		
		$sort = $this->input->post('set_sorting');
		if (isset($sort)&& ($sort!=null)) {
			$data = array(
			   'sort'  => $sort
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('sort')!=null) {
				$sort = $this->session->userdata('sort');
			} else {
				$sort = 1;
			}
		}
		$data['is_administrator'] = $this->is_administrator;
		$data['sort'] = $sort;
		
		$search = $this->input->post('search');
		if (isset($sort)) {
		} else {
			$search = null;
		}
		$data['search'] = $search;
		$data['total_sessions'] = 0;
		
		$data['sessions'] = $this->general_m->pull_sessions($type,$this->ion_auth->get_user_id(),$sort,$search);
		foreach ($data['sessions']['result'] as $k=>$v)
		{
			$v->exercises = $this->general_m->pull_session_exercises($v->id);
			$v->teacher = $this->general_m->pull_user($v->owner_id);
			$v->student = $this->general_m->pull_user($v->student_id);
			
			if ($search!=null) {
				if ($type == 'me') {
					if (strpos(strtolower($v->student[0]->first_name), strtolower($search)) !== false) {
					} else {
						if (strpos(strtolower($v->student[0]->last_name), strtolower($search)) !== false) {
						} else {
							unset($data['sessions']['result'][$k]);
						}
					}
				} else {
					if (strpos(strtolower($v->teacher[0]->first_name), strtolower($search)) !== false) {
					} else {
						if (strpos(strtolower($v->teacher[0]->last_name), strtolower($search)) !== false) {
						} else {
							unset($data['sessions']['result'][$k]);
						}
					}
				}
			}
		}
		if ($type=='me') {
			$data['total_sessions'] = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
			$data['breadcrumbs'] = array(
				'Sessions created by me' => base_url().'sessions/view/me'
			);
			$data['subtitle'] = 'Sessions created by me';
		} else {
			$data['total_sessions'] = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),null);
			$data['breadcrumbs'] = array(
				'Sessions created by my teachers' => base_url().'sessions/view/all'
			);
			$data['subtitle'] = 'Sessions created by my teachers';
		}
		$data['page'] = 'sessions';
		$data['type'] = $type;
		$data['first_name'] = $this->first_name;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
        $data['is_teacher'] = $this->is_teacher;
		$this->load->view('sessions_v',$data);
	}
	
	public function update()
	{
		$session_id = $this->input->post('session_id');
		$session_target = $this->input->post('session_target');
		$session_comments = $this->input->post('session_comments');
		
		$session_target = explode(',',$session_target);
		foreach ($session_target as $k=>$v)
		{
			$status = $this->ion_auth->email_check($v);
			if ($status) {
				$res = $this->general_m->pull_user_id_by_email($v);
				$student_email = $v;
				foreach ($res as $k_in=>$v_in)
				{
					$data = array(
						'owner_id' => $this->ion_auth->get_user_id(),
						'student_id' => $v_in->id,
						'datetime' => date('Y-m-d H:i:s'),
						'comments' => $session_comments
					);
					$session_id = $this->general_m->update_sessions($data,$session_id);
					$data['user'] = $this->general_m->pull_user($v_in->id);
					$data['teacher'] = $this->general_m->pull_user($this->ion_auth->get_user_id());
					$data['session_id'] = $session_id;
					
					//$this->general_m->complete_lesson_with_session($v_in->id,$this->ion_auth->get_user_id());
					
					$subject = 'Hey! You have a new session';
					$message = $this->load->view($this->config->item('email_templates', 'ion_auth').$this->config->item('email_new_session', 'ion_auth'), $data, true);
					send_email($student_email, $this->config->item('admin_email', 'ion_auth'), $subject, $message);
				}
			}
		}
		
		/*$exercises = $this->input->post('exercises');*/
		$exercises = $this->session->userdata('exercises');
		if ($exercises!='') {
			$exercises = explode(',',$exercises);
			$this->general_m->add_exercises_to_session($exercises,$session_id);
			$this->session->unset_userdata('exercises');
			redirect(base_url().'sessions/view/me');
		}				
	}
	
	public function store_remove()
	{
        $this->session->duplicate_state='duplicate';
		$exercises = $this->session->userdata('exercises');
		if ($exercises!='') {
			$exercises = explode(',',$exercises);
		}
		$id = $this->input->post('id');
		$id = str_replace('exercise','',$id);
		
		$ex_str = '';
		$counter = 0;
		foreach ($exercises as $k=>$v)
		{
			if ($v == $id) {
			} else {
				if ($counter>0) {
					$ex_str .= ',';
				}
				$ex_str .= $v;
				$counter++;
			}
		}
		$ex_str = array(
		   'exercises'  => $ex_str
	    );
		$this->session->set_userdata($ex_str);
	}
	
	public function store($exercise_id = null)
	{
		$exercises = $this->session->userdata('exercises');
		if ($exercises!='') {
			$exercises = explode(',',$exercises);
		}
		
		if ($exercise_id!=null) {
			$id = $exercise_id;
		} else {
			$id = $this->input->post('id');
			$id = str_replace('exercise','',$id);
		}
		$exercises[] = $id;
		$exercises = array_unique($exercises);
		
		$ex_str = '';
		$counter = 0;
		foreach ($exercises as $k=>$v)
		{
			if ($counter>0) {
				$ex_str .= ',';
			}
			$v = str_replace('exercise','',$v);
			$ex_str .= $v;
			$counter++;
		}
		
		$ex_str = array(
		   'exercises'  => $ex_str
	    );
		$this->session->set_userdata($ex_str);
	}
	
	public function validate_account()
	{
		$account = $this->input->post('account');
		$status = $this->ion_auth->email_check($account);
		if ($status) {
			if ($this->email!=$account) {
				echo 'true';
			} else {
				echo 'duplicate';
			}
		} else {
			echo 'false';
		}
	}

    public function duplicate_flag_clean()
    {
        $this->session->duplicate_state='';
        $this->session->exercises='';
        $this->session->userdata['method'] = 0;
    }

    public function duplicate_flag($session_id = null,$state=null)
    {

        $state_post = $this->input->post('state');
        if ($state_post !=null && $state_post !='' ){
            $state=$state_post;
        }

        if ( $state == null ) {
            redirect(base_url());
        }
        else{

            $this->session->duplicate_state=$state;
        }
    }
	
	public function duplicate($session_id = null)
	{
		if ($session_id == null) {
			redirect(base_url());
		}
		else{

		    if ($this->session->duplicate_state!=='duplicate'){
                $this->session->exercises='';
                $this->duplicate_flag($session_id,'duplicate');
            }
        }

		/*
		if ($session_id == "favicon.ico") {
		    return ;
        }*/

		$sort = $this->input->post('set_sorting');
		if (isset($sort)&& ($sort!=null)) {
			$data = array(
			   'sort'  => $sort
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('sort')!=null) {
				$sort = $this->session->userdata('sort');
			} else {
				$sort = 0;
			}
		}
		
		$style = $this->input->post('set_style');
		if (isset($style)&& ($style!=null)) {
			$data = array(
			   'style'  => $style
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('style')!=null) {
				$style = $this->session->userdata('style');
			} else {
				$style = 0;
			}
		}
		
		$role = $this->input->post('set_role');
		if (isset($role)&& ($role!=null)) {
			$data = array(
			   'role'  => $role
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('role')!=null) {
				$role = $this->session->userdata('role');
			} else {
				$role = 0;
			}
		}
        $method = $this->input->post('set_method');
        if (isset($method)&& ($method!=null)) {
            $data = array(
                'method'  => $method
            );

            $this->session->set_userdata($data);
        } else {
            if ($this->session->userdata('method')!=null) {
                $method = $this->session->userdata('method');
            } else {
                $method = 0;
            }
        }


        $search = $this->input->post('search');
		if (isset($sort)) {
		} else {
			$search = null;
		}
		
		$data = '';
		$data['my_students'] = $this->general_m->students($this->ion_auth->get_user_id());
		$data['is_administrator'] = $this->is_administrator;
		$data['sort'] = $sort;
		$data['music_styles'] = $this->general_m->pull_music_styles();
		$data['roles'] = $this->general_m->pull_role();
        $data['methods'] = $this->general_m->pull_exercise_methods();

		$data['sessions'] = $this->general_m->pull_exercises('tl',$this->ion_auth->get_user_id(),$sort,$style,$role,$search);
        if($method != 0 ){
            $data['sessions'] =  $this->filter_by_method($data['sessions'],array($method));
        }

        $all_exercises = $this->general_m->pull_exercises('me',$this->ion_auth->get_user_id());
		$data['search'] = $search;
		$data['page'] = 'sessions';
		$data['first_name'] = $this->first_name;
		$data['role'] = $role;
		$data['style'] = $style;
        $data['method'] = $method;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
        $data['is_teacher'] = $this->is_teacher;
		$exercises = $this->session->userdata('exercises');
		$ex_in_session = array();
		$ex_id_general = array();
		if ($exercises!='') {
			$exercises = explode(',',$exercises);
			foreach ($exercises as $k=>$v)
			{
				foreach ($all_exercises['result'] as $key=>$val)
				{
					if ($val->id == $v) {
						$ex_id_general[] = $val->id;
						$ex_in_session[] = '<label class="checkbox"><input value="'.$v.'" type="checkbox" id="exercise'.$v.'" name="exercises[]"><i></i>'.$val->name.'</label>';
					}
				}				
			}
		}
		$data['ex_id_general'] = $ex_id_general;
		$data['ex_in_session'] = $ex_in_session;
		
		$data['session'] = $this->general_m->pull_session($session_id,$this->ion_auth->get_user_id());
		$data['session_exercises'] = $this->general_m->pull_session_exercises($session_id,'no_total');

        $session_exercises=explode(',',$this->session->exercises);
		foreach ($data['session_exercises'] as $key=>$val)
		{
		    if (is_array($session_exercises) && $session_exercises[0]!=''){
                if (in_array($val->exercise_id,$session_exercises)){
                    $this->store($val->exercise_id);
                }
                else {
                    unset($data['session_exercises'][$key]);
                }
            } else {
                $this->store($val->exercise_id);
            }


		}
		
		$this->load->view('session_duplicate_v',$data);
	}
	
	public function create($target_user_id = null)
	{
		$sort = $this->input->post('set_sorting');
		if (isset($sort)&& ($sort!=null)) {
			$data = array(
			   'sort'  => $sort
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('sort')!=null) {
				$sort = $this->session->userdata('sort');
			} else {
				$sort = 0;
			}
		}
		
		$style = $this->input->post('set_style');
		if (isset($style)&& ($style!=null)) {
			$data = array(
			   'style'  => $style
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('style')!=null) {
				$style = $this->session->userdata('style');
			} else {
				$style = 0;
			}
		}
		
		$role = $this->input->post('set_role');
		if (isset($role)&& ($role!=null)) {
			$data = array(
			   'role'  => $role
			);
	
			$this->session->set_userdata($data);
		} else {
			if ($this->session->userdata('role')!=null) {
				$role = $this->session->userdata('role');
			} else {
				$role = 0;
			}
		}
        $method = $this->input->post('set_method');
        if (isset($method)&& ($method!=null)) {
            $data = array(
                'method'  => $method
            );

            $this->session->set_userdata($data);
        } else {
            if ($this->session->userdata('method')!=null) {
                $method = $this->session->userdata('method');
            } else {
                $method = 0;
            }
        }
		
		$search = $this->input->post('search');
		if (isset($sort)) {
		} else {
			$search = null;
		}
		
		$data = '';
		$data['my_students'] = $this->general_m->students($this->ion_auth->get_user_id());
		$data['is_administrator'] = $this->is_administrator;
		$data['sort'] = $sort;
		$data['music_styles'] = $this->general_m->pull_music_styles();
        $data['methods'] = $this->general_m->pull_exercise_methods();
		$data['roles'] = $this->general_m->pull_role();
		$data['sessions'] = $this->general_m->pull_exercises('tl',$this->ion_auth->get_user_id(),$sort,$style,$role,$search);
		if($method != 0 ){
            $data['sessions'] =  $this->filter_by_method($data['sessions'],array($method));
        }

		$all_exercises = $this->general_m->pull_exercises('tl',$this->ion_auth->get_user_id());
		$data['search'] = $search;
		$data['page'] = 'sessions';
		$data['first_name'] = $this->first_name;
		$data['role'] = $role;
		$data['style'] = $style;
        $data['method'] = $method;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
        $data['is_teacher'] = $this->is_teacher;
		$exercises = $this->session->userdata('exercises');
		$ex_in_session = array();

		if ($exercises!='') {
			$exercises = explode(',',$exercises);
			foreach ($exercises as $k=>$v)
			{
				foreach ($all_exercises['result'] as $key=>$val)
				{

					if ($val->id == $v) {
						$ex_in_session[] = '<label class="checkbox"><input value="'.$v.'" type="checkbox" id="exercise'.$v.'" name="exercises[]"><i></i>'.$val->name.'</label>';
					}
				}				
			}
		}
		$data['ex_in_session'] = $ex_in_session;
		$data['target_user_id'] = null;	
		if ($target_user_id!=null) {
			$data['target_user_id'] = $this->general_m->pull_user($target_user_id);
		}
		
		$this->load->view('session_create_v',$data);
	}

	private function filter_by_method($exercises,$methods = array()){
        $filtered = $exercises;
        $filtered['result']=[];
        $filtered['total']=0;

        foreach ($exercises['result'] as  $key=>$val){
            foreach($val->exercise_methods as $k=>$exercise_method){
                if (in_array($exercise_method->method_id,$methods)){

                    $filtered['result'][$key] = $val;
                    $filtered['total']++;
                }
            }

        }

        return $filtered;
    }
}
