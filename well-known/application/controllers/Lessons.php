<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'../vendor/autoload.php';
use JsonRPC\Client;

class Lessons extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;
		$this->simplybook_me_id = $this->ion_auth->user()->row()->simplybook_me_id;
		$groups = $this->ion_auth->get_users_groups()->row()->id;
		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
	}
	
	public function set_lesson($program_id)
	{
		$date = $this->input->get('date');
		$date = str_replace('/', '-', $date);
		$date = date('Y-m-d', strtotime($date));
		$time = $this->input->get('time');
		
		$this->load->model('Admin_m');
		/*
		 * Program Details
		 */
		$data = $this->Admin_m->get(array(
			'tbl' => 'programs',
			'where' => array('id'=>$program_id)
		));
		$teacher_id = ($data['results'][0]->teacher_id);		
		$time_length = intval($data['results'][0]->time_length);		
		/*
		 * Plan Details
		 */
		$data = $this->Admin_m->get(array(
			'tbl' => 'programs_to_users',
			'where' => array('program_id'=>$program_id,'user_id'=>$this->ion_auth->get_user_id())
		));
		$basictime_units = ($data['results'][0]->basictime_units);		
		
		$unit = $this->general_m->pull_user($teacher_id);
		$unit_id = $unit[0]->simplybook_serviceprovider_id;
		
		$my_user = $this->general_m->pull_user($this->ion_auth->get_user_id());
		$clientId = $my_user[0]->simplybook_me_id;
		
		$from_time_convert = strtotime($time);
		$endtime = date('H:i',strtotime('+'.$time_length.' minutes',$from_time_convert));
		
		$this->load->library('SimplyBook');
		$SimplyBook = new SimplyBook();
		$is_success = true;
		$is_failure_reason = '';
		for ($x=0;$x<$basictime_units;$x++) {
			$params = array(
				'eventId' => intval($program_id),
				'startDate' => $date,
				'endDate' => $date,
				'startTime' => $time.':00',
				'endTime' => $endtime.':00',
				'clientId' => intval($clientId),
				'unitId' => intval($unit_id)
			);
			$data = $SimplyBook->book($params);						
			$data = json_decode($data);
			if (isset($data->status))
			{
				if (!$data->status) {
					$is_failure_reason = $data->result->message;
					$is_success = false;
				}
			}
			$from_time_convert = strtotime($time);
			$time = date('H:i',strtotime('+'.$time_length.' minutes',$from_time_convert));
			
			$from_time_convert = strtotime($endtime);
			$endtime = date('H:i',strtotime('+'.$time_length.' minutes',$from_time_convert));
		}
		if ($is_success) {
			echo json_encode(array('status'=>true));
		} else {
			echo json_encode(array('status'=>false,'message'=>$is_failure_reason));
		}
	}
	
	public function get_available_times($program_id)
	{
		/*
		 * Pull User Basic Time Units for Current program
		 * basictime_units - value for next step
		 */
		$basictime_units = 0;
		$teacher_id = 0;
		$time_length = 0;
		$basic_time_length = 0;
		$my_programs = $this->general_m->pull_user_programs($this->ion_auth->get_user_id());
		if ($my_programs!=null) {
			foreach ($my_programs as $k=>$v)
			{
				if (($v->program_id == intval($program_id)) && ($v->is_active)) {
					$basictime_units = $v->basictime_units;
					$teacher_id = $v->program->teacher_id;
					$time_length = intval(($v->program->time_length) * ($basictime_units));
					$basic_time_length = intval(($v->program->time_length));
				}
			}
			if ($basictime_units == 0) {
				exit();
			}
		} else {
			exit();
		}		
		
		$selected_date = $this->input->get('selected_date');
		$selected_date = str_replace('T00:00:00+03:00','',$selected_date);
		$printed_date = str_replace('/', '-', $selected_date);
		$printed_date_tmp = explode('T',$printed_date);
		$printed_date = date('d/m/Y', strtotime($printed_date_tmp[0]));
		
		$unit = $this->general_m->pull_user($teacher_id);
		$unit_id = $unit[0]->simplybook_serviceprovider_id;
		
		$params = array(
			'dateFrom' => $selected_date,
			'dateTo' => $selected_date,
			'eventId' => $program_id,
			'unitId' => $unit_id
		);
		$this->load->library('SimplyBook');
		$SimplyBook = new SimplyBook();
		$available_times = $SimplyBook->get('getAvailableTimeIntervals',$params); /* Get All Available Times from SimplyBook */
		//var_dump($available_times);		
		$available_times_arr = array();
		
		foreach($available_times as $key=>$val)
		{
			$selected_date = $key;
			foreach ($val as $k=>$v)
			{
				if (($k == $unit_id)&&(!empty($v))) { /* Right Unit Id (Teacher), Get Times */
					foreach ($v as $data_key=>$data_val)
					{
						$from = $data_val->from;
						$to = $data_val->to;
						
						$to_time = strtotime($selected_date." ".$from);
						$from_time = strtotime($selected_date." ".$to);
						$minutes = round(abs($to_time - $from_time) / 60,2);
						$from_time_convert = strtotime($from);
						/* Init Time */
						if ($minutes<$time_length) {
						} else {
							$from_time_convert = date('H:i',strtotime('+0 minutes',$from_time_convert));
							$available_times_arr[] = $from_time_convert;
							for ($x=0;$x<(floor($minutes/$basic_time_length)-$basictime_units);$x++)
							{
								$from_time_convert = strtotime($from_time_convert);
								//$from_time_convert = date('H:i',strtotime('+'.$time_length.' minutes',$from_time_convert));
								$from_time_convert = date('H:i',strtotime('+'.$basic_time_length.' minutes',$from_time_convert));
								$available_times_arr[] = $from_time_convert;					
							}
						}
					}
			    }
			}
		}		
		//var_dump($available_times_arr);
		$message = '';
		if (empty($available_times_arr)) {
			$message = 'These lessons are not available on this date';
		}
		echo json_encode(array('date'=>$printed_date,'time_length'=>$time_length,'times'=>$available_times_arr,'message'=>$message));
	}
	
	public function cancel($event_id)
	{
		$this->load->library('SimplyBook');
		$SimplyBook = new SimplyBook();
		$result = $SimplyBook->edit('cancelBooking',$event_id);		
	}
	
	public function index($program_id = null, $cmd = null)
	{
		$data = array(
		);
		$data['is_administrator'] = $this->is_administrator;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
		$data['page'] = 'mylessons';
		$data['first_name'] = $this->first_name;
		$data['program_id'] = $program_id;
		$data['my_programs'] = $this->general_m->pull_user_programs($this->ion_auth->get_user_id(),$this->is_administrator);
		$data['events'] = null;
		$data['cmd'] = $cmd;
		$data['is_administrator'] = $this->is_administrator;
        $data['is_teacher'] = $this->is_teacher;
		//$found_program = false; /* Simply Book Program Id is called Event */
		/*
		foreach ($data['my_programs'] as $k=>$v)
		{
			if ($v->program_id == $data['program_id']) {
				$found_program = true;
			}
		}
		*/
		
		$this->load->library('SimplyBook');
		$SimplyBook = new SimplyBook();
		$data['programs'] = $SimplyBook->get('getEventList');
		if (($this->simplybook_me_id == 0)&&(!$this->is_administrator)) {
			$data['events'] = null;
		} else {
			if ($this->is_administrator) {
				$arr = array(
					'client_id' => 0,
					'event_id' => $program_id
				);
			} else {
				$arr = array(
					'client_id' => intval($this->simplybook_me_id),
				);
			}
			$data['events'] = $SimplyBook->get('getBookings',$arr);
		}
		
		$this->load->view('mylessons_v',$data);		
	}
	
}
