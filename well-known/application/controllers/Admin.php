<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth','form_validation'));
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		$this->load->model('general_m');
		$this->first_name = $this->ion_auth->user()->row()->first_name;
        $this->is_teacher = $this->ion_auth->user()->row()->is_teacher;
		$this->email = $this->ion_auth->user()->row()->email;
		$groups = $this->ion_auth->get_users_groups()->row()->id;


		$this->is_administrator = false;
		if ($groups == 1) //administrator
		{
			$this->is_administrator = true;
		}
		
		/*
		 * Administrator validation
		 */
		if (!$this->is_administrator) {
			redirect('auth/login', 'refresh');
		}
		$this->total_sessions_created_by_me = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),'me');
		$this->load->model('Admin_m');
		$this->load->library('SimplyBook');
	}
	
	public function index()
	{
		
	}
	
	public function getClientList()
	{
		$SimplyBook = new SimplyBook();
		$data = $SimplyBook->get('getClientList');
		?>
        <table>
        <?
		foreach($data as $k=>$v)
		{
			?>
            <tr>
            	<td><?=$v->id?></td>
                <td><?=$v->name?></td>
                <td><?=$v->phone?></td>
                <td><?=$v->email?></td>
            </tr>
            <?
		}
		?>
        </table>
        <?
	}
	
	public function update_user()
	{
		$id = $this->input->post('user_id');
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'phone' => $this->input->post('phone'),
			'simplybook_me_id' => $this->input->post('simplybook_me_id'),
			'skype_id' => $this->input->post('skype_id'),
            'is_teacher' => $this->input->post('is_teacher'),
		);
		
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		
		$continue = true;
		if ($password!='') {
			if (($password == $confirm_password)&&(strlen($password)>=8)) {				
				$pass = $this->ion_auth_model->hash_password($password,FALSE,FALSE);
				$data['password'] = $pass;
			} else {
				$continue = false;
			}
		}
		if ($continue) {
			$this->general_m->update_user($data,$id);
		}
		redirect(base_url().'admin/view/users');
	}
	
	public function login_as_user($user_id)
	{
		if ($user_id == null) {
			redirect(base_url());
		}
		$administrator = $this->is_administrator;
		if (!$administrator) {
			redirect(base_url());
		}
		
		$this->ion_auth->bypasslogin($user_id);
		header('Location:'.base_url());
	}
	
	public function delete_user($id = null)
	{
		if (($id == null)||(!is_numeric($id))) {
			redirect(base_url().'admin/view/users');
		}
		if ($this->config->item('env') == 'dev') {
			$this->general_m->delete_user($id);
		}
		redirect(base_url().'admin/view/users');
	}
	
	public function edit_user($id = null)
	{
		if (($id == null)||(!is_numeric($id))) {
			redirect(base_url().'admin/view/users');
		}
		$data = array(
			'page' => 'admin',
			'id' => $id,
			'first_name' => $this->first_name,
			'is_administrator' => $this->is_administrator,
			'total_sessions_created_by_me' => $this->total_sessions_created_by_me,
            'is_teacher' => $this->is_teacher
		);
		$data['user'] = $this->general_m->pull_user($id);
		if ($data['user'] == null) {
			redirect(base_url().'admin/view/users');
		}
		
		
		$SimplyBook = new SimplyBook();
		$data['simplybook_users'] = $SimplyBook->get('getClientList');
		$this->load->view('admin/edit_user_v',$data);
	}
	
	public function user_plans($user_id,$program_id = null)
	{
		$data = $this->general_m->pull_inactive_user_programs($user_id);
		if ($program_id == null) {
			echo json_encode($data);
		} else {
			$program_data = array();
			foreach ($data as $k=>$v)
			{
				if ($v->program_id == $program_id)
				{
					$program_data[] = $v;
				}
			}
			echo json_encode($program_data);
		}
	}
	
	public function user_programs($user_id)
	{
		if (($user_id == null)||(!is_numeric($user_id))) {
			redirect(base_url().'admin/view/users');
		}
		$data = array(
			'page' => 'admin',
			'id' => $user_id,
			'first_name' => $this->first_name,
			'is_administrator' => $this->is_administrator,
			'total_sessions_created_by_me' => $this->total_sessions_created_by_me,
            'is_teacher'=> $this->is_teacher
		);
		$data['user_id'] = $user_id;
		$data['user_selected_programs'] = $this->general_m->pull_user_programs($user_id);
		
		$this->load->view('admin/user_programs_v',$data);
	}
	
	public function set_to_program()
	{
		$program_id = $this->input->get('program_id');
		$user_id = $this->input->get('user_id');
		$status = $this->input->get('status');
		$basictime_units = $this->input->get('basictime_units');
		$time_interval = $this->input->get('time_interval');
		$price = $this->input->get('price');
		
		/*
		 * Attach user to new plan
		 */
		if ($status == 'deattached') {
			$data = array(
				'user_id' => $user_id,
				'program_id' => $program_id,
				'is_active' => 1,
				'price' => $price,
				'time_interval' => $time_interval,
				'starttime' => date('Y-m-d H:i:s'),
				'basictime_units' => $basictime_units
			);
			$this->Admin_m->insert('programs_to_users',$data);
		}
		/*
		 * Deattach user from current plan
		 */
		if ($status == 'attached') {
			$where = [];
			$where[] = array('field'=>'user_id','value'=>$user_id);
			$where[] = array('field'=>'program_id','value'=>$program_id);
			$where[] = array('field'=>'is_active','value'=>1);
			$data = array(
				'is_active' => 0,
				'endtime' => date('Y-m-d H:i:s')
			);
			$this->Admin_m->update('programs_to_users',$data,$where);
		}
		/*
		 * Update user's current plan
		 */
		if ($status == 'update') {
			$where = [];
			$where[] = array('field'=>'user_id','value'=>$user_id);
			$where[] = array('field'=>'program_id','value'=>$program_id);
			$where[] = array('field'=>'is_active','value'=>1);
			$data = array(
				'is_active' => 1,
				'price' => $price,
				'time_interval' => $time_interval,
				'starttime' => date('Y-m-d H:i:s'),
				'basictime_units' => $basictime_units
			);
			$this->Admin_m->update('programs_to_users',$data,$where);
		}
	}
	
	public function users()
	{
		$data = $this->Admin_m->users();
		$display_data = '';
		$counter = 0;
		$row_counter = 0;
		foreach ($data['results'] as $k=>$v)
		{
            $v->type = 'Student';
            if($v->is_teacher==1){
                $v->type = 'Teacher';
            }
			($counter>0)?$display_data .= ',':null;
			$display_data .= '{"'.$row_counter.'":"'.gmdate('d/m/Y',$v->created_on).'",';
			$display_data .= '"'.($row_counter+1).'":"'.gmdate('d/m/Y',$v->last_login).'",';
			$display_data .= '"'.($row_counter+2).'":"'.$v->first_name.'",';
			$display_data .= '"'.($row_counter+3).'":"'.$v->last_name.'",';
			$display_data .= '"'.($row_counter+4).'":"'.$v->email.'",';
			$display_data .= '"'.($row_counter+5).'":"'.$v->type.'",';
			$display_data .= '"'.($row_counter+6).'":"<a href=\"'.base_url().'admin/user_programs/'.$v->id.'\">Programs</a>",';
			$display_data .= '"'.($row_counter+7).'":"<a href=\"'.base_url().'admin/edit_user/'.$v->id.'\">Edit</a>",';			
			if ($this->config->item('env') == 'dev') {
				$display_data .= '"'.($row_counter+8).'":"<a href=\"'.base_url().'admin/login_as_user/'.$v->id.'\">Login</a>",';
				$display_data .= '"'.($row_counter+9).'":"<a href=\"'.base_url().'admin/delete_user/'.$v->id.'\" style=color:red;>Delete</a>"}';
			} else {
				$display_data .= '"'.($row_counter+8).'":"<a href=\"'.base_url().'admin/login_as_user/'.$v->id.'\">Login as user</a>"}';
			}
			$counter++;
		}
		echo '{"recordsTotal":'.$data['total'].',"recordsFiltered":'.$data['total'].',"data":['.$display_data.']}';
	}
	
	public function update_user_programs()
	{
		$user_id = $this->input->post('user_id');
		$selected_programs = $this->input->post('selected_programs');
		
		$this->general_m->remove_user_programs($user_id);
		$selected_programs = explode(',',$selected_programs);
		foreach ($selected_programs as $k=>$v)
		{
			if ($v>0) {
				$this->general_m->set_user_program($user_id,$v);
			}
		}
		header('Location:'.base_url().'admin/user_programs/'.$user_id);
	}
	
	public function data($type = 'teachers',$id = null, $echo = true)
	{
		$params = array(
			'start' => $this->input->get('start'),
			'length' => $this->input->get('length'),
			'order_column' => $this->input->get('order[0][column]'),
			'order_dir' => $this->input->get('order[0][dir]'),
		);
		
		if ($type == 'user_programs') {
			$teachers = $this->get(array('tbl'=>'users'));
			$locations = $this->get(array('tbl'=>'locations'));
			$instruments = $this->get(array('tbl'=>'instruments'));
			
			$results = $this->Admin_m->data($params,'programs');
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($data_tmp as $k=>$v)
			{
				$location_name = 'N/A';
				foreach ($locations['results'] as $key=>$val)
				{
					if ($val->id == $v->location_id) {
						$location_name = $val->name;
					}
				}
				$teacher_name = 'N/A';
				foreach ($teachers['results'] as $key=>$val)
				{
					if ($val->id == $v->teacher_id) {
						$teacher_name = $val->first_name.' '.$val->last_name;
					}
				}
				$instrument_name = 'N/A';
				foreach ($instruments['results'] as $key=>$val)
				{
					if ($val->id == $v->instrument_id) {
						$instrument_name = $val->name;
					}
				}
				$time_length = $v->time_length;
				($counter>0)?$data .= ',':null;
				$data .= '{"'.$row_counter.'":"",';
				$data .=  '"'.($row_counter+1).'":"'.$v->name.'",';
				$data .=  '"'.($row_counter+2).'":"'.$location_name.'",';
				$data .=  '"'.($row_counter+3).'":"'.$teacher_name.'",';
				$data .=  '"'.($row_counter+4).'":"'.$instrument_name.'",';
				$data .=  '"'.($row_counter+5).'":"'.$time_length.'",';
				$data .=  '"'.($row_counter+6).'":"<a href=\"#\" class=\"edit\" data-length=\"'.$v->length.'\" data-id=\"'.$v->id.'\" data-teacherid=\"'.$v->teacher_id.'\" data-locationid=\"'.$v->location_id.'\" data-instrumentid=\"'.$v->instrument_id.'\" data-name=\"'.$v->name.'\">Edit</a>",';
				$data .=  '"'.($row_counter+7).'":"<a href=\"#\" class=\"delete\" data-id=\"'.$v->id.'\">Delete</a>",';
				$data .=  '"DT_RowId":"dtr'.$v->id.'"}';
				$counter++;
			}
		}
		if ($type == 'users_in_program') {
			$counter = 0;
			$results = $this->get(array('tbl'=>'users'));
			$where = array('program_id'=>$id); // $id == program_id
			$users_results = $this->get(array('tbl'=>'programs_to_users','where'=>$where));
			$data = '';
			$row_counter = 0;
			$price = 0;
			$data_tmp = ($results['results']);
			foreach ($data_tmp as $k=>$v)
			{				
				($counter>0)?$data .= ',':null;
				$data .=  '{"'.($row_counter).'":"'.$v->first_name.'",';
				$data .=  '"'.($row_counter+1).'":"'.$v->last_name.'",';
				$data .=  '"'.($row_counter+2).'":"'.$v->email.'",';
				$found_in_program = false;
				foreach ($users_results['results'] as $key=>$val)
				{
					if (($val->user_id == $v->id)&&($val->is_active)) {
						$found_in_program = true;
						$price = $val->price;
						$basictime_units = $val->basictime_units;
						$time_interval = $val->time_interval;
					}
				}
				if ($found_in_program) {
					$data .=  '"'.($row_counter+3).'":"<a href=\"#\" class=\"edit_plan\" data-price=\"'.$price.'\" data-time_interval=\"'.$time_interval.'\" data-basictime_units=\"'.$basictime_units.'\" data-status=\"update\" data-programid=\"'.$id.'\" data-id=\"'.$v->id.'\">Edit plan</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"#\" class=\"attach_to_program\" data-status=\"attached\" data-price=\"'.$price.'\" data-time_interval=\"'.$time_interval.'\" data-basictime_units=\"'.$basictime_units.'\" data-programid=\"'.$id.'\" data-id=\"'.$v->id.'\">Deattach from program</a>",';
				} else {
					$data .=  '"'.($row_counter+3).'":"<a href=\"#\" class=\"attach_to_program\" data-status=\"deattached\" data-programid=\"'.$id.'\" data-id=\"'.$v->id.'\">Attach to program</a>",';
				}
				$data .=  '"DT_RowId":"dtr'.$v->id.'"}';
				$counter++;
			}
		}
		if ($type == 'programs') {
			$SimplyBook = new SimplyBook();
			$events = $SimplyBook->get('getEventList');
			
			$teachers = $this->get(array('tbl'=>'users'));
			$locations = $this->get(array('tbl'=>'locations'));
			$instruments = $this->get(array('tbl'=>'instruments'));
			
			$results = $this->Admin_m->data($params,$type);
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($events as $k=>$v)
			{
				$v->location_id = 0;
				$v->teacher_id = 0;
				$v->instrument_id = 0;
				$location_id = 0;
				$teacher_id = 0;
				$instrument_id = 0;
				$time_length = 'N/A';
				
				$length = 0;
				$simplybook_program_text = '';
				$simplybook_program_id = -1;
				$toneline_program_id = $v->id;
				foreach ($data_tmp as $key=>$val)
				{
					/*
					 * Try to find the match between Simplybook.me to Toneline's program
					 */
					if ($val->simplybook_program_id == $v->id)
					{						
						$location_id = $val->location_id;
						$teacher_id = $val->teacher_id;
						$instrument_id = $val->instrument_id;
						$simplybook_program_id = $val->simplybook_program_id;
						$time_length = ($val->time_length == 0)?'N/A':$val->time_length;
						$length = $val->length;
					}
				}
				$location_name = 'N/A';
				foreach ($locations['results'] as $key=>$val)
				{
					if ($val->id == $location_id) {
						$location_name = $val->name;
					}
				}
				$teacher_name = 'N/A';
				foreach ($teachers['results'] as $key=>$val)
				{
					if ($val->id == $teacher_id) {
						$teacher_name = $val->first_name.' '.$val->last_name;
					}
				}
				$instrument_name = 'N/A';
				foreach ($instruments['results'] as $key=>$val)
				{
					if ($val->id == $instrument_id) {
						$instrument_name = $val->name;
					}
				}		
				($counter>0)?$data .= ',':null;
				$data .=  '{"'.($row_counter).'":"'.$v->name.'",';
				$data .=  '"'.($row_counter+1).'":"'.$location_name.'",';
				$data .=  '"'.($row_counter+2).'":"'.$teacher_name.'",';
				$data .=  '"'.($row_counter+3).'":"'.$instrument_name.'",';
				$data .=  '"'.($row_counter+4).'":"'.$time_length.'",';				
				if ($simplybook_program_id == -1) {
					$data .=  '"'.($row_counter+5).'":"&nbsp;",';
				} else {
					$data .=  '"'.($row_counter+5).'":"<a href=\"'.base_url().'admin/view/users_in_program/'.$toneline_program_id.'\" class=\"users\" data-id=\"'.$toneline_program_id.'\">Users</a>",';
				}
				$data .=  '"'.($row_counter+6).'":"<a href=\"#\" class=\"edit\" data-length=\"'.$length.'\" data-id=\"'.$toneline_program_id.'\" data-teacherid=\"'.$v->teacher_id.'\" data-locationid=\"'.$v->location_id.'\" data-instrumentid=\"'.$v->instrument_id.'\" data-name=\"'.$v->name.'\">Edit</a>"}';
				$counter++;	
			}
		}
		if ($type == 'instruments') {
			$results = $this->Admin_m->data($params,$type);
			
			$results_programs = $this->Admin_m->data('','programs');
			$results_programs = $results_programs['results'];
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($data_tmp as $k=>$v)
			{
				$attached_to_program = false;
				foreach ($results_programs as $key=>$val)
				{
					if ($val->instrument_id == $v->id) {
						$attached_to_program = true;
					}
				}
				($counter>0)?$data .= ',':null;
				$data .=  '{"'.($row_counter).'":"'.$v->name.'",';
				$data .=  '"'.($row_counter+1).'":"<a href=\"#\" class=\"edit\" data-id=\"'.$v->id.'\" data-name=\"'.$v->name.'\">Edit</a>",';
				if ($attached_to_program) {
					$data .=  '"'.($row_counter+2).'":"Attached to program"}';
				} else {
					$data .=  '"'.($row_counter+2).'":"<a href=\"#\" class=\"delete\" data-id=\"'.$v->id.'\">Delete</a>"}';
				}
				$counter++;
			}
		}
		if ($type == 'locations') {
			$results = $this->Admin_m->data($params,$type);
			
			$results_programs = $this->Admin_m->data('','programs');
			$results_programs = $results_programs['results'];
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($data_tmp as $k=>$v)
			{
				$attached_to_program = false;
				foreach ($results_programs as $key=>$val)
				{
					if ($val->location_id == $v->id) {
						$attached_to_program = true;
					}
				}
				($counter>0)?$data .= ',':null;
				$data .=  '{"'.($row_counter).'":"'.$v->name.'",';
				$data .=  '"'.($row_counter+1).'":"<a href=\"#\" class=\"edit\" data-id=\"'.$v->id.'\" data-name=\"'.$v->name.'\">Edit</a>",';
				if ($attached_to_program) {
					$data .=  '"'.($row_counter+2).'":"Attached to program"}';
				} else {
					$data .=  '"'.($row_counter+2).'":"<a href=\"#\" class=\"delete\" data-id=\"'.$v->id.'\">Delete</a>"}';
				}
				$counter++;
			}
		}
		if ($type == 'students') {
			$results = $this->Admin_m->data($params,$type);
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($data_tmp as $k=>$v)
			{
				foreach ($results['users'] as $key=>$val)
				{
					if ($v->owner_id == $val->id) {
						$student_name = $val->last_name.' '.$val->first_name;
					}
				}
				($counter>0)?$data .= ',':null;
				if ($v->edited_datetime == '0000-00-00 00:00:00') {
					$edited_datetime = 'Never';
				} else {
					$edited_datetime = date('d/m/Y',strtotime($v->edited_datetime));
				}				
				$exercise_name = ($v->name);
				$exercise_name = str_replace("	","",$exercise_name);				
				$exercise_name = str_replace("\\","",$exercise_name);
				$data .= '{"'.$row_counter.'":"'.date('d/m/Y',strtotime($v->created_datetime)).'",';
				$data .=  '"'.($row_counter+1).'":"'.$edited_datetime.'",';
				$data .=  '"'.($row_counter+2).'":"'.$student_name.'",';
				$data .=  '"'.($row_counter+3).'":"'.$exercise_name.'"}';
				$counter++;
			}
		}
		if ($type == 'teachers') {
			$results = $this->Admin_m->data($params,$type);
			
			$data_tmp = ($results['results']);
			$counter = 0;
			$data = '';
			$row_counter = 0;
			foreach ($data_tmp as $k=>$v)
			{
				foreach ($results['users'] as $key=>$val)
				{
					if ($v->owner_id == $val->id) {
						$teacher_name = $val->last_name.' '.$val->first_name;
						$teacher_email = $val->email;
					}
					if ($v->student_id == $val->id) {
						$student_name = $val->last_name.' '.$val->first_name;
					}
				}
				$total_exercises = 0;
				foreach ($results['exercises'] as $key=>$val)
				{
					if ($v->id == $val->sessionid) {
						$total_exercises = $val->total;
					}
				}
				($counter>0)?$data .= ',':null;
				$data .= '{"'.$row_counter.'":"'.date('d/m/Y',strtotime($v->datetime)).'",';
				$data .=  '"'.($row_counter+1).'":"'.$teacher_name.'",';
				$data .=  '"'.($row_counter+2).'":"'.$student_name.'",';
				$data .=  '"'.($row_counter+3).'":"<a href=\"javascript:generate_exercises_modal('.$v->id.');\">'.$total_exercises.'</a>"}';
				$counter++;
			}
		}
		if ($echo) {
			echo '{"recordsTotal":'.$results['total'].',"recordsFiltered":'.$results['total'].',"data":['.$data.']}';
		} else {
			return $results;
		}
	}
	
	public function set_program($id = null)
	{
		$teacher_to_remove_id = 0;
		if ($id > 0) {
			$teacher = $this->Admin_m->get(array(
				'tbl' => 'programs',
				'where' => array(
					'id' => $id
				)
			));
			$teacher_to_remove_id = $teacher['results'][0]->teacher_id;
		}
		
		$program_name = $this->input->post('program_name');
		$instrument_id = $this->input->post('instrument_id');
		$location_id = $this->input->post('location_id');
		$teacher_id = $this->input->post('teacher_id'); /* simplybook_serviceprovider_id - unit */
		if ($teacher_to_remove_id == $teacher_id) {
			$teacher_to_remove_id = 0;
		}
		$notes = $this->input->post('notes');
		$simplybook_program_id = $this->input->post('simplybook_program_id');
		$length = $this->input->post('length');
		$time_length = $this->input->post('time_length');
		
		$data = array(
			'name' => $program_name,
			'instrument_id' => $instrument_id,
			'location_id' => $location_id,
			'notes' => $notes,
			'length' => $length,
			'simplybook_program_id' => $simplybook_program_id,
			'time_length' => $time_length,
			'teacher_id' => $teacher_id
		);		
				
		$SimplyBook = new SimplyBook();
		if ($id == 0) {
			$params = array(
				'name' => $program_name,
				'description' => $notes,
				'duration' => $time_length
			);
			/*
			Integrity constraint violation: 1452 Cannot add or update a child row: a foreign key constraint fails (`em_tonelinetest`.`event_unit_group`, CONSTRAINT `event_unit_group_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE)
			*/
			$resultset = $SimplyBook->add('addService',$params);
			$data['simplybook_program_id'] = $resultset->id;
			$data['id'] = $resultset->id;
			$id = $data['id'];
			$service_id = $data['id'];
		} else {
			$params = array(
				'name' => $program_name,
				'description' => $notes,
				'duration' => $time_length
			);
			$resultset = $SimplyBook->edit('editService',$id,$params);
			$service_id = $id;
		}
		$this->Admin_m->set_program($data,$id);
		
		/*
		 * Set SimplyBook Service Provider
		 */
		$teacher = $this->Admin_m->get(array(
			'tbl' => 'users',
			'where' => array(
				'id' => $teacher_id
			)
		));
		$simplybook_serviceprovider_id = $teacher['results'][0]->simplybook_serviceprovider_id;
		if ($simplybook_serviceprovider_id == 0) {
			$params = array(
				'name' => $teacher['results'][0]->first_name.' '.$teacher['results'][0]->last_name,
				'phone' => $teacher['results'][0]->phone,
				'email' => $teacher['results'][0]->email,
				'qty' => 1,
				'services' => array($service_id)
			);
			$data = $SimplyBook->add('addServiceProvider',$params);
			$simplybook_serviceprovider_id = $data->id;
			$where = array();
			$where[] = array(
					'field' => 'id',
					'value' => $teacher_id
			);
			$data = array(
				'simplybook_serviceprovider_id' => $simplybook_serviceprovider_id
			);
			$this->Admin_m->update('users',$data,$where);
		} else {
			$teacher_programs = $this->Admin_m->get(array(
				'tbl' => 'programs',
				'where' => array(
					'teacher_id' => $teacher_id
				)
			));
			$programs = array();
			foreach ($teacher_programs['results'] as $k=>$v)
			{
				$programs[] = $v->id;
			}
			$params = array(
				'services' => $programs
			);
			try {
				$SimplyBook->edit('editServiceProvider',$simplybook_serviceprovider_id,$params);	
			} catch (Exception $e) {
			}
		}
		
		if ($teacher_to_remove_id>0) {
			$teacher_programs = $this->Admin_m->get(array(
				'tbl' => 'programs',
				'where' => array(
					'teacher_id' => $teacher_to_remove_id
				)
			));
			$programs = array();
			foreach ($teacher_programs['results'] as $k=>$v)
			{
				$programs[] = $v->id;
			}
			$params = array(
				'services' => $programs
			);
			try {
				$SimplyBook->edit('editServiceProvider',$teacher_to_remove_id,$params);	
			} catch (Exception $e) {
			}			
		}
	}
	
	public function set_location($id = null)
	{
		$location_name = $this->input->post('location_name');
		$data = array(
			'name' => $location_name
		);
		$this->Admin_m->set_location($data,$id);
	}
	
	public function remove_program($id)
	{
		$data = $this->data('users_in_program',$id,false);
		if ($data['total'] == 0) {
			$this->Admin_m->remove_program($id);
		}
	}
	
	public function remove_location($id)
	{
		$this->Admin_m->remove_location($id);
	}
	
	public function set_instrument($id = null)
	{
		$instrument_name = $this->input->post('instrument_name');
		$data = array(
			'name' => $instrument_name
		);
		$this->Admin_m->set_instrument($data,$id);
	}
	
	public function remove_instrument($id)
	{
		$this->Admin_m->remove_instrument($id);
	}
	
	public function exercises($session_id)
	{
		if ($session_id == null) {
			echo '{}';
		} else {
			$data = $this->general_m->pull_session_exercises($session_id,'no_total');
			$arr = array();
			foreach ($data as $k=>$v)
			{
				$arr[] = array(
					'name' => $v->name,
					'first_time' => $v->first_time,
					'num_of_times' => $v->num_of_times
				);
			}
			echo json_encode($arr);
		}
	}
	
	public function get($data)
	{
		$result = $this->Admin_m->get($data);
		return $result;
	}
	
	public function programs($program_id = null)
	{
		if ($program_id == null) {
			exit();
		}						
		$data = $this->get(array('tbl'=>'programs','where'=>array('id'=>$program_id)));
		if ($data['total'] == 0) {
			$data = array('results'=>array(
				array('notes' => '')
			));
		}
		echo json_encode($data);		
		/*
		{"recordsTotal":2,"recordsFiltered":2,"data":[{"0":"1","1":"Tseasdasd","2":"Test1","3":"Efi Ben-Tov","4":"Piano","5":"<a href=\"http://codecode.cc/toneline/admin/view/users_in_program/1\" class=\"users\" data-id=\"1\">Users</a>","6":"<a href=\"#\" class=\"edit\" data-length=\"12\" data-id=\"1\" data-teacherid=\"6\" data-locationid=\"2\" data-instrumentid=\"2\" data-name=\"Tseasdasd\">Edit</a>","7":"<a href=\"#\" class=\"delete\" data-id=\"1\">Delete</a>"},{"0":"2","1":"Elad Test","2":"Test2","3":"Eran1 Elyakim2","4":"Piano","5":"<a href=\"http://codecode.cc/toneline/admin/view/users_in_program/2\" class=\"users\" data-id=\"2\">Users</a>","6":"<a href=\"#\" class=\"edit\" data-length=\"0\" data-id=\"2\" data-teacherid=\"2\" data-locationid=\"1\" data-instrumentid=\"2\" data-name=\"Elad Test\">Edit</a>","7":"<a href=\"#\" class=\"delete\" data-id=\"2\">Delete</a>"}]}
		*/
	}
	
	public function view($type = 'teachers',$item_id = null)
	{
		$data['is_administrator'] = $this->is_administrator;
		
		$data['total_sessions'] = $this->general_m->pull_total_sessions($this->ion_auth->get_user_id(),null);
		$data['breadcrumbs'] = array(
			'Sessions Report' => base_url().'admin/view/teachers'
		);
		
		switch($type)
		{
			case 'instruments': $data['subtitle'] = 'Instruments in ToneLine'; break;
			case 'locations': $data['subtitle'] = 'Locations in ToneLine'; break;
			case 'students': $data['subtitle'] = 'Excercises Created and Edited in ToneLine'; break;
			case 'users': $data['subtitle'] = 'Registered Users'; break;
			case 'programs': $data['subtitle'] = 'Programs in ToneLine'; break;
			case 'users_in_program':  $data['plans'] = $this->Admin_m->get(array('tbl'=>'programs_to_users','where'=>array('program_id'=>$item_id,'is_active'=>0)));
									  $data['program'] = $this->Admin_m->get(array('tbl'=>'programs','where'=>array('id'=>$item_id)));
									  if (!isset($data['program']['results'])) {
										  exit();
									  }
									  $data['subtitle'] = 'Users in Program: '; break;
			default: $data['subtitle'] = 'Sessions Sent in ToneLine'; break;
		}
		
		$data['page'] = 'admin';
		$data['type'] = $type;
		$data['first_name'] = $this->first_name;
		$data['total_sessions_created_by_me'] = $this->total_sessions_created_by_me;
        $data['is_teacher'] = $this->is_teacher;
		
		switch($type)
		{
			case 'users': $this->load->view('admin/users_v',$data); break;
			case 'locations': $this->load->view('admin/locations_v',$data); break;
			case 'users_in_program': $this->load->view('admin/users_in_program_v.php',$data); break;
			case 'programs': 	$data['teachers'] = $this->get(array('tbl'=>'users','order_by'=>'first_name,last_name'));
								$data['locations'] = $this->get(array('tbl'=>'locations'));
								$data['instruments'] = $this->get(array('tbl'=>'instruments'));																
								$SimplyBook = new SimplyBook();
								$data['simplybook_programs'] = $SimplyBook->get('getEventList');
								$this->load->view('admin/programs_v',$data); break;
			case 'instruments': $this->load->view('admin/instruments_v',$data); break;
			default: $this->load->view('admin/report_v',$data);
		}
	}
}
