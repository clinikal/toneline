<?
class SimplyBook
{
	public function __construct()
	{
		$CI =& get_instance();
		$CI->load->library('JSONRpcClient',array('url'=>'http://user-api.simplybook.me/login'));	
		$this->config = $CI->config->item('simplybook');
	}
	
	public function getToken()
	{
		$CI =& get_instance();
		$config = $this->config;
		
		$api_key = $config['api_key']['key'];		
		
		$loginClient = new JsonRpcClient('http://user-api.simplybook.me' . '/login/');
		$token = $loginClient->getUserToken($config['credentials']['account'], $config['credentials']['username'], $config['credentials']['password']);
		return $token;
	}
	
	public function book($params)
	{
		$CI =& get_instance();
		$config = $this->config;
		$token = $this->getToken();
		
		$client = new JsonRpcClient('http://user-api.simplybook.me/admin/' , array(
			'headers' => array(
			'X-Company-Login: ' . $config['credentials']['account'],
			'X-User-Token: ' . $token
			)
		));
		$clientTimeOffset = 0;
		$count = 1;
		$batchId = '';
		$recurringData = '';
		$data = $client->book($params['eventId'], $params['unitId'], $params['clientId'], $params['startDate'], $params['startTime'], $params['endDate'], $params['endTime'], $clientTimeOffset, $count, $batchId, $recurringData);
		return json_encode($data);
	}
	
	public function add($method,$params = null)
	{
		$CI =& get_instance();
		$config = $this->config;
		$token = $this->getToken();
		
		$client = new JsonRpcClient('http://user-api.simplybook.me/admin/' , array(
			'headers' => array(
			'X-Company-Login: ' . $config['credentials']['account'],
			'X-User-Token: ' . $token
			)
		));		
		
		switch ($method)
		{
			case 'addServiceProvider': $data = $client->addServiceProvider($params); break;
			case 'addService': $data = $client->addService($params); break;
			case 'addClient': $data = $client->addClient($params); break;
		}
		return $data;
	}
	
	public function edit($method,$id,$params = null)
	{
		$CI =& get_instance();
		$config = $this->config;
		$token = $this->getToken();
		
		$client = new JsonRpcClient('http://user-api.simplybook.me/admin/' , array(
			'headers' => array(
			'X-Company-Login: ' . $config['credentials']['account'],
			'X-User-Token: ' . $token
			)
		));		
		
		switch ($method)
		{
			case 'cancelBooking': $data = $client->cancelBooking($id); break;
			case 'editServiceProvider': $data = $client->editServiceProvider($id,$params); break;
			case 'editService': $data = $client->editService($id,$params); break;
		}
		return $data;
	}
	
	public function get($method,$params = null)
	{
		$token = $this->getToken();
		$CI =& get_instance();
		$config = $this->config;
		$this->url = 'http://user-api.simplybook.me';
		switch($method)
		{
			case 'getClientBookings': $service_url = '/toneline/'; break;
			default: $service_url = '/admin/'; break;
		}
		
		$client = new JsonRpcClient($this->url . $service_url, array(
			'headers' => array(
			'X-Company-Login: ' . $config['credentials']['account'],
			'X-User-Token: ' . $token
			)
		));	
		
		switch ($method)
		{			
			case 'getClientBookings': $data = $client->getClientBookings($params['clientId'],$params['sign'],''); break;
			case 'getClientList': $data = $client->getClientList(); break;
			case 'getUnitList': $data = $client->getUnitList(); break;
			case 'getEventList': $data = $client->getEventList(); break;
			case 'getBookings': $data = $client->getBookings($params); break;
			case 'getWorkDaysInfo': $data = $client->getWorkDaysInfo($params); break;
			case 'getAvailableTimeIntervals': $data = $client->getAvailableTimeIntervals(
														$params['dateFrom'],
														$params['dateTo'],
														$params['eventId'],
														$params['unitId'],
														1
													  );
													  break;
		}
		return $data;
		//$categories = $client->getUnitList();
		/*$params = array(
			"date_from" => date('Y-m-d',strtotime('-2 weeks')),
			"date_to" => date('Y-m-d',strtotime('+2 weeks')),
		);*/		
	}
}