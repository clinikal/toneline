<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'toneline_wp');

/** MySQL database username */
define('DB_USER', 'toneline_wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'T8.(sKVv{]]M');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8onnnyaaqgk91stozof0icwugpb0u9cf0clubb5push3w6ulbe9f7e653qxbpjiv');
define('SECURE_AUTH_KEY',  'ltgcajfq9rwycyqefcidsmiyfu04fgevdvx8mgue4kr7ioev7lljhsqix9fjy1hx');
define('LOGGED_IN_KEY',    'zecq2zadymtzh0rncmvdrar1cnuegfbf1igkap5h1bsiiwuvak9cb8j6vhxolbpo');
define('NONCE_KEY',        '1qmvue9v1nzqxzk9pgbusjkq3rjhe08vbeluxoynxign7s2cbgwb8uvpfhyga5zp');
define('AUTH_SALT',        'y6icdybirdawbv0sdtby36vzfr22ty4blmigv52krq9l05ck5amhszfdebzf6mp3');
define('SECURE_AUTH_SALT', '8gmg2c9wyzhav1ujqwiv0efuzmyiv3b42nmznec0oe3pknd9lfdsymlmzb2oe05z');
define('LOGGED_IN_SALT',   '7datuvmop7dzzvsbuvki0qfsyqlxywtoyhakhfwo53y0tipjyhwvsoxzxztwuh1h');
define('NONCE_SALT',       'ypwzaehplincgmtv92yfcqpen6icwyve1kd1rffibgjlf8yxxld9ic5exntsiqyd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpnk_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
